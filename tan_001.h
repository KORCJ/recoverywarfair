/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "cocos2d.h"
#include "enemyTan.h"
#include "tan_basic.h"
#include "mainRogic.h"

class tan_001 : public tan_basic{
private:
	cocos2d::Point pos;
	mainRogic *Run;
	float angle_ac,angle_ac_acc,speed,angle;
	int count, size, bound_angle;
	std::vector<enemyTan*> tanmak;
	int tanType, shootType;
	float dt_angle;
	int state, value, density, tag, parentTag;
	~tan_001();
public:
	int prevMakedTime, called_time;

	void add_prevMarkedTime();

	int get_prevMarkedTime();

	tan_001(mainRogic *forRun, int x ,int y ,int shootType, float n_speed, int angle, float angle_ac, float angle_ac_acc, float count, int tanType, int value, int density);

	void setBounds(int angle);

	void make();

	void move();

	void add_point();

	int get_state();

	int get_density();

	void set_stat(int stat);

	void updatePosition(cocos2d::Point &n_pos);

	void set_parent_tanmak(int tag);

	void set_tag(int tag);

	int get_tag();

	int get_parent_tag();

	void set_center(cocos2d::Point);

	void set_additional_info(float speed, int angle, float angle_acc);

	void removecurrentTan();
};