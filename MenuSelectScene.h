/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef _MenuSelectScene_
#define _MenuSelectScene_
#include "LocalPreferenceManager.h"
#include "mainRogic.h"

class MenuSelectScene : public cocos2d::LayerColor{
private:
	void start_func(cocos2d::Object *pSender);
	
	void option_func(cocos2d::Object *pSender);

	void collection_func(cocos2d::Object *pSender);

	void ranking_func(cocos2d::Object *pSender);

	void back_func(cocos2d::Object *pSender);

	void selt_1_func(cocos2d::Object *pSender);

	void selt_2_func(cocos2d::Object *pSender);

	void selt_3_func(cocos2d::Object *pSender);

	void selt_func(cocos2d::Object *pSender);

	void call_type_info(int type_num);

	void set_Prefernce_func(cocos2d::Object *pSender);

	void update();

	void stage_selt(cocos2d::Object *pSender);

	cocos2d::Sprite *_pointer_sprite;
	
	cocos2d::Sprite *pointer_sprite;

	cocos2d::Sprite *option_pointer_sprite;

	cocos2d::Sprite *stage_selt_sprite;

	cocos2d::Size screenSize;

	short selt_num;

	bool flag_1, flag_2, flag_3;

	LocalPrefernceData pref_data;
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	// implement the "static create()" method manually
	CREATE_FUNC(MenuSelectScene);

	// button func
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event);
};
#endif