/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef _logoScene_
#define _logoScene_
#include "cocos2d.h"
#include "MenuSelectScene.h"

class logoScene : public cocos2d::LayerColor{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	// implement the "static create()" method manually
	CREATE_FUNC(logoScene);

	void call_menu(float dt);
};
#endif
