/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef _tan_drawing_
#define _tan_drawing_
#include "cocos2d.h"
#include "enemyTan.h"
#include "tan_basic.h"
#include "mainRogic.h"
#include "toolBoxxxxx.h"

class tan_drawing : public tan_basic{
private:
	mainRogic *forRun;
	cocos2d::Point str, dst, center, tan_dst;
	float speed, half_diameter, angle, interval, tan_angle, angle_density, oval_radial, rotate_angle, gap_between;
	int tan_type, draw_type, density, value, stat, prevMakedTime, parentTag, tag, endTime, after_type;
	std::vector<enemyTan*> tanmak;
public:
	tan_drawing(mainRogic *Run, cocos2d::Point str, cocos2d::Point dst, cocos2d::Point tan_dst, float speed, int tan_type, int draw_type, int after_type, int density, int value, float half_diameter, int endTime);

	~tan_drawing();

	void make();

	void move();

	int get_state();

	int get_density();

	void set_stat(int stat);

	void add_prevMarkedTime();

	int get_prevMarkedTime();

	void set_parent_tanmak(int tag);

	void set_tag(int tag);

	int get_tag();

	void set_center(cocos2d::Point pos);

	int get_parent_tag();

	void set_additional_info(float speed, int angle, float angle_acc);

	void removecurrentTan();
};
#endif