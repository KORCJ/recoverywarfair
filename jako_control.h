/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef _jako_control_h_
#define _jako_control_h_
#include <cocos2d.h>

class mainRogic;

class jako_control{
private:
	int state, gaved_damage, jako_type, jako_number, prev_shoted_tan, prev_shoted_sub_tan, tag;
	int grade_var, fat_var;
	cocos2d::Point start_position, dst_position, prev_position;
	float speed;
	cocos2d::Sprite *jako_sprite;
	std::vector<int> child_tanmak;
	mainRogic *forRun;
	char png_name[25];
	float radian;
	bool wasInside;
	cocos2d::Rect dst_rect;
public:
	cocos2d::Sprite *getSprite();

	void move();

	jako_control(mainRogic *forRun, cocos2d::Point start_position, cocos2d::Point dst_position, float speed, short jako_type, short jako_number);

	~jako_control();

	int get_state();

	void set_state(int state);

	void add_damage(int shot_tan_number);

	void add_sub_damage(int shot_tan_number);

	void bomb_damage(int delta);

	void clear_shoted_number();

	int get_prev_shot_tan_number();

	int get_prev_shot_sub_tan_number();

	void add_child_tanmak(int tag);

	void delete_child_tanmak(int *tag);

	void set_tag(int tag);

	void launch_particle(int type);

	int get_tag();
};

#endif