/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "LocalPreferenceManager.h"

USING_NS_CC;

LocalPrefernceData* LocalPrefernceData::_instance = NULL;

LocalPrefernceData::LocalPrefernceData(){
	CCUserDefault *tempUserDefault = CCUserDefault::sharedUserDefault();

	setter_BGM = tempUserDefault->getBoolForKey(getLocalPrefernceKey[Prefernce_playBgm], true);
	setter_SFX = tempUserDefault->getBoolForKey(getLocalPrefernceKey[Prefernce_playSfx], true);
	setter_VIBRATE = tempUserDefault->getBoolForKey(getLocalPrefernceKey[Prefernce_playVibrate], true);

}

LocalPrefernceData::~LocalPrefernceData(){

}

void LocalPrefernceData::setOption(){
	CCUserDefault *tempUserDefault = CCUserDefault::sharedUserDefault();
	tempUserDefault->setBoolForKey(getLocalPrefernceKey[Prefernce_playBgm], setter_BGM);
	tempUserDefault->setBoolForKey(getLocalPrefernceKey[Prefernce_playSfx], setter_SFX);
	tempUserDefault->setBoolForKey(getLocalPrefernceKey[Prefernce_playVibrate], setter_VIBRATE);

	tempUserDefault->flush();
}
void LocalPrefernceData::setOption(bool bgm, bool sfx, bool vibrate){
	this->setforBGM(bgm);
	this->setforSFX(sfx);
	this->setforVIBRATE(vibrate);
	this->setOption();
}