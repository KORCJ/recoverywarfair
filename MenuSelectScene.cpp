/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "MenuSelectScene.h"
#include "SimpleAudioEngine.h" 

USING_NS_CC;

Scene* MenuSelectScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = MenuSelectScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool MenuSelectScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!LayerColor::initWithColor(Color4B(0, 0, 0, 0)))
	{
		return false;
	}

	//여기부터 시작
	////
	//화면크기를 미리 받아놓는다.
	Director::getInstance()->getTextureCache()->destroyInstance();
	Director::getInstance()->getTextureCache()->removeAllTextures();
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("option_reso.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("stage_selt.plist");
	pref_data = LocalPrefernceData();

	char soundLocation[100] = { 0, };

	if(pref_data.getforBGM()){
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
		sprintf(soundLocation,"sdForIOS/title_bg.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		sprintf(soundLocation, "sdForAndroid/title_bg.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		sprintf(soundLocation, "sdForPC/title_bg.wav");
#endif
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(soundLocation,true);
	}

	if (pref_data.getforSFX()){
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
		sprintf(soundLocation, "sdForIOS/click.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		sprintf(soundLocation, "sdForAndroid/click.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		sprintf(soundLocation, "sdForPC/click.wav");
#endif
		CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect(soundLocation);
	}

	screenSize = Size(768.0f, 1024.0f);
	selt_num = 0;
	flag_3 = false;
	//
	//버튼 처리 on
	this->setKeypadEnabled(true);

	pointer_sprite = Sprite::create();

	pointer_sprite->setPosition(Point::ZERO);

	///
	auto bg_sprite = Sprite::create("mainBg/Comp 1_00000.png");

	bg_sprite->setPosition(Point(screenSize.width * 0.5, screenSize.height * 0.5));
	bg_sprite->setScale(2.0f);

	this->addChild(bg_sprite);
	
	auto bg_animation = Animation::create();
	bg_animation->setDelayPerUnit(0.03);
	char temp_spriteName[100] = { 0, };
	for (int i = 0; i <= 20; i++){
		sprintf(temp_spriteName, "mainBg/Comp 1_%05d.png", i);
		bg_animation->addSpriteFrameWithFileName(temp_spriteName);
	}
	for (int i = 20; i >= 0 ; i--){
		sprintf(temp_spriteName, "mainBg/Comp 1_%05d.png", i);
		bg_animation->addSpriteFrameWithFileName(temp_spriteName);
	}
	bg_animation->setLoops(-1);
	bg_sprite->runAction(Animate::create(bg_animation));
	
	///
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("title.plist");

	auto mainRogo = Sprite::createWithSpriteFrameName("TITLE_00000.png");

	mainRogo->setPosition(Point(screenSize.width * 0.5, screenSize.height * 0.8f));

	pointer_sprite->addChild(mainRogo);

	auto logo_animation = Animation::create();
	logo_animation->setDelayPerUnit(0.03);
	for (int i = 0; i <= 69; i++){
		sprintf(temp_spriteName, "TITLE_%05d.png", i);
		logo_animation->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_spriteName));
	}
	logo_animation->setLoops(-1);
	mainRogo->runAction(Animate::create(logo_animation));

	//
	auto start = MenuItemImage::create("select_menu_START.png", "select_menu_START.png", CC_CALLBACK_1(MenuSelectScene::start_func, this));

	start->setPosition(Point(screenSize.width * 0.5, screenSize.height * 0.5f));

	//
	/*
	auto collection = MenuItemImage::create("select_menu_COLLECTION.png", "select_menu_COLLECTION.png", CC_CALLBACK_1(MenuSelectScene::collection_func, this));

	collection->setPosition(Point(screenSize.width * 0.5, screenSize.height * 0.45f));

	//
	auto ranking = MenuItemImage::create("select_menu_RANKING.png", "select_menu_RANKING.png", CC_CALLBACK_1(MenuSelectScene::ranking_func, this));

	ranking->setPosition(Point(screenSize.width * 0.5, screenSize.height * 0.35f));
	*/
	//
	auto option = MenuItemImage::create("select_menu_OPTION.png", "select_menu_OPTION.png", CC_CALLBACK_1(MenuSelectScene::option_func, this));

	option->setPosition(Point(screenSize.width * 0.5, screenSize.height * 0.35f));

	////
	auto menu = Menu::create(start, option, NULL);

	menu->setPosition(Point::ZERO);

	pointer_sprite->addChild(menu);

	this->addChild(pointer_sprite, 0, "pointer_sprite");

	/////////
	_pointer_sprite = Sprite::create();

	_pointer_sprite->setPosition(Point(screenSize.width, 0));

	///

	///백그라운드 이미지

	//캐릭터 선택
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("cha_selt.plist");

	//
	auto mainBG = Sprite::createWithSpriteFrameName("chaselt_back.png");

	mainBG->setPosition(Point(screenSize.width * 0.5, screenSize.height * 0.465f));

	_pointer_sprite->addChild(mainBG);

	auto succle = Sprite::createWithSpriteFrameName("succle.png");

	succle->setPosition(Point(screenSize.width * 0.685, screenSize.height * 0.693f));

	succle->runAction(RepeatForever::create(RotateBy::create(10, 360)));

	_pointer_sprite->addChild(succle);

	//
	auto f_back = MenuItemImage::create("chaselt_back.png", "chaselt_back.png", CC_CALLBACK_1(MenuSelectScene::back_func, this));

	f_back->setPosition(Point(screenSize.width * 0.884, screenSize.height * 0.022f));

	//각 기체별 선택
	auto cha_selt_base = Sprite::createWithSpriteFrameName("type_selt_box.png");

	cha_selt_base->setPosition(Point(screenSize.width * 0.500, screenSize.height * 0.169f));

	_pointer_sprite->addChild(cha_selt_base, -2);

	auto cha_selt_base_type = Sprite::createWithSpriteFrameName("type_base.png");

	cha_selt_base_type->setPosition(Point(screenSize.width * 0.521, screenSize.height * 0.169f));

	_pointer_sprite->addChild(cha_selt_base_type);
	
	auto vsuri_selt_btn = MenuItemSprite::create(Sprite::createWithSpriteFrameName("vsuri_selt.png"), Sprite::createWithSpriteFrameName("vsuri_selt.png"), CC_CALLBACK_1(MenuSelectScene::selt_1_func, this));

	vsuri_selt_btn->setPosition(Point(screenSize.width * 0.175, screenSize.height * 0.170f));

	vsuri_selt_btn->setOpacity(0.1f);

	vsuri_selt_btn->setTag(1);

	auto never_selt_btn = MenuItemSprite::create(Sprite::createWithSpriteFrameName("never_selt.png"), Sprite::createWithSpriteFrameName("never_selt.png"), CC_CALLBACK_1(MenuSelectScene::selt_2_func, this));

	never_selt_btn->setPosition(Point(screenSize.width * 0.504, screenSize.height * 0.164f));

	never_selt_btn->setOpacity(0.1f);

	never_selt_btn->setTag(2);

	auto byak_selt_btn = MenuItemSprite::create(Sprite::createWithSpriteFrameName("byak_selt.png"), Sprite::createWithSpriteFrameName("byak_selt.png"), CC_CALLBACK_1(MenuSelectScene::selt_3_func, this));

	byak_selt_btn->setPosition(Point(screenSize.width * 0.835, screenSize.height * 0.163f));

	byak_selt_btn->setOpacity(0.1f);

	byak_selt_btn->setTag(3);

	///상단 문구
	auto cha_selt_txt = Sprite::createWithSpriteFrameName("text main.png");
	cha_selt_txt->setPosition(Point(screenSize.width * 0.598f, screenSize.height * 0.497f));
	_pointer_sprite->addChild(cha_selt_txt);

	///
	//조종사 원화 스프라이트
	auto cha_selt_pic = Sprite::create();
	_pointer_sprite->addChild(cha_selt_pic, 0, "cha_selt_pic");

	//선택 빡스 스프라이트
	auto type_selt = Sprite::createWithSpriteFrameName("type_selt.png");
	type_selt->setPosition(Point(screenSize.width * 0.500, screenSize.height * 0.169f));
	type_selt->setOpacity(0.0f);

	//셀렉트 버턴 스프라이트르트흐르으흐르으흐를으으흐르르으르ㅡ으
	auto select_btn = MenuItemSprite::create(Sprite::createWithSpriteFrameName("select1-1.png"), Sprite::createWithSpriteFrameName("select1-2.png"), CC_CALLBACK_1(MenuSelectScene::selt_func, this));
	select_btn->setPosition(Point(screenSize.width * 0.500f, screenSize.height * 0.357f));
	select_btn->setName("select_btn");
	select_btn->setOpacity(0.0f);

	////
	auto _menu = Menu::create(f_back, vsuri_selt_btn, never_selt_btn, byak_selt_btn, select_btn, NULL);

	_menu->setPosition(Point::ZERO);

	_pointer_sprite->addChild(type_selt, -1, "type_selt");

	_pointer_sprite->addChild(_menu, 0 ,"cha_selt_menu");

	this->addChild(_pointer_sprite, 0 ,"_pointer_sprite");

	/////////
	option_pointer_sprite = Sprite::create();

	option_pointer_sprite->setPosition(Point(-768.0f, 0.0f));

	this->addChild(option_pointer_sprite);

	auto optionTitle = Sprite::createWithSpriteFrameName("option.png");

	optionTitle->setPosition(Point(screenSize.width * 0.5f, screenSize.height * 0.96f));

	option_pointer_sprite->addChild(optionTitle);

	auto optionLine = Sprite::createWithSpriteFrameName("line.png");

	optionLine->setPosition(Point(screenSize.width * 0.5f, screenSize.height * 0.96f));

	option_pointer_sprite->addChild(optionLine);

	auto optionBg = Sprite::create("option_bg.png");

	optionBg->setPosition(Point(screenSize.width * 0.5f, screenSize.height * 0.462f));

	option_pointer_sprite->addChild(optionBg);

	auto optionString = Sprite::createWithSpriteFrameName("vibration.png");

	optionString->setPosition(Point(screenSize.width * 0.313f, screenSize.height * 0.529f));

	option_pointer_sprite->addChild(optionString);

	auto option_back = MenuItemImage::create("chaselt_back.png", "chaselt_back.png", CC_CALLBACK_1(MenuSelectScene::back_func, this));

	option_back->setPosition(Point(screenSize.width * 0.884, screenSize.height * 0.022f));

	char pref_info_string[20] = { 0, };
	//브금
	if (pref_data.getforBGM())
		sprintf(pref_info_string, "on.png");
	else
		sprintf(pref_info_string, "off.png");
	auto option_bgmbtn = MenuItemSprite::create(Sprite::createWithSpriteFrameName(pref_info_string), Sprite::createWithSpriteFrameName(pref_info_string), CC_CALLBACK_1(MenuSelectScene::set_Prefernce_func,this));
	option_bgmbtn->setPosition(Point(screenSize.width * 0.673f, screenSize.height * 0.601f));
	option_bgmbtn->setTag(1);
	//효과음
	if (pref_data.getforSFX())
		sprintf(pref_info_string, "on.png");
	else
		sprintf(pref_info_string, "off.png");
	auto option_sfxbtn = MenuItemSprite::create(Sprite::createWithSpriteFrameName(pref_info_string), Sprite::createWithSpriteFrameName(pref_info_string), CC_CALLBACK_1(MenuSelectScene::set_Prefernce_func, this));
	option_sfxbtn->setPosition(Point(screenSize.width * 0.673f, screenSize.height * 0.532f));
	option_sfxbtn->setTag(2);
	//진동
	if (pref_data.getforVIBRATE())
		sprintf(pref_info_string, "on.png");
	else
		sprintf(pref_info_string, "off.png");
	auto option_vibratebtn = MenuItemSprite::create(Sprite::createWithSpriteFrameName(pref_info_string), Sprite::createWithSpriteFrameName(pref_info_string), CC_CALLBACK_1(MenuSelectScene::set_Prefernce_func, this));
	option_vibratebtn->setPosition(Point(screenSize.width *0.673f, screenSize.height * 0.461f));
	option_vibratebtn->setTag(3);

	auto option_menu = Menu::create(option_bgmbtn, option_sfxbtn, option_vibratebtn, option_back, NULL);
	option_menu->setPosition(Point::ZERO);

	option_pointer_sprite->addChild(option_menu);
	////////
	stage_selt_sprite = Sprite::create();
	stage_selt_sprite->setPosition(Point(768.0f * 3, 0.0f));
	this->addChild(stage_selt_sprite, 0, "stage_selt_sprite");

	auto stage_selt_menu = Menu::create();
	stage_selt_menu->setPosition(Point::ZERO);
	stage_selt_sprite->addChild(stage_selt_menu, 1, "stage_selt_menu");
	char temp_btn_name[30] = { 0, };

	auto stage_selt_title = Sprite::createWithSpriteFrameName("stage_selt_title.png");
	stage_selt_title->setPosition(Point(screenSize.width * 0.5f, screenSize.height * 0.96f));
	stage_selt_sprite->addChild(stage_selt_title);

	for (int i = 1; i <= 2; i++){
		sprintf(temp_btn_name, "stage_selt_stage%d.png", i);
		auto stageBtns = Sprite::createWithSpriteFrameName(temp_btn_name);
		stageBtns->setPosition(Point(screenSize.width * 0.495f, screenSize.height * (0.845f - (i - 1) * 0.14)));
		stage_selt_sprite->addChild(stageBtns, 1);
	}
	for (int i = 3; i <= 6; i++){
		auto stageBtns = Sprite::createWithSpriteFrameName("stage_selt_locked.png");
		stageBtns->setPosition(Point(screenSize.width * 0.495f, screenSize.height * (0.845f - (i - 1) * 0.14)));
		stage_selt_sprite->addChild(stageBtns, 1);
	}
	for (int i = 1; i <= 2; i++){
		auto menu_Items = MenuItemSprite::create(Sprite::createWithSpriteFrameName("stage_selt_selted.png"), Sprite::createWithSpriteFrameName("stage_selt_selted.png"), CC_CALLBACK_1(MenuSelectScene::stage_selt, this));
		menu_Items->setPosition(Point(screenSize.width * 0.495f, screenSize.height * (0.845f - (i - 1) * 0.14)));
		menu_Items->setOpacity(0.0f);
		menu_Items->setTag(i);
		stage_selt_menu->addChild(menu_Items);
	}
	auto stage_selt_back = MenuItemImage::create("chaselt_back.png", "chaselt_back.png", CC_CALLBACK_1(MenuSelectScene::back_func, this));
	stage_selt_back->setPosition(Point(screenSize.width * 0.884, screenSize.height * 0.022f));
	stage_selt_menu->addChild(stage_selt_back);

	auto stage_selt_Line = Sprite::createWithSpriteFrameName("line.png");
	stage_selt_Line->setPosition(Point(screenSize.width * 0.5f, screenSize.height * 0.96f));
	stage_selt_sprite->addChild(stage_selt_Line);

	auto stage_selt_Bg = Sprite::create("option_bg.png");
	stage_selt_Bg->setPosition(Point(screenSize.width * 0.5f, screenSize.height * 0.462f));
	stage_selt_sprite->addChild(stage_selt_Bg);

	this->scheduleUpdate();

	return true;
}

void MenuSelectScene::start_func(Object *pSender){
	if (pref_data.getforSFX()){
		char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
		sprintf(soundLocation, "sdForIOS/click.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		sprintf(soundLocation, "sdForAndroid/click.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		sprintf(soundLocation, "sdForPC/click.wav");
#endif
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(soundLocation);
	}
	////////
	_pointer_sprite->runAction(MoveTo::create(0.2f, Point::ZERO));

	pointer_sprite->runAction(MoveTo::create(0.2f, Point(-1 * screenSize.width, 0)));
//	auto sub_pointer = Sprite::create("pointer.png");

//	sub_pointer->setPosition(Point(screenSize.width, 0));


}

void MenuSelectScene::stage_selt(cocos2d::Object *pSender){
	if (((MenuItemSprite*) pSender)->getOpacity() >= 255.0f){
	//게임시작
		if (selt_num != 0){
			CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
			Director::getInstance()->replaceScene(mainRogic::createScene(selt_num, ((MenuItemSprite*) pSender)->getTag()));
			return;
		}
	}
	else{
		for (int i = 1; i <= 2; i++){
			((MenuItemSprite*)this->getChildByName("stage_selt_sprite")->getChildByName("stage_selt_menu")->getChildByTag(i))->setOpacity(0.0f);
		}
		((MenuItemSprite*) pSender)->setOpacity(255.0f);
	}
}

void MenuSelectScene::collection_func(Object *pSender){
	return;
}

void MenuSelectScene::ranking_func(Object *pSender){
	return;
}

void MenuSelectScene::option_func(Object *pSender){
	pointer_sprite->runAction(MoveTo::create(0.2f, Point(768.0f, 0.0f)));
	option_pointer_sprite->runAction(MoveTo::create(0.2f, Point::ZERO));
}
void MenuSelectScene::set_Prefernce_func(cocos2d::Object *pSender){
	char pref_info_string[100] = { 0, };
	
	switch (((MenuItemSprite*) pSender)->getTag()){
	case 1:{
			   pref_data.setforBGM(!pref_data.getforBGM());
			   //브금
			   if (pref_data.getforBGM()){
				   sprintf(pref_info_string, "on.png");
				   char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
				   sprintf(soundLocation, "sdForIOS/title_bg.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
				   sprintf(soundLocation, "sdForAndroid/title_bg.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
				   sprintf(soundLocation, "sdForPC/title_bg.wav");
#endif
				   CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(soundLocation, true);
			   }
			   else{
				   sprintf(pref_info_string, "off.png");
				   CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(false);
			   }
			   ((MenuItemSprite*) pSender)->setNormalImage(Sprite::createWithSpriteFrameName(pref_info_string));
			   ((MenuItemSprite*) pSender)->setSelectedImage(Sprite::createWithSpriteFrameName(pref_info_string));
	}break;
	case 2:{
			   pref_data.setforSFX(!pref_data.getforSFX());
			   //효과음
			   if (pref_data.getforSFX())
				   sprintf(pref_info_string, "on.png");
			   else
				   sprintf(pref_info_string, "off.png");
			   ((MenuItemSprite*) pSender)->setNormalImage(Sprite::createWithSpriteFrameName(pref_info_string));
			   ((MenuItemSprite*) pSender)->setSelectedImage(Sprite::createWithSpriteFrameName(pref_info_string));
	}break;
	case 3:{
			   pref_data.setforVIBRATE(!pref_data.getforVIBRATE());
			   //진동
			   if (pref_data.getforVIBRATE())
				   sprintf(pref_info_string, "on.png");
			   else
				   sprintf(pref_info_string, "off.png");
			   ((MenuItemSprite*) pSender)->setNormalImage(Sprite::createWithSpriteFrameName(pref_info_string));
			   ((MenuItemSprite*) pSender)->setSelectedImage(Sprite::createWithSpriteFrameName(pref_info_string));
	}
	}
	pref_data.setOption();
	return;
}

void MenuSelectScene::back_func(Object *pSender){
	if (pointer_sprite->getPosition() == Point(-768.0f, 0)){
		if (stage_selt_sprite->getPosition() == Point::ZERO){
			stage_selt_sprite->runAction(MoveTo::create(0.2f, Point(screenSize.width * 3, 0.0f)));
			_pointer_sprite->runAction(MoveTo::create(0.2f, Point::ZERO));
		}
		else{
			_pointer_sprite->runAction(MoveTo::create(0.2f, Point(screenSize.width, 0)));

			pointer_sprite->runAction(MoveTo::create(0.2f, Point::ZERO));
		}
	}

	else if (pointer_sprite->getPosition() == Point(768.0f, 0)){
		pointer_sprite->runAction(MoveTo::create(0.2f, Point::ZERO));
		option_pointer_sprite->runAction(MoveTo::create(0.2f, Point(-768.0f, 0.0f)));
	}

	return;
}
//175 504 835
void MenuSelectScene::selt_1_func(cocos2d::Object *pSender){
	if (selt_num == 1)
		return;
	if (pref_data.getforSFX()){
		char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
		sprintf(soundLocation, "sdForIOS/click.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		sprintf(soundLocation, "sdForAndroid/click.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		sprintf(soundLocation, "sdForPC/click.wav");
#endif
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(soundLocation);
	}
	((Sprite*) pSender)->setOpacity(255.0f);
	_pointer_sprite->getChildByName("cha_selt_menu")->getChildByTag(2)->setOpacity(0.1f);
	_pointer_sprite->getChildByName("cha_selt_menu")->getChildByTag(3)->setOpacity(0.1f);
	call_type_info(1);

	((Sprite*)_pointer_sprite->getChildByName("cha_selt_pic"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("vsuri.png"));
	((Sprite*)_pointer_sprite->getChildByName("cha_selt_pic"))->setPosition(Point(screenSize.width * 0.694, screenSize.height * 0.675f));
	
	((Sprite*)_pointer_sprite->getChildByName("type_selt"))->setOpacity(255.0f);
	_pointer_sprite->getChildByName("type_selt")->runAction(MoveTo::create(0.1f, Point(screenSize.width * 0.175, screenSize.height * 0.169f)));

	_pointer_sprite->getChildByName("cha_selt_menu")->getChildByName("select_btn")->setOpacity(255.0f);

	selt_num = 1;
}

void MenuSelectScene::selt_2_func(cocos2d::Object *pSender){
	if (selt_num == 3)
		return;
	if (pref_data.getforSFX()){
		char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
		sprintf(soundLocation, "sdForIOS/click.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		sprintf(soundLocation, "sdForAndroid/click.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		sprintf(soundLocation, "sdForPC/click.wav");
#endif
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(soundLocation);
	}

	_pointer_sprite->getChildByName("cha_selt_menu")->getChildByTag(1)->setOpacity(0.1f);
	((Sprite*) pSender)->setOpacity(255.0f);
	_pointer_sprite->getChildByName("cha_selt_menu")->getChildByTag(3)->setOpacity(0.1f);
	call_type_info(2);

	((Sprite*)_pointer_sprite->getChildByName("cha_selt_pic"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("never.png"));
	((Sprite*)_pointer_sprite->getChildByName("cha_selt_pic"))->setPosition(Point(screenSize.width * 0.691, screenSize.height * 0.674f));

	((Sprite*)_pointer_sprite->getChildByName("type_selt"))->setOpacity(255.0f);
	_pointer_sprite->getChildByName("type_selt")->runAction(MoveTo::create(0.1f, Point(screenSize.width * 0.504f, screenSize.height * 0.17f)));

	_pointer_sprite->getChildByName("cha_selt_menu")->getChildByName("select_btn")->setOpacity(255.0f);

	selt_num = 3;
}

void MenuSelectScene::selt_3_func(cocos2d::Object *pSender){
	if (selt_num == 2)
		return;
	if (pref_data.getforSFX()){
		char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
		sprintf(soundLocation, "sdForIOS/click.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		sprintf(soundLocation, "sdForAndroid/click.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		sprintf(soundLocation, "sdForPC/click.wav");
#endif
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(soundLocation);
	}
	_pointer_sprite->getChildByName("cha_selt_menu")->getChildByTag(1)->setOpacity(0.1f);
	_pointer_sprite->getChildByName("cha_selt_menu")->getChildByTag(2)->setOpacity(0.1f);
	((Sprite*) pSender)->setOpacity(255.0f);
	call_type_info(3);

	((Sprite*)_pointer_sprite->getChildByName("cha_selt_pic"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("byak.png"));
	((Sprite*)_pointer_sprite->getChildByName("cha_selt_pic"))->setPosition(Point(screenSize.width * 0.687, screenSize.height * 0.695f));
	
	((Sprite*)_pointer_sprite->getChildByName("type_selt"))->setOpacity(255.0f);
	_pointer_sprite->getChildByName("type_selt")->runAction(MoveTo::create(0.1f, Point(screenSize.width * 0.832f, screenSize.height * 0.17f)));
	
	_pointer_sprite->getChildByName("cha_selt_menu")->getChildByName("select_btn")->setOpacity(255.0f);

	selt_num = 2;
	/*
	selt_num = 3;
	if (!flag_3){
		//
		auto f_name = Sprite::create("chaselt_balyack/balyack.png");
		f_name->setPosition(Point(screenSize.width * 0.511, screenSize.height * 0.896));
		_pointer_sprite->addChild(f_name);
		//
		auto preview = Sprite::create("chaselt_balyack/preview.png");
		preview->setPosition(Point(screenSize.width * 0.816, screenSize.height * 0.133));
		_pointer_sprite->addChild(preview);
		//
		auto rawimg = Sprite::create("chaselt_balyack/rawimg.png");
		rawimg->setPosition(Point(screenSize.width * 0.626, screenSize.height * 0.632));
		_pointer_sprite->addChild(rawimg);
		//
		auto f_type = Sprite::create("chaselt_balyack/type.png");
		f_type->setPosition(Point(screenSize.width * 0.238, screenSize.height * 0.383));
		_pointer_sprite->addChild(f_type);
		//
		auto selected = MenuItemImage::create("chaselt_balyack/selected.png", "chaselt_balyack/selected.png", CC_CALLBACK_1(MenuSelectScene::selt_3_func, this));
		selected->setPosition(Point(screenSize.width * 0.34, screenSize.height * 0.073));

		auto _menu = Menu::create(selected, NULL);

		_menu->setPosition(Point::ZERO);

		_pointer_sprite->addChild(_menu);

		flag_3 = true;
	}
	*/
}

void MenuSelectScene::onKeyReleased(cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event){
	if (keycode == EventKeyboard::KeyCode::KEY_BACK || keycode == EventKeyboard::KeyCode::KEY_ESCAPE) {
		if (pointer_sprite->getPosition() == Point(0, 0))
			Director::getInstance()->end();
		else
			back_func(this);
	}
}

void MenuSelectScene::selt_func(cocos2d::Object *pSender){
	_pointer_sprite->runAction(MoveTo::create(0.3f, Point(-768.0f, 0.0f)));
	stage_selt_sprite->runAction(MoveTo::create(0.3f, Point::ZERO));
}

void MenuSelectScene::call_type_info(int type_num){
	Sprite* type_info;
	if (_pointer_sprite->getChildByName("type_info") == NULL){
		type_info = Sprite::create();
		type_info->setName("type_info");
	}
	else
		type_info = ((Sprite*)_pointer_sprite->getChildByName("type_info"));
	switch (type_num){
	case 1:{type_info->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("type-vsuri.png")); } break;
	case 2:{type_info->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("type-never.png")); } break;
	case 3:{type_info->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("type-byak.png")); } break;
	}
	if (_pointer_sprite->getChildByName("type_info") == NULL){
		_pointer_sprite->addChild(type_info, 5);
	}
	type_info->setPosition(Point(screenSize.width * -0.261, screenSize.height * 0.537f));
	type_info->runAction(Sequence::create(MoveTo::create(0.1f, Point(screenSize.width * 0.261, screenSize.height * 0.537f)), Blink::create(0.3f, 10), NULL));
}

void MenuSelectScene::update(){
	if (pref_data.getforBGM() && !CocosDenshion::SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying()){
			CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
	}
}