/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include"jako_control.h"
#include "mainRogic.h"
#include "player_tan_cotrol.h"
#include "sub_player_control.h"
#include "neverb_tan_control.h"
USING_NS_CC;

jako_control::jako_control(mainRogic *forRun, cocos2d::Point start_position, cocos2d::Point dst_position, float speed, short jako_type, short jako_number){
	jako_control::forRun = forRun;
	jako_control::start_position = start_position;
	jako_control::dst_position = dst_position;
	jako_control::speed = speed;
	sprintf(png_name, "jakos/jako_%d_%d_1.png", jako_number, jako_type);
	jako_control::jako_type = jako_type;
	jako_control::jako_number = jako_number;
	jako_control::jako_sprite = Sprite::create(png_name);
//	jako_control::jako_sprite->setScale(0.4f);
	forRun->addChild(jako_sprite,1);
	jako_control::jako_sprite->setPosition(start_position);
	jako_control::state = 2;
	jako_control::prev_position = start_position;
	jako_control::gaved_damage = 1;
	if (dst_position.x < start_position.x)
		jako_control::radian = atan2f(abs(start_position.y - dst_position.y), abs(start_position.x - dst_position.x)) + 1.0f * M_PI;
	else
		jako_control::radian = atan2f(abs(start_position.x - dst_position.x), abs(start_position.y - dst_position.y)) + 1.5f * M_PI;
	wasInside = false;
	if (1 <= jako_number && jako_number <= 29)
		grade_var = forRun->jako_hps[jako_number] / (float) 3;
	else if (30 <= jako_number && jako_number <= 39)
		grade_var = forRun->jako_hps[jako_number] / (float) 6;
	else
		grade_var = forRun->jako_hps[jako_number] / (float) 8;
	fat_var = 1;

	dst_rect = Rect(dst_position.x - 4.0f, dst_position.y - 4.0f, 8.0f, 8.0f);
}

jako_control::~jako_control(){
	forRun->jako_stop_tanmak(&child_tanmak);
	jako_control::jako_sprite->removeFromParentAndCleanup(true);
}

int jako_control::get_state(){
	return state;
}

void jako_control::set_state(int state){
	jako_control::state = state;
}

void jako_control::move(){
	if (state == 2){
		jako_sprite->setPosition(Point(jako_sprite->getPositionX() + speed * cosf(radian), jako_sprite->getPositionY() + speed*sinf(radian)));
		jako_sprite->setRotation(-1 * radian * 180.0f / M_PI - 90.0f);
		if(dst_rect.containsPoint(jako_sprite->getPosition())&& state == 2){
			state = 1;
		}
		if ((0 > jako_sprite->getPositionX() || 768 < jako_sprite->getPositionX() || 0 > jako_sprite->getPositionY() || 1024 < jako_sprite->getPositionY()) && state == 2){
			if (wasInside)
				state = 0;
		}
		else
			wasInside = true;
	}
}

cocos2d::Sprite *jako_control::getSprite(){
	return jako_sprite;
}

void jako_control::add_damage(int shot_tan_number){
	jako_control::prev_shoted_tan = shot_tan_number;
	switch (jako_type){
	case 1:gaved_damage += 24; break;
	case 2:gaved_damage += 6; break;
	case 3:gaved_damage += 5; break;
	}
	if (gaved_damage >= forRun->jako_hps[jako_number]){
		if (state != 0){
			launch_particle(0);
			forRun->add_combo();
			forRun->acc_fever();
			forRun->add_score(jako_number);
		}
		state = 0;
		forRun->jako_stop_tanmak(&child_tanmak);
		return;
	}
	char particleName[30] = { 0, };
	switch (forRun->player_type){
	case 1:{sprintf(particleName, "particle/vsuri_attack.plist"); }break;
	case 2:{sprintf(particleName, "particle/balyak_attack.plist"); }break;
	case 3:{sprintf(particleName, "particle/never_attack.plist"); }break;
	}
	CCParticleSystem* particleSystem = CCParticleSystemQuad::create(particleName);

	particleSystem->setPosition(forRun->player_tan->get_playerTanVector().at(shot_tan_number)->getPosition() + Point(0, 80.0f));
	particleSystem->setAutoRemoveOnFinish(true);
	if (1 <= jako_number && jako_number <= 29)
		particleSystem->setScale(0.6f);
	else if (30 <= jako_number && jako_number <= 39)
		particleSystem->setScale(0.9f);
	else
		particleSystem->setScale(1.0f);
	forRun->addChild(particleSystem, 5);
	particleSystem = NULL;
	if ((gaved_damage / grade_var) + 1 > fat_var){
		/////////////////////////////
		/////////////////////////////
		fat_var = (gaved_damage / grade_var) + 1;
		char png_name[30];
		//launch particle
		launch_particle(1);
		if (1 <= jako_number && jako_number <= 29 && fat_var <= 3){
			sprintf(png_name, "jakos/jako_%d_%d_%d.png", jako_number, jako_type, fat_var);
			jako_control::jako_sprite->setTexture(Director::getInstance()->getTextureCache()->addImage(png_name));
		}
		else if (30 <= jako_number && jako_number <= 39 && fat_var <= 6){
			sprintf(png_name, "jakos/jako_%d_%d_%d.png", jako_number, jako_type, fat_var);
			jako_control::jako_sprite->setTexture(Director::getInstance()->getTextureCache()->addImage(png_name));
		}
		else if (jako_number >= 40 && fat_var <= 8){
			sprintf(png_name, "jakos/jako_%d_%d_%d.png", jako_number, jako_type, fat_var);
			jako_control::jako_sprite->setTexture(Director::getInstance()->getTextureCache()->addImage(png_name));
		}
	}
}

void jako_control::add_sub_damage(int shot_tan_number){
	jako_control::prev_shoted_sub_tan = shot_tan_number;
	switch (jako_type){
	case 2:gaved_damage += 2; break;
	case 3:gaved_damage += 5; break;
	}
	if (gaved_damage >= forRun->jako_hps[jako_number]){
		if (state != 0){
			launch_particle(0);
			forRun->add_combo();
			forRun->acc_fever();
			forRun->add_score(jako_number);
		}
		state = 0;
		forRun->jako_stop_tanmak(&child_tanmak);
		return;
	}
	if (jako_type == 2){
		/////////////////////////////
		char particleName[30] = { 0, };
		switch (forRun->player_type){
		case 1:{sprintf(particleName, "particle/vsuri_attack.plist"); }break;
		case 2:{sprintf(particleName, "particle/balyak_attack.plist"); }break;
		case 3:{sprintf(particleName, "particle/never_attack.plist"); }break;
		}
		CCParticleSystem* particleSystem = CCParticleSystemQuad::create(particleName);
		particleSystem->setScale(0.5f);
		particleSystem->setPosition(forRun->sub_player->get_subTanVector(shot_tan_number / forRun->sub_player->get_value()).at(shot_tan_number % forRun->sub_player->get_value())->getPosition());
		particleSystem->setAutoRemoveOnFinish(true);
		if (1 <= jako_number && jako_number <= 29)
			particleSystem->setScale(0.3f);
		else if (30 <= jako_number && jako_number <= 39)
			particleSystem->setScale(0.5f);
		else
			particleSystem->setScale(0.7f);
		forRun->addChild(particleSystem, 5);
		particleSystem = NULL;
	}
	else if (jako_type == 3){
		char particleName[30] = { 0, };
		switch (forRun->player_type){
		case 1:{sprintf(particleName, "particle/vsuri_attack.plist"); }break;
		case 2:{sprintf(particleName, "particle/balyak_attack.plist"); }break;
		case 3:{sprintf(particleName, "particle/never_attack.plist"); }break;
		}
		CCParticleSystem* particleSystem = CCParticleSystemQuad::create(particleName);

		particleSystem->setPosition(forRun->neverb_sub_player->get_playerTanVector().at(shot_tan_number)->getPosition());
		if (1 <= jako_number && jako_number <= 29)
			particleSystem->setScale(0.3f);
		else if (30 <= jako_number && jako_number <= 39)
			particleSystem->setScale(0.5f);
		else
			particleSystem->setScale(0.7f);
		particleSystem->setAutoRemoveOnFinish(true);
		forRun->addChild(particleSystem, 5);
		particleSystem = NULL;
	}
	if ((gaved_damage / grade_var) + 1 > fat_var){
		/////////////////////////////
		//launch particle
		launch_particle(2);
		fat_var = (gaved_damage / grade_var) + 1;
		char png_name[30];
		if (1 <= jako_number && jako_number <= 29 && fat_var <= 3){
			sprintf(png_name, "jakos/jako_%d_%d_%d.png", jako_number, jako_type, fat_var);
			jako_control::jako_sprite->setTexture(Director::getInstance()->getTextureCache()->addImage(png_name));
		}
		else if (30 <= jako_number && jako_number <= 39 && fat_var <= 6){
			sprintf(png_name, "jakos/jako_%d_%d_%d.png", jako_number, jako_type, fat_var);
			jako_control::jako_sprite->setTexture(Director::getInstance()->getTextureCache()->addImage(png_name));
		}
		else if (jako_number >= 40 && fat_var <= 8){
			sprintf(png_name, "jakos/jako_%d_%d_%d.png", jako_number, jako_type, fat_var);
			jako_control::jako_sprite->setTexture(Director::getInstance()->getTextureCache()->addImage(png_name));
		}
	}
}

void jako_control::bomb_damage(int delta){
	gaved_damage += delta;
	if (gaved_damage >= forRun->jako_hps[jako_number]){
		if (state != 0){
			launch_particle(0);
			forRun->add_combo();
			forRun->acc_fever();
			forRun->add_score(jako_number);
		}
		state = 0;
		forRun->jako_stop_tanmak(&child_tanmak);
		return;
	}
	if ((gaved_damage / grade_var) + 1 > fat_var){
		//launch particle
		launch_particle(3);
		fat_var = (gaved_damage / grade_var) + 1;
		char png_name[30];
		if (1 <= jako_number && jako_number <= 29 && fat_var <= 3){
			sprintf(png_name, "jakos/jako_%d_%d_%d.png", jako_number, jako_type, fat_var);
			jako_control::jako_sprite->setTexture(Director::getInstance()->getTextureCache()->addImage(png_name));
		}
		else if (30 <= jako_number && jako_number <= 39 && fat_var <= 6){
			sprintf(png_name, "jakos/jako_%d_%d_%d.png", jako_number, jako_type, fat_var);
			jako_control::jako_sprite->setTexture(Director::getInstance()->getTextureCache()->addImage(png_name));
		}
		else if (jako_number >= 40 && fat_var <= 8){
			sprintf(png_name, "jakos/jako_%d_%d_%d.png", jako_number, jako_type, fat_var);
			jako_control::jako_sprite->setTexture(Director::getInstance()->getTextureCache()->addImage(png_name));
		}
	}
}


int jako_control::get_prev_shot_tan_number(){
	return prev_shoted_tan;
}

int jako_control::get_prev_shot_sub_tan_number(){
	return prev_shoted_sub_tan;
}

void jako_control::clear_shoted_number(){
	prev_shoted_tan = -1;
}

void jako_control::add_child_tanmak(int tag){
	child_tanmak.push_back(tag);
}

void jako_control::set_tag(int tag){
	jako_control::tag = tag;
}

int jako_control::get_tag(){
	return jako_control::tag;
}

void jako_control::delete_child_tanmak(int *tag){
	for (int i = 0; i < child_tanmak.size(); i++){
		if (child_tanmak.at(i) == *tag){
			child_tanmak.erase(child_tanmak.begin() + i);
			break;
		}
	}
}

void jako_control::launch_particle(int type){
	
	char particleName[30] = { 0, };

	if (type == 0){
		sprintf(particleName, "particle/enemy_broken.plist");
		CCParticleSystem* particleSystem = CCParticleSystemQuad::create(particleName);
		if (1 <= jako_number && jako_number <= 29)
			particleSystem->setScale(0.6f);
		else if (30 <= jako_number && jako_number <= 39)
			particleSystem->setScale(0.9f);
		else
			particleSystem->setScale(1.2f);
		particleSystem->setPosition(jako_sprite->getPosition());
		particleSystem->setAutoRemoveOnFinish(true);
		forRun->addChild(particleSystem);
		particleSystem = NULL;
	}
	else{
	}
}