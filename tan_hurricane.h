/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef _tan_hurricane_h_
#define _tan_hurricane_h_

#include "cocos2d.h"
#include "enemyTan.h"
#include "tan_basic.h"
#include "mainRogic.h"

class tan_hurricane : public tan_basic{
private:
	cocos2d::Point center, player;
	std::vector<enemyTan*> tanmak;
	mainRogic *Run;
	int tanType, state, density, tag, parentTag;
	float speed, rotate_angle_acc, max_radius, temp_x, temp_y, rotate_density, radius, angle, rotate_angle, target_angle, temp_target_angle, radius_density, acc = 0.0f;
	int rounded_value, value, total_counter;

public:
	~tan_hurricane();

	int prevMakedTime;

	void add_prevMarkedTime();

	int get_prevMarkedTime();

	tan_hurricane(mainRogic *forRun, cocos2d::Point center, float speed, float rotate_angle_acc, float max_radius, int tanType, int rounded_value, int value, int density);

	void make();

	void move();

	int get_density();

	int get_state();

	void set_stat(int stat);

	void set_parent_tanmak(int tag);

	void set_tag(int tag);

	void set_center(cocos2d::Point pos);

	int get_tag();

	int get_parent_tag();

	void set_additional_info(float speed, int angle, float angle_acc);

	void removecurrentTan();
};

#endif