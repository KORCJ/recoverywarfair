/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "enemyTan.h"

USING_NS_CC;

void enemyTan::setSpeed(float speed){
	enemyTan::speed = speed;
}

void enemyTan::setAngle(float angle){
	enemyTan::angle = angle;
	if (enemyTan::angle >= 1.0f)
		enemyTan::angle -= 1.0f;
	else if (enemyTan::angle <= -1.0f)
		enemyTan::angle += 1.0f;
}

void enemyTan::setAngle_acc(float angle_acc){
	this->angle_acc = angle_acc;
}

float enemyTan::getSpeed(){
	return enemyTan::speed;
}

float enemyTan::getAngle(){
	return enemyTan::angle;
}

void enemyTan::move(){
	if (tanSprite == NULL){
		isAlive = false;
		return;
	}
	enemyTan::speed += enemyTan::speed_acc;
	enemyTan::angle += enemyTan::angle_acc;
	enemyTan::angle -= floor(enemyTan::angle);
	if (tan_type == 6 || tan_type == 7){
		enemyTan::tanSprite->setRotation(enemyTan::tanSprite->getRotation() + 60.0f);
	}
	else{
		enemyTan::tanSprite->setRotation( -1 * angle * 360.0f - 90.0f);
	}
	float new_x = cos(M_PI*((enemyTan::angle * 360) / 180))*speed + enemyTan::tanSprite->getPositionX();
	float new_y = sin(M_PI*((enemyTan::angle * 360) / 180))*speed + enemyTan::tanSprite->getPositionY();
	enemyTan::tanSprite->setPosition(Point(new_x, new_y));
	enemyTan::updateStat();
}

void enemyTan::updateStat(){
	if (-100.0f > tanSprite->getPositionX()
		|| tanSprite->getPositionX() > 868.0f
		|| -100.0f > tanSprite->getPositionY()
		|| tanSprite->getPositionY() > 1124.0f){
		isAlive = false;
	}
}

cocos2d::Sprite *enemyTan::getSprite(){
	return enemyTan::tanSprite;
}

enemyTan::enemyTan(int tan_type, float x, float y, float n_speed, float n_angle, float n_speed_acc, float n_angle_acc){
//	log("init");
	char temp_char[25] = { 0, };
	sprintf(temp_char, "jako_tan_0%d.png", tan_type);
	tanSprite = Sprite::createWithSpriteFrameName(temp_char);

	enemyTan::tanSprite->setPosition(cocos2d::Point(x, y));

	enemyTan::tanSprite->setScale(0.78f);

	enemyTan::speed = n_speed;

	enemyTan::angle = n_angle;

	enemyTan::speed_acc = n_speed_acc;

	enemyTan::angle_acc = n_angle_acc;

	enemyTan::tan_type = tan_type;

	isAlive = true;
}