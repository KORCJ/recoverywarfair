/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "sub_player_control.h"
#include "mainRogic.h"
USING_NS_CC;

sub_player_control::sub_player_control(mainRogic *forRun){
	sprintf(tan_png, "sub_player.png");
	sub_player_control::forRun = forRun;
	for (int i = 0, temp = -45; i < 3; i++, temp-=70){
		sub_player[i] = Sprite::create(tan_png);
		forRun->get_player()->addChild(sub_player[i]);
		sub_player[i]->setPositionX(temp);
		sub_player[i]->setVisible(false);
	}
	for (int i = 3, temp = 245; i < 6; i++, temp += 70){
		sub_player[i] = Sprite::create(tan_png);
		forRun->get_player()->addChild(sub_player[i]);
		sub_player[i]->setPositionX(temp);
		sub_player[i]->setVisible(false);
	}
	tanmakTexture = Director::getInstance()->getTextureCache()->addImage("byak_sub_2.png");
	prevMakedTime = 0;
	density = 0;
	make_prevMakedTime = 0;
	density = 1;
	make_density = 3;
	speed = 30.0f;
	value = 10;
	line = 0;
	sub_player[0+line]->setVisible(true);
	sub_player[3+line]->setVisible(true);
}

sub_player_control::~sub_player_control(){
	for (int i = 0; i < 6; i++){
		for (int j = 0; sub_player_tan[i].size(); j++){
			sub_player_tan[i][j]->removeFromParentAndCleanup(true);
			sub_player_tan[i].erase(sub_player_tan[i].begin() + j);
			j--;
		}
	}
}

void sub_player_control::add_more_subPlayer(){
	line++;
	sub_player[0 + line]->setVisible(true);
	sub_player[3 + line]->setVisible(true);
}


void sub_player_control::move(){
	for (int j = 0; j < 6; j++){
		if (j % 3 > line || sub_player[j] == NULL)
			continue;
		for (int i = 0; i < sub_player_tan[j].size(); i++){
			if (sub_player_tan[j].at(i)->getPositionY() > 1100.0f ||
				sub_player_tan[j].at(i)->getPositionX() > 800.0f || 
				sub_player_tan[j].at(i)->getPositionX() < 0.0f){
				sub_player_tan[j].at(i)->setVisible(false);
			}
			else{
				if (j < 3){
					sub_player_tan[j].at(i)->setPositionX(speed * cosf(sub_player_tan_angle[j].at(i)) + sub_player_tan[j].at(i) ->getPositionX());
					sub_player_tan[j].at(i)->setPositionY(speed * sinf(sub_player_tan_angle[j].at(i)) + sub_player_tan[j].at(i)->getPositionY());
				}
				else{
					sub_player_tan[j].at(i)->setPositionX(speed * cosf(sub_player_tan_angle[j].at(i)) + sub_player_tan[j].at(i)->getPositionX());
					sub_player_tan[j].at(i)->setPositionY(speed * sinf(sub_player_tan_angle[j].at(i)) + sub_player_tan[j].at(i)->getPositionY());
				}
			}
		}
	}
}

std::vector<cocos2d::Sprite*> sub_player_control::get_subTanVector(int index){
	return sub_player_tan[index];
}

void sub_player_control::add_prevMarkedTime(){
	prevMakedTime++;
}

int sub_player_control::get_prevMarkedTime(){
	return prevMakedTime;
}

int sub_player_control::get_density(){
	return density;
}

void sub_player_control::add_make_prevMarkedTime(){
	make_prevMakedTime++;
}

int sub_player_control::get_make_prevMarkedTime(){
	return make_prevMakedTime;
}

int sub_player_control::get_make_density(){
	return make_density;
}

void sub_player_control::set_angle_func(int i,int k){
	if (forRun->BossIslive){
		if (i < 3){
			int j, size;
			bool flag = false;
			if (forRun->boss->getSprite()->getPositionY() > forRun->playerPoint.y &&
				forRun->boss->getSprite()->getPositionY() <= forRun->playerPoint.y + 384 &&
				forRun->boss->get_state() != 0){
				if (k < 0){
					sub_player_tan[i].push_back(Sprite::createWithTexture(tanmakTexture));
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionX(-80.0f - 35.0f * (i) +forRun->playerPoint.x);
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
					forRun->addChild(sub_player_tan[i].at(sub_player_tan[i].size() - 1));
				}
				else{
					sub_player_tan[i].at(k)->setPositionX(-80.0f - 35.0f * (i) +forRun->playerPoint.x);
					sub_player_tan[i].at(k)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
				}
				if (forRun->boss->getSprite()->getPositionX() > sub_player[i]->getPositionX() + forRun->playerPoint.x){
					if (k >= 0){
						sub_player_tan_angle[i].at(k) = (atan2f(abs(forRun->boss->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))
							, abs(forRun->boss->getSprite()->getPositionX() - (-80.0f - 35.0f * (i) +forRun->playerPoint.x))));
						sub_player_tan[i].at(k)->setVisible(true);
					}
					else{
						sub_player_tan_angle[i].push_back(atan2f(abs(forRun->boss->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f)),
							abs(forRun->boss->getSprite()->getPositionX() - (-80.0f - 35.0f * (i) +forRun->playerPoint.x))));
					}
				}
				else{
					if (k >= 0){
						sub_player_tan_angle[i].at(k) = atan2f(abs(forRun->boss->getSprite()->getPositionX() - (-80.0f - 35.0f * (i) +forRun->playerPoint.x)),
							abs(forRun->boss->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))) + 0.5f * M_PI;
						sub_player_tan[i].at(k)->setVisible(true);
					}
					else{
						sub_player_tan_angle[i].push_back(atan2f(abs(forRun->boss->getSprite()->getPositionX() - (-80.0f - 35.0f * (i) +forRun->playerPoint.x)),
							abs(forRun->boss->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))) + 0.5f * M_PI);
					}
				}
				flag = true;
			}
			if (!flag){
				if (k < 0){
					sub_player_tan_angle[i].push_back(0.5f * M_PI);
					sub_player_tan[i].push_back(Sprite::createWithTexture(tanmakTexture));
					forRun->addChild(sub_player_tan[i].at(sub_player_tan[i].size() - 1));
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionX(-80.0f - 35.0f * (i) +forRun->playerPoint.x);
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
				}
				else{
					sub_player_tan_angle[i].at(k) = 0.5f * M_PI;
					sub_player_tan[i].at(k)->setPositionX(-80.0f - 35.0f * (i) +forRun->playerPoint.x);
					sub_player_tan[i].at(k)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
					sub_player_tan[i].at(k)->setVisible(true);
				}
			}
		}
		else{
			int j = 999;
			bool flag = false;
			if (forRun->boss->getSprite()->getPositionY() > forRun->playerPoint.y &&
				forRun->boss->getSprite()->getPositionY() <= forRun->playerPoint.y + 384 &&
				forRun->boss->get_state() != 0){
				if (k < 0){
					sub_player_tan[i].push_back(Sprite::createWithTexture(tanmakTexture));
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionX(70.0f + 35.0f * (i - 3) + forRun->playerPoint.x);
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
					forRun->addChild(sub_player_tan[i].at(sub_player_tan[i].size() - 1));
				}
				else{
					sub_player_tan[i].at(k)->setPositionX(70.0f + 35.0f * (i - 3) + forRun->playerPoint.x);
					sub_player_tan[i].at(k)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
				}
				if (forRun->boss->getSprite()->getPositionX() >(sub_player[i]->getPositionX() - 100.0f) * 0.03f + forRun->playerPoint.x){
					if (k >= 0){
						sub_player_tan_angle[i].at(k) = (atan2f(abs(forRun->boss->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))
							, abs(forRun->boss->getSprite()->getPositionX() - (70.0f + 35.0f * (i - 3) + forRun->playerPoint.x))));
						sub_player_tan[i].at(k)->setVisible(true);
					}
					else{
						sub_player_tan_angle[i].push_back(atan2f(abs(forRun->boss->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f)),
							abs(forRun->boss->getSprite()->getPositionX() - (70.0f + 35.0f * (i - 3) + forRun->playerPoint.x))));
					}
				}
				else{
					if (k >= 0){
						sub_player_tan_angle[i].at(k) = atan2f(abs(forRun->boss->getSprite()->getPositionX() - (70.0f + 35.0f * (i - 3) + forRun->playerPoint.x)),
							abs(forRun->boss->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))) + 0.5f * M_PI;
						sub_player_tan[i].at(k)->setVisible(true);
					}
					else{
						sub_player_tan_angle[i].push_back(atan2f(abs(forRun->boss->getSprite()->getPositionX() - (70.0f + 35.0f * (i - 3) + forRun->playerPoint.x)),
							abs(forRun->boss->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))) + 0.5f * M_PI);
					}
				}
				flag = true;
			}
			if (!flag){
				if (k < 0){
					sub_player_tan_angle[i].push_back(0.5f * M_PI);
					sub_player_tan[i].push_back(Sprite::createWithTexture(tanmakTexture));
					forRun->addChild(sub_player_tan[i].at(sub_player_tan[i].size() - 1));
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionX(70.0f + 35.0f * (i - 3) + forRun->playerPoint.x);
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
				}
				else{
					sub_player_tan_angle[i].at(k) = 0.5f * M_PI;
					sub_player_tan[i].at(k)->setPositionX(70.0f + 35.0f * (i - 3) + forRun->playerPoint.x);
					sub_player_tan[i].at(k)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
					sub_player_tan[i].at(k)->setVisible(true);
				}
			}
		}
	}
	else{
		if (i < 3){
			int j, size;
			bool flag = false;
			for (j = 0, size = forRun->get_jakos()->size(); j < size && sub_player_tan[i].size() != 0; j++){
				if (forRun->get_jakos()->at(j)->getSprite()->getPositionY() > forRun->playerPoint.y &&
					forRun->get_jakos()->at(j)->getSprite()->getPositionY() <= forRun->playerPoint.y + 384 &&
					forRun->get_jakos()->at(j)->get_state() != 0){
					if (k < 0){
						sub_player_tan[i].push_back(Sprite::createWithTexture(tanmakTexture));
						sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionX(-80.0f - 35.0f * (i) +forRun->playerPoint.x);
						sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
						forRun->addChild(sub_player_tan[i].at(sub_player_tan[i].size() - 1));
					}
					else{
						sub_player_tan[i].at(k)->setPositionX(-80.0f - 35.0f * (i) +forRun->playerPoint.x);
						sub_player_tan[i].at(k)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
					}
					if (forRun->get_jakos()->at(j)->getSprite()->getPositionX() > sub_player[i]->getPositionX() + forRun->playerPoint.x){
						if (k >= 0){
							sub_player_tan_angle[i].at(k) = (atan2f(abs(forRun->get_jakos()->at(j)->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))
								, abs(forRun->get_jakos()->at(j)->getSprite()->getPositionX() - (-80.0f - 35.0f * (i) +forRun->playerPoint.x))));
							sub_player_tan[i].at(k)->setVisible(true);
						}
						else{
							sub_player_tan_angle[i].push_back(atan2f(abs(forRun->get_jakos()->at(j)->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f)),
								abs(forRun->get_jakos()->at(j)->getSprite()->getPositionX() - (-80.0f - 35.0f * (i) +forRun->playerPoint.x))));
						}
					}
					else{
						if (k >= 0){
							sub_player_tan_angle[i].at(k) = atan2f(abs(forRun->get_jakos()->at(j)->getSprite()->getPositionX() - (-80.0f - 35.0f * (i) +forRun->playerPoint.x)),
								abs(forRun->get_jakos()->at(j)->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))) + 0.5f * M_PI;
							sub_player_tan[i].at(k)->setVisible(true);
						}
						else{
							sub_player_tan_angle[i].push_back(atan2f(abs(forRun->get_jakos()->at(j)->getSprite()->getPositionX() - (-80.0f - 35.0f * (i) +forRun->playerPoint.x)),
								abs(forRun->get_jakos()->at(j)->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))) + 0.5f * M_PI);
						}
					}
					flag = true;
					break;
				}
			}
			if (!flag){
				if (k < 0){
					sub_player_tan_angle[i].push_back(0.5f * M_PI);
					sub_player_tan[i].push_back(Sprite::createWithTexture(tanmakTexture));
					forRun->addChild(sub_player_tan[i].at(sub_player_tan[i].size() - 1));
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionX(-80.0f - 35.0f * (i) +forRun->playerPoint.x);
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
				}
				else{
					sub_player_tan_angle[i].at(k) = 0.5f * M_PI;
					sub_player_tan[i].at(k)->setPositionX(-80.0f - 35.0f * (i) +forRun->playerPoint.x);
					sub_player_tan[i].at(k)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
					sub_player_tan[i].at(k)->setVisible(true);
				}
			}
		}
		else{
			int j = 999;
			bool flag = false;
			for (j = forRun->get_jakos()->size() - 1; j >= 0 && sub_player_tan[i].size() != 0; j--){
				if (forRun->get_jakos()->at(j)->getSprite()->getPositionY() > forRun->playerPoint.y &&
					forRun->get_jakos()->at(j)->getSprite()->getPositionY() <= forRun->playerPoint.y + 384 &&
					forRun->get_jakos()->at(j)->get_state() != 0){
					if (k < 0){
						sub_player_tan[i].push_back(Sprite::createWithTexture(tanmakTexture));
						sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionX(70.0f + 35.0f * (i - 3) + forRun->playerPoint.x);
						sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
						forRun->addChild(sub_player_tan[i].at(sub_player_tan[i].size() - 1));
					}
					else{
						sub_player_tan[i].at(k)->setPositionX(70.0f + 35.0f * (i - 3) + forRun->playerPoint.x);
						sub_player_tan[i].at(k)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
					}
					if (forRun->get_jakos()->at(j)->getSprite()->getPositionX() >(sub_player[i]->getPositionX() - 100.0f) * 0.03f + forRun->playerPoint.x){
						if (k >= 0){
							sub_player_tan_angle[i].at(k) = (atan2f(abs(forRun->get_jakos()->at(j)->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))
								, abs(forRun->get_jakos()->at(j)->getSprite()->getPositionX() - (70.0f + 35.0f * (i - 3) + forRun->playerPoint.x))));
							sub_player_tan[i].at(k)->setVisible(true);
						}
						else{
							sub_player_tan_angle[i].push_back(atan2f(abs(forRun->get_jakos()->at(j)->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f)),
								abs(forRun->get_jakos()->at(j)->getSprite()->getPositionX() - (70.0f + 35.0f * (i - 3) + forRun->playerPoint.x))));
						}
					}
					else{
						if (k >= 0){
							sub_player_tan_angle[i].at(k) = atan2f(abs(forRun->get_jakos()->at(j)->getSprite()->getPositionX() - (70.0f + 35.0f * (i - 3) + forRun->playerPoint.x)),
								abs(forRun->get_jakos()->at(j)->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))) + 0.5f * M_PI;
							sub_player_tan[i].at(k)->setVisible(true);
						}
						else{
							sub_player_tan_angle[i].push_back(atan2f(abs(forRun->get_jakos()->at(j)->getSprite()->getPositionX() - (70.0f + 35.0f * (i - 3) + forRun->playerPoint.x)),
								abs(forRun->get_jakos()->at(j)->getSprite()->getPositionY() - (sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f))) + 0.5f * M_PI);
						}
					}
					flag = true;
					break;
				}
			}
			if (!flag){
				if (k < 0){
					sub_player_tan_angle[i].push_back(0.5f * M_PI);
					sub_player_tan[i].push_back(Sprite::createWithTexture(tanmakTexture));
					forRun->addChild(sub_player_tan[i].at(sub_player_tan[i].size() - 1));
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionX(70.0f + 35.0f * (i - 3) + forRun->playerPoint.x);
					sub_player_tan[i].at(sub_player_tan[i].size() - 1)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
				}
				else{
					sub_player_tan_angle[i].at(k) = 0.5f * M_PI;
					sub_player_tan[i].at(k)->setPositionX(70.0f + 35.0f * (i - 3) + forRun->playerPoint.x);
					sub_player_tan[i].at(k)->setPositionY(sub_player[i]->getPositionY() + forRun->playerPoint.y - 30.0f);
					sub_player_tan[i].at(k)->setVisible(true);
				}
			}
		}
	}
}

void sub_player_control::make(){
	if (forRun->isGameEnd)
		return;
	for (int i = 0; i < 6; i++){
		if ((i % 3)>line)
			continue;
		if (sub_player_tan[i].size() < value){
			set_angle_func(i, -1);
			continue;
		}
		else{
			for (int j = 0; j < value; j++){
				if (!sub_player_tan[i].at(j)->isVisible()){
					set_angle_func(i, j);
					break;
				}
			}
		}
	}
	
}

int sub_player_control::get_line(){
	return line;
}

int sub_player_control::get_value(){
	return value;
}