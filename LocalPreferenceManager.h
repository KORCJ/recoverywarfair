/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef _LocalPrefernceManager_
#define _LocalPrefernceManager_
#include "cocos2d.h"
USING_NS_CC;

enum LocalPrefernceIndex{
	Prefernce_playBgm = 0,
	Prefernce_playSfx,
	Prefernce_playVibrate,
	Prefernce_size
};

static const char getLocalPrefernceKey[Prefernce_size][32] = { "playBgm", "playSfx", "playVibrate" };

class LocalPrefernceData{

private:
	static LocalPrefernceData *_instance;
public:
	LocalPrefernceData();
	~LocalPrefernceData();

	static LocalPrefernceData* sharedInstance(void)
	{
		if (_instance == NULL)
			_instance = new LocalPrefernceData();
		return _instance;
	}

	static void release(void)
	{
		if (_instance != NULL)
			delete _instance;
		_instance = NULL;
	}

	void setOption(bool bgm, bool sfx, bool vibrate);

	void setOption();

	CC_SYNTHESIZE(bool, setter_BGM, forBGM);
	CC_SYNTHESIZE(bool, setter_SFX, forSFX);
	CC_SYNTHESIZE(bool, setter_VIBRATE, forVIBRATE);
};
#endif