/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "neverb_tan_control.h"

USING_NS_CC;

neverb_tan_control::neverb_tan_control(mainRogic *run, int speed, int density, int make_density){
	neverb_tan_control::forRun = run;
	neverb_tan_control::speed = speed;
	neverb_tan_control::density = density;
	neverb_tan_control::total_size = 14*5;
	neverb_tan_control::make_density = make_density;
	sprintf(png_name, "never_1.png");
	neverb_tan_control::texture = Director::getInstance()->getTextureCache()->addImage(png_name);
	int temp[14] = { 120, 100, 80, 60, 70, 90, 110, 60, 80, 100, 120, 110, 90, 70 };
	for (int i = 0; i < 14; i++){
		shootangle[i] = temp[i];
	}
}

void neverb_tan_control::add_prevMarkedTime(){
	prevMakedTime++;
}

int neverb_tan_control::get_prevMarkedTime(){
	return prevMakedTime;
}

int neverb_tan_control::get_density(){
	return density;
}

void neverb_tan_control::add_make_prevMarkedTime(){
	make_prevMakedTime++;
}

int neverb_tan_control::get_make_prevMarkedTime(){
	return make_prevMakedTime;
}

neverb_tan_control::~neverb_tan_control(){
	for (int i = 0; i < playerTan.size(); i++){
		playerTan.at(i)->removeFromParentAndCleanup(true);
		playerTan.erase(playerTan.begin() + i--);
	}
}

int neverb_tan_control::get_make_density(){
	return make_density;
}

void neverb_tan_control::move(){
	prevMakedTime = 0;

	for (int i = 0; i < playerTan.size(); i++){
		if (playerTan.at(i) != NULL){
			if (playerTan.at(i)->getPositionY()>1224.0f - (texture->getPixelsHigh() / 2.0f)){
				playerTan.at(i)->setVisible(false);
			}
			else{
				playerTan.at(i)->setPositionY(playerTan.at(i)->getPositionY() + (speed * sinf(shootangle[i % 14] * M_PI / 180.0f)));
				playerTan.at(i)->setPositionX(playerTan.at(i)->getPositionX() + (speed * cosf(shootangle[i % 14] * M_PI / 180.0f)));
			}
		}
	}
}

std::vector<cocos2d::Sprite*> neverb_tan_control::get_playerTanVector(){
	return playerTan;
}

void neverb_tan_control::make(){
	if (forRun->isGameEnd)
		return;
	make_prevMakedTime = 0;

	if (playerTan.size() < neverb_tan_control::total_size){
		playerTan.push_back(Sprite::createWithTexture(neverb_tan_control::texture));
		playerTan.at(playerTan.size() - 1)->setPosition(forRun->playerPoint);
		playerTan.at(playerTan.size() - 1)->setScale(0.5f);
		forRun->addChild(playerTan.at(playerTan.size() - 1));
	}
	else{
		for (int i = 0; i < total_size; i++){
			if (!playerTan.at(i)->isVisible()){
				forRun->clear_jakos_prev_shoted_number(i + 1);
				playerTan.at(i)->setPosition(forRun->playerPoint);
				playerTan.at(i)->setVisible(true);
				break;
			}
		}
	}
}