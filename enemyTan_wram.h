/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef __enemyTan_wram__
#define __enemyTan_wram__
#include"cocos2d.h"

class enemyTan_wram : cocos2d::Sprite{
private:
	float speed, angle, rotate_angle_acc, lenth, rotate_angle, prev_temp_y = 999, tan_value, s_angle_acc, s_tan_value;

	/* 0 is left; 1 if right*/
	int state, checkForupSide, upside_flag;

	cocos2d::Sprite *tanSprite;

	std::vector<cocos2d::Point> center;
	
	cocos2d::Point prev_pos;

	int flag, total_point, tan_type;

	float doubleLenth_x, doubleLenth_y, temp_y;
	
	float new_x, new_y;

	int inline_flag;
public:
	bool isAlive;

	void setSpeed(float speed);

	float getSpeed();

	float getAngle();

	bool getStat();

	void move();

	void updateStat();

	bool checkReachNextCircle(float &tan_value, int call_type);

	bool checkCorrectNextCircle();

	cocos2d::Sprite *getSprite();

	enemyTan_wram(int tan_type, int total_point, std::vector<cocos2d::Point> center, float n_speed, float rotate_angle_acc);
};
#endif