/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "boss_control.h"
#include "mainRogic.h"
#include "player_tan_cotrol.h"
#include "sub_player_control.h"
#include "neverb_tan_control.h"
#include "toolBoxxxxx.h"
USING_NS_CC;

boss_control::boss_control(mainRogic *forRun, float speed, short jako_type){
	boss_control::forRun = forRun;
	boss_control::speed = speed;
	sprintf(png_name, "Boss_troy_%d_1_0.png", jako_type);
	boss_control::jako_type = jako_type;
	boss_control::boss_sprite = Sprite::createWithSpriteFrameName(png_name);
	boss_control::boss_sprite->setPosition(Point(384.0f, 1100.0f));
	boss_control::boss_sprite->setScale(1.5f);
	//	boss_control::boss_sprite->setScale(0.4f);
	forRun->addChild(boss_sprite, 1);
	boss_control::state = 4;
	boss_control::gaved_damage = 1;
	wasInside = false;
	fat_var = 1;
	hp = 20000;
	gap_between = 150.0f;
	flag = 1;
	rotate_angle_acc = 0.03;
	redraw_img_density = 4;
	ovalRate = 70.0f;
	fat_var = 1;
	frame_var = 0;
	grade_var = hp / 10;
	dt_redraw_img_density = 0;
	center = Point(384.0f, 800.0f);
	if (flag == 0){
		oval_radial = 0;
	}
	else{
		oval_radial = M_PI;
	}
	rotate_angle = 0;
	dt_density = 0;
	density = 1;
}

boss_control::~boss_control(){
	forRun->jako_stop_tanmak(&child_tanmak);
}

int boss_control::get_state(){
	return state;
}

void boss_control::set_state(int state){
	boss_control::state = state;
}

void boss_control::move(){
	if (dt_redraw_img_density >= redraw_img_density){
		dt_redraw_img_density = 0;
		sprintf(png_name, "Boss_troy_%d_%d_%d.png", jako_type, ++frame_var, fat_var);
		if (frame_var >= 6)
			frame_var = 0;
		boss_sprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(png_name));

	}
	else{
		dt_redraw_img_density++;
	}
	if (dt_density < density){
		dt_density++;
		return;
	}
	dt_density = 0;
	if (state == 4){
		boss_sprite->setPosition(boss_sprite->getPosition() - Point(0, 4.0f));
		if (boss_sprite->getPositionY() < 800.0f){
			state = 3;
		}
	}
	else if (state == 3 || state == 2){
		float new_x, new_y;
		//원형 이동
		/*
		if (rotate_angle == 90.0f * M_PI / 180.0f){
		new_x = ovalRate;
		new_y = 0.0f;
		}
		else if (rotate_angle == 270.0f * M_PI / 180.0f){
		new_x = -ovalRate;
		new_y = 0.0f;
		}
		*/
		new_x = gap_between * cosf(rotate_angle) * cosf(oval_radial) - ovalRate * sinf(rotate_angle) * sinf(oval_radial);
		new_y = gap_between * cosf(rotate_angle) * sinf(oval_radial) + ovalRate * sinf(rotate_angle) * cosf(oval_radial);
		if (flag == 0){
			new_x += center.x - 150.0f;
		}
		else{
			new_x += center.x + 150.0f;
		}
		new_y += center.y;
		//설정된 방향에 따라 이동
		if (flag == CURVE_CCW)
			rotate_angle += rotate_angle_acc;
		else
			rotate_angle -= rotate_angle_acc;

		boss_sprite->setPosition(Point(new_x, new_y));
		///반바퀴 도는 과정이 끝난 경우.
		//단 마지막이였던 경우, 이 연산은 생략한다.
		if (rotate_angle >= 2.0f * M_PI || rotate_angle <= -2.0f * M_PI){
			rotate_angle = adjust_angle_rad(rotate_angle);
			flag = flag ^ 1;
			//다음 좌표와 그 다음 좌표 사이에 값을 정의
			if (flag == 0){
				oval_radial = 0;
			}
			else{
				oval_radial = M_PI;
			}
			rotate_angle = 0;
		}
	}
}

cocos2d::Sprite *boss_control::getSprite(){
	return boss_sprite;
}

void boss_control::add_damage(int shot_tan_number){
	boss_control::prev_shoted_tan = shot_tan_number;
	switch (jako_type){
	case 1:gaved_damage += 24; break;
	case 2:gaved_damage += 6; break;
	case 3:gaved_damage += 5; break;
	}
	if (gaved_damage >= hp){
		if (state != 0){
			forRun->add_score(100);
			launch_particle(0);
			forRun->add_combo();
			forRun->acc_fever();
			forRun->BossIslive = false;
			forRun->BossisRunning = false;
			forRun->isGameEnd = true;
			char temp_str[30] = { 0, };
			switch (jako_type){
			case 1:sprintf(temp_str, "complete_%d.png", 3); break;
			case 2:sprintf(temp_str, "complete_%d.png", 1); break;
			case 3:sprintf(temp_str, "complete_%d.png", 2); break;
			}
			((Sprite*)forRun->getChildByName("uiSprite")->getChildByName("recoveringSprite"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_str));
			forRun->isGameEnd = true;
			forRun->runAction(Sequence::create(CCDelayTime::create(2.0f), CallFunc::create(CC_CALLBACK_0(mainRogic::show_result_window, forRun, 1)), NULL));
			boss_control::boss_sprite->removeFromParentAndCleanup(true);
		}
		state = 0;
		forRun->jako_stop_tanmak(&child_tanmak);
		return;
	}
	char particleName[30] = { 0, };
	switch (forRun->player_type){
	case 1:{sprintf(particleName, "particle/vsuri_attack.plist"); }break;
	case 2:{sprintf(particleName, "particle/balyak_attack.plist"); }break;
	case 3:{sprintf(particleName, "particle/never_attack.plist"); }break;
	}
	CCParticleSystem* particleSystem = CCParticleSystemQuad::create(particleName);
	particleSystem->setPosition(forRun->player_tan->get_playerTanVector().at(shot_tan_number)->getPosition() + Point(0, 80.0f));
	particleSystem->setAutoRemoveOnFinish(true);
	forRun->addChild(particleSystem, 5);
	particleSystem = NULL;
	if ((gaved_damage / grade_var)> fat_var){
		/////////////////////////////
		/////////////////////////////
		fat_var = (gaved_damage / grade_var);
		char png_name[30];
		//launch particle
		launch_particle(1);
		if (frame_var >= 6)
			frame_var = 1;
		else if (frame_var <= 0)
			frame_var = 1;
		if (gaved_damage > 14000){
			redraw_img_density = 1; rotate_angle_acc = 0.06;
			state = 2;
		}
		forRun->BossDamage++;
		sprintf(png_name, "Boss_troy_%d_%d_%d.png", jako_type, frame_var, fat_var);
		boss_control::boss_sprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(png_name));
	}
}

void boss_control::add_sub_damage(int shot_tan_number){
	boss_control::prev_shoted_sub_tan = shot_tan_number;
	switch (jako_type){
	case 2:gaved_damage += 2; break;
	case 3:gaved_damage += 6; break;
	}
	if (gaved_damage >= hp){
		if (state != 0){
			launch_particle(0);
			forRun->add_combo();
			forRun->acc_fever();
			forRun->BossIslive = false;
			forRun->BossisRunning = false;
		}
		state = 0;
		forRun->jako_stop_tanmak(&child_tanmak);
		forRun->isGameEnd = true;
		forRun->add_score(100);
		char temp_str[30] = { 0, };
		switch (jako_type){
		case 1:sprintf(temp_str, "complete_%d.png", 3); break;
		case 2:sprintf(temp_str, "complete_%d.png", 1); break;
		case 3:sprintf(temp_str, "complete_%d.png", 2); break;
		}
		((Sprite*) forRun->getChildByName("uiSprite")->getChildByName("recoveringSprite"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_str));
		forRun->isGameEnd = true;
		forRun->runAction(Sequence::create(CCDelayTime::create(2.0f), CallFunc::create(CC_CALLBACK_0(mainRogic::show_result_window, forRun, 1)), NULL));
		boss_control::boss_sprite->removeFromParentAndCleanup(true);
		return;
	}
	if (jako_type == 2){
		/////////////////////////////
		CCParticleSystem* particleSystem = CCParticleSystemQuad::create("particle/balyak_attack.plist");
		particleSystem->setScale(0.5f);
		particleSystem->setPosition(forRun->sub_player->get_subTanVector(shot_tan_number / forRun->sub_player->get_value()).at(shot_tan_number % forRun->sub_player->get_value())->getPosition());
		particleSystem->setAutoRemoveOnFinish(true);
		forRun->addChild(particleSystem, 5);
		particleSystem = NULL;
	}
	else if (jako_type == 3){
		CCParticleSystem* particleSystem = CCParticleSystemQuad::create("particle/never_attack.plist");

		particleSystem->setPosition(forRun->neverb_sub_player->get_playerTanVector().at(shot_tan_number)->getPosition());
		particleSystem->setAutoRemoveOnFinish(true);
		forRun->addChild(particleSystem, 5);
		particleSystem = NULL;
	}
	if ((gaved_damage / grade_var)> fat_var){
		/////////////////////////////
		//launch particle
		launch_particle(2);
		fat_var = (gaved_damage / grade_var);
		if (frame_var >= 6)
			frame_var = 1;
		else if (frame_var <= 0)
			frame_var = 1;
		if (gaved_damage > 14000){
			redraw_img_density = 1; rotate_angle_acc = 0.06;
			state = 2;
		}
		forRun->BossDamage++;
		sprintf(png_name, "Boss_troy_%d_%d_%d.png", jako_type, frame_var, fat_var);
		boss_control::boss_sprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(png_name));
	}
}

void boss_control::bomb_damage(int delta){
	gaved_damage += delta;
	if (gaved_damage >= hp){
		if (state != 0){
			forRun->add_score(100);
			launch_particle(0);
			forRun->add_combo();
			forRun->acc_fever();
			forRun->BossIslive = false;
			forRun->BossisRunning = false;
			forRun->isGameEnd = true;
			char temp_str[30] = { 0, };
			switch (jako_type){
			case 1:sprintf(temp_str, "complete_%d.png", 3); break;
			case 2:sprintf(temp_str, "complete_%d.png", 1); break;
			case 3:sprintf(temp_str, "complete_%d.png", 2); break;
			}
			((Sprite*) forRun->getChildByName("uiSprite")->getChildByName("recoveringSprite"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_str));
			forRun->isGameEnd = true;
			forRun->runAction(Sequence::create(CCDelayTime::create(2.0f), CallFunc::create(CC_CALLBACK_0(mainRogic::show_result_window, forRun, 1)), NULL));
			boss_control::boss_sprite->removeFromParentAndCleanup(true);
		}
		state = 0;
		forRun->jako_stop_tanmak(&child_tanmak);
		return;
	}
	if ((gaved_damage / grade_var) > fat_var){
		//launch particle
		launch_particle(3);
		fat_var = (gaved_damage / grade_var);
		if (frame_var >= 6)
			frame_var = 1;
		else if (frame_var <= 0)
			frame_var = 1;
		if (gaved_damage > 14000){
			redraw_img_density = 1; 
			rotate_angle_acc = 0.06;
			state = 2;
		}
		forRun->BossDamage++;
		sprintf(png_name, "Boss_troy_%d_%d_%d.png", jako_type, frame_var, fat_var);
		boss_control::boss_sprite->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(png_name));
	}
}


int boss_control::get_prev_shot_tan_number(){
	return prev_shoted_tan;
}

int boss_control::get_prev_shot_sub_tan_number(){
	return prev_shoted_sub_tan;
}

void boss_control::clear_shoted_number(){
	prev_shoted_tan = -1;
}

void boss_control::add_child_tanmak(int tag){
	child_tanmak.push_back(tag);
}

void boss_control::delete_child_tanmak(int *tag){
	for (int i = 0; i < child_tanmak.size(); i++){
		if (child_tanmak.at(i) == *tag){
			child_tanmak.erase(child_tanmak.begin() + i);
			break;
		}
	}
}

void boss_control::launch_particle(int type){

	char particleName[30] = { 0, };

	if (type == 0){
		sprintf(particleName, "particle/enemy_broken.plist");
		CCParticleSystem* particleSystem = CCParticleSystemQuad::create(particleName);
		particleSystem->setScale(1.5f);
		particleSystem->setPosition(boss_sprite->getPosition());
		particleSystem->setAutoRemoveOnFinish(true);
		forRun->addChild(particleSystem);
		particleSystem = NULL;
	}
	else{
	}
}