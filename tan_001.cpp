/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "tan_001.h"
USING_NS_CC;

tan_001::tan_001(mainRogic *forRun, int x, int y, int shootType, float n_speed, int angle, float angle_ac, float angle_ac_acc, float count, int tanType, int value, int density){
	tan_001::Run = forRun;
	tan_001::angle_ac = angle_ac;
	tan_001::angle_ac_acc = angle_ac_acc;
	tan_001::count = (int)(count + 0.5f);
	tan_001::pos = Point(x,y);
	tan_001::size = 0;
	tan_001::tanType = tanType;
	tan_001::speed = n_speed;
	if (shootType == 1){
		if (Run->playerPoint.y > y){
			if (Run->playerPoint.x > x)
				tan_001::angle = atan2f(abs(Run->playerPoint.y - y), abs(Run->playerPoint.x - x));
			else
				tan_001::angle = atan2f(abs(Run->playerPoint.x - x), abs(Run->playerPoint.y - y)) + 0.5f * M_PI;
		}
		else{
			if (Run->playerPoint.x < x)
				tan_001::angle = atan2f(abs(Run->playerPoint.y - y), abs(Run->playerPoint.x - x)) + 1.0f * M_PI;
			else
				tan_001::angle = atan2f(abs(Run->playerPoint.x - x), abs(Run->playerPoint.y - y)) + 1.5f * M_PI;
		}
		tan_001::angle /= (M_PI * 2.0f);
	}
	else
		tan_001::angle = angle / 360.0f;
	tan_001::dt_angle = 0;
	tan_001::bound_angle = 360;
	tan_001::state = 3;
	tan_001::value = value; 
	tan_001::density = density;
	tan_001::shootType = shootType;
	tan_001::prevMakedTime = 0;
	tan_001::called_time = 0;
}

void tan_001::set_center(Point pos){
	this->pos = pos;
}

tan_001::~tan_001(){
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
		delete tanmak.at(i);
		tanmak.erase(tanmak.begin() + i--);
	}
}

void tan_001::make(){

	prevMakedTime = 0;
	if (shootType == 1){
		if (Run->playerPoint.y > pos.y){
			if (Run->playerPoint.x > pos.x)
				tan_001::angle = atan2f(abs(Run->playerPoint.y - pos.y), abs(Run->playerPoint.x - pos.x));
			else
				tan_001::angle = atan2f(abs(Run->playerPoint.x - pos.x), abs(Run->playerPoint.y - pos.y)) + 0.5f * M_PI;
		}
		else{
			if (Run->playerPoint.x < pos.x)
				tan_001::angle = atan2f(abs(Run->playerPoint.y - pos.y), abs(Run->playerPoint.x - pos.x)) + 1.0f * M_PI;
			else
				tan_001::angle = atan2f(abs(Run->playerPoint.x - pos.x), abs(Run->playerPoint.y - pos.y)) + 1.5f * M_PI;
		}
		tan_001::angle /= (M_PI * 2.0f);
//		tan_001::angle -= ((float) bound_angle / tan_001::count) / 360.0f * tan_001::count / 2;
		tan_001::angle -= ((float) bound_angle / tan_001::count / 2) / 360.0f;
	}
	//탄막 텍스쳐 로드
	tan_001::angle_ac += angle_ac_acc;
	if (angle >= 2.0f * M_PI)
		angle -= 2.0f * M_PI;
	else if (angle <= -2.0f * M_PI)
		angle += 2.0f * M_PI;
	dt_angle = 0;
	for (int i = 0; i < tan_001::count; i++){
		//벡터에 탄막 생성
		tanmak.push_back(new enemyTan(tanType, pos.x, pos.y, speed, angle + dt_angle, 0.03f, 0.000f));
		//탄막 각도 재조정
		tanmak.at(tanmak.size() - 1)->setAngle(tanmak.at(tanmak.size() - 1)->getAngle() + angle_ac);
		//지금까지 생성한 탄막 카운트
		size++;
		//mainRogic에 추가
		Run->addChild(tanmak.at(tanmak.size() - 1)->getSprite());

		
		if (tan_001::count > 1){
			dt_angle += ((float)bound_angle / tan_001::count)/360.0f ;
		}
	}

	tan_001::called_time++;
	tan_001::angle += angle_ac;
	if (tanmak.size() >= 1)
		tan_001::state = 2;
	if (called_time == tan_001::value){
		tan_001::state = 1;
	}
}

int tan_001::get_density(){
	return tan_001::density;
}

int tan_001::get_state(){
	return tan_001::state;
}

void tan_001::add_prevMarkedTime(){
	prevMakedTime++;
}

int tan_001::get_prevMarkedTime(){
	return prevMakedTime;
}

void tan_001::move(){
	if (state == 3)
		return;
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->move();
		if (!tanmak.at(i)->isAlive){
			tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
			delete(tanmak.at(i));
			tanmak.erase(tanmak.begin() + i);
			i--;
		}
		else if ((Run->isPlayerLive && !Run->isPlayerOP) && tanmak.at(i)->getSprite()->getBoundingBox().intersectsRect(Run->playerBoundingBox)){
			float d_x = abs(tanmak.at(i)->getSprite()->getPositionX() - Run->playerPoint.x);
			float d_y = abs(tanmak.at(i)->getSprite()->getPositionY() - Run->playerPoint.y);
			float temp = sqrtf(d_x*d_x + d_y*d_y);
			if (temp < tanmak.at(i)->getSprite()->getBoundingBox().getMaxX() - tanmak.at(i)->getSprite()->getBoundingBox().getMidX() - 8){
				Run->playerDeadTriger();
				tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
				delete(tanmak.at(i));
				tanmak.erase(tanmak.begin() + i);
				i--;
			}
		}
	}

	if (tanmak.size() == 0){
		state = 0;
	}
}

void tan_001::set_additional_info(float speed, int angle, float angle_acc){
	if (speed != -1){
//		this->speed = speed;
		for (enemyTan *temp : tanmak){
			temp->setSpeed(speed);
		}
	}
	if (angle != -1.0f){
		for (enemyTan *temp : tanmak){
			temp->setAngle(temp->getAngle() +angle / 360.0f);
		}
	}
	if (angle_acc != -1.0f){
		for (enemyTan *temp : tanmak){
			temp->setAngle_acc(angle_acc);
		}
	}
}

void tan_001::updatePosition(Point &n_pos){
	tan_001::pos = n_pos;
}

void tan_001::setBounds(int angle){
	tan_001::bound_angle = angle;
	//범위 지정하기
}

void tan_001::set_stat(int stat){
	tan_001::state = stat;
}

void tan_001::set_parent_tanmak(int tag){
	parentTag = tag;
}

void tan_001::set_tag(int tag){
	tan_001::tag = tag;
}

int tan_001::get_tag(){
	return tag;
}

int tan_001::get_parent_tag(){
	return parentTag;
}

void tan_001::removecurrentTan(){
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
		delete(tanmak.at(i));
		tanmak.erase(tanmak.begin() + i);
		i--;
	}
}