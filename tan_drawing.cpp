/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "tan_drawing.h"

USING_NS_CC;
#define float_square(x) (x)*(x)

tan_drawing::tan_drawing(mainRogic *Run, cocos2d::Point str, cocos2d::Point dst, cocos2d::Point tan_dst, float speed, int tan_type, int draw_type, int after_type, int density, int value, float half_diameter, int endTime){
	tan_drawing::forRun = Run;
	tan_drawing::str = str;
	tan_drawing::dst = dst;
	tan_drawing::speed = speed;
	tan_drawing::tan_type = tan_type;
	tan_drawing::draw_type = draw_type;
	tan_drawing::density = density;
	tan_drawing::value = value;
	tan_drawing::half_diameter = half_diameter;
	tan_drawing::prevMakedTime = 0;
	tan_drawing::after_type = after_type;
	tan_drawing::tan_dst = tan_dst;
	tan_drawing::gap_between = sqrtf(float_square(str.x - dst.x) + float_square(str.y - dst.y)) / 2.0f;
	tan_drawing::angle_density = 1.0f*M_PI / (float) value;
	if (str.x < dst.x){
		angle_density *= -1;
	}
	tan_drawing::center = Point(getSmaller(str.x, dst.x) + (getBigger(str.x, dst.x) - getSmaller(str.x, dst.x)) / 2.0f, getSmaller(str.y, dst.y) + (getBigger(str.y, dst.y) - getSmaller(str.y, dst.y)) / 2.0f);
	if (str.x == dst.x){
		if (dst.y < str.y)
			tan_drawing::angle = 90 * M_PI / 180.0f;
		else
			tan_drawing::angle = 270 * M_PI / 180.0f;
	}
	tan_drawing::angle = atanf(tanf(abs((str.y - dst.y) / (str.x - dst.x))));
	if (dst.y < str.y)
		tan_drawing::angle += M_PI;
	if (dst.x > str.x)
		tan_drawing::angle += 0.5f * M_PI;
	tan_drawing::interval = sqrt((str.x - dst.x)*(str.x - dst.x) + (str.y - dst.y)*(str.y - dst.y)) / (tan_drawing::value - 1);
	tan_drawing::stat = 4;
	tan_drawing::endTime = endTime;
	if (str.x - dst.x < 0)
		oval_radial = atan2f(abs(str.x - dst.x), abs(str.y - dst.y)) + (90.0f * M_PI / 180);
	else
		oval_radial = oval_radial = atan2f(abs(str.y - dst.y), abs(str.x - dst.x));
	if (str.y - dst.y < 0)
		oval_radial += M_PI;
}

tan_drawing::~tan_drawing(){
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
		delete tanmak.at(i);
		tanmak.erase(tanmak.begin() + i--);
	}
}

void tan_drawing::make(){
	if (draw_type == 1 && stat == 4){
		switch (after_type){
		case 1:
		case 2:{tanmak.push_back(new enemyTan(tan_type, str.x, str.y, speed, 0.0f, 0, 0));}break;
		case 3:tanmak.push_back(new enemyTan(tan_type, str.x, str.y, speed, rand() % 360 / 180.0f, 0, 0));
		}
		forRun->addChild(tanmak.at(tanmak.size() - 1)->getSprite());
		str = Point(str.x + interval * sinf(angle), str.y + interval * cosf(angle));
	}
	else if (draw_type == 1 && stat == 4){
		float new_x = gap_between * cosf(rotate_angle) * cosf(oval_radial) - half_diameter * sinf(rotate_angle) * sinf(oval_radial);
		float new_y = gap_between * cosf(rotate_angle) * sinf(oval_radial) + half_diameter * sinf(rotate_angle) * cosf(oval_radial);
		new_x += center.x;
		new_y += center.y;
		switch (after_type){
		case 1:
		case 2:{tanmak.push_back(new enemyTan(tan_type, new_x, new_y, speed, 0.0f, 0, 0)); }break;
		case 3:tanmak.push_back(new enemyTan(tan_type, new_x, new_y, speed, rand() % 360 / 180.0f, 0, 0));
		}
		forRun->addChild(tanmak.at(tanmak.size() - 1)->getSprite());
		rotate_angle += angle_density;
	}
	if (tanmak.size() == tan_drawing::value){
		stat = 1;
	}
}

void tan_drawing::move(){
	if (stat > 1 || forRun->get_time_counter() < endTime)
		return;
	if (after_type != 3 && forRun->get_time_counter() == endTime){
		if (tan_dst.x == -1.0f)
			tan_dst = forRun->playerPoint;
		for (int i = 0; i < tanmak.size(); i++){
			if (tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().x == tan_dst.x){
				if (tan_dst.y < tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().y)
					tan_angle = 90 * M_PI / 180.0f;
				else
					tan_angle = 270 * M_PI / 180.0f;
			}
			if (tan_dst.x > tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().x){
				if (tan_dst.y < tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().y){
					tan_angle = atan2f(abs(tan_dst.x - tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().x), abs(tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().y - tan_dst.y));
					tan_angle += 1.5f * M_PI;
				}
				else{
					tan_angle = atan2f(abs(tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().y - tan_dst.y), abs(tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().x - tan_dst.x));
				}
			}
			else{
				if (tan_dst.y < tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().y){
					tan_angle = atan2f(abs(tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().y - tan_dst.y), tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().x - tan_dst.x);
					tan_angle += 1.0f * M_PI;
				}
				else{
					tan_angle = atan2f(abs(tan_dst.x - tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().x), abs(tanmak.at(tanmak.size() - 1)->getSprite()->getPosition().y - tan_dst.y));
					tan_angle += 0.5f * M_PI;
				}
			}

			tanmak.at(tanmak.size() - 1)->setAngle((tan_angle * 180 / M_PI) / 360.0f);
		}
	}
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->move();
		if (!tanmak.at(i)->isAlive){
			tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
			delete(tanmak.at(i));
			tanmak.erase(tanmak.begin() + i);
			i--;
		}
		else if ((forRun->isPlayerLive && !forRun->isPlayerOP) && tanmak.at(i)->getSprite()->getBoundingBox().intersectsRect(forRun->playerBoundingBox)){
			float d_x = abs(tanmak.at(i)->getSprite()->getPositionX() - forRun->playerPoint.x);
			float d_y = abs(tanmak.at(i)->getSprite()->getPositionY() - forRun->playerPoint.y);
			float temp = sqrtf(d_x*d_x + d_y*d_y);
			if (temp < tanmak.at(i)->getSprite()->getBoundingBox().getMaxX() - tanmak.at(i)->getSprite()->getBoundingBox().getMidX() - 8){
				forRun->playerDeadTriger();
				tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
				delete(tanmak.at(i));
				tanmak.erase(tanmak.begin() + i);
				i--;
			}
		}
	}

	if (tanmak.size() == 0){
		stat = 0;
	}
}

void tan_drawing::set_additional_info(float speed, int angle, float angle_acc){
	if (speed != -1){
		//		this->speed = speed;
		for (enemyTan *temp : tanmak){
			temp->setSpeed(speed);
		}
	}
	if (angle != -1.0f){
		for (enemyTan *temp : tanmak){
			temp->setAngle(temp->getAngle() + angle / 360.0f);
		}
	}
	if (angle_acc != -1.0f){
		for (enemyTan *temp : tanmak){
			temp->setAngle_acc(angle_acc);
		}
	}
}

int tan_drawing::get_state(){
	return tan_drawing::stat;
}

int tan_drawing::get_density(){
	return tan_drawing::density;
}

void tan_drawing::set_stat(int stat){
	tan_drawing::stat = stat;
}

void tan_drawing::add_prevMarkedTime(){
	prevMakedTime++;
}

int tan_drawing::get_prevMarkedTime(){
	return prevMakedTime;
}

void tan_drawing::set_parent_tanmak(int tag){
	parentTag = tag;
}

void tan_drawing::set_tag(int tag){
	tan_drawing::tag = tag;
}

int tan_drawing::get_tag(){
	return tan_drawing::tag;
}

void tan_drawing::set_center(cocos2d::Point pos){
	return;
}

int tan_drawing::get_parent_tag(){
	return parentTag;
}

void tan_drawing::removecurrentTan(){
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
		delete(tanmak.at(i));
		tanmak.erase(tanmak.begin() + i);
		i--;
	}
}