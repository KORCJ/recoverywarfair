/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef _player_tan_control_
#define _player_tan_control_
#include "cocos2d.h"
#include "mainRogic.h"

class player_tan_control{
private:
	int speed, density, prevMakedTime = 0, total_size = 0;
	int make_density, make_prevMakedTime = 0;
	std::vector<cocos2d::Sprite*> playerTan;
	mainRogic *forRun;
	cocos2d::Texture2D* texture;
	char png_name[20];	
public:
	~player_tan_control();

	player_tan_control(mainRogic *run, int speed, int density, int make_density, int total_size, int tanType);

	std::vector<cocos2d::Sprite*> get_playerTanVector();

	void add_prevMarkedTime();

	int get_prevMarkedTime();

	int get_density();

	void add_make_prevMarkedTime();

	int get_make_prevMarkedTime();

	int get_make_density();

	void move();

	void make();

};
#endif