/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#define CURVE_CW 1
#define CURVE_CCW 0

float added_angle(float angle);
float adjust_angle(double angle);
float adjust_angle_rad(float &angle);
float getBigger(float x1, float x2);
float getSmaller(float x1, float x2);