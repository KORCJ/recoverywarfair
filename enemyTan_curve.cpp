/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include"enemyTan_curve.h"
#include "toolBoxxxxx.h"

#define float_square(x) (x)*(x)

USING_NS_CC;

enemyTan_curve::enemyTan_curve(int tan_type, int total_point, std::vector<cocos2d::Point> center, std::vector<float> ovalRate, std::vector<char> direction, float rotate_angle_acc){
	//	log("init");
	char temp_char[25] = { 0, };
	sprintf(temp_char, "jako_tan_0%d.png", tan_type);
	tanSprite = Sprite::createWithSpriteFrameName(temp_char);

	enemyTan_curve::tanSprite->setScale(0.7f);

	enemyTan_curve::rotate_angle = 0.0f;

	enemyTan_curve::rotate_angle_acc = rotate_angle_acc;

	enemyTan_curve::lenth = 15.0f;

	enemyTan_curve::total_point = total_point;

	enemyTan_curve::center = center;

	enemyTan_curve::ovalRate = ovalRate;

	enemyTan_curve::direct = direction;

	enemyTan_curve::tan_type = tan_type;

	isAlive = true;

	flag = 0;

	//맨 첫번째 플래그와 두번째 플레그의 사이에서 이동할 값 정의
	float temp_x = getBigger(center[flag].x, center[flag + 1].x) - getSmaller(center[flag].x, center[flag + 1].x);
	float temp_y = getBigger(center[flag].y, center[flag + 1].y) - getSmaller(center[flag].y, center[flag + 1].y);
	if (center[flag].y < center[flag + 1].y){
		if (center[flag].x > center[flag + 1].x)
			oval_radial = atan2f(temp_x, temp_y) + 1.5f * M_PI;
		else
			oval_radial = atan2f(temp_y, temp_x) + M_PI;
	}
	else{
		if (center[flag].x < center[flag + 1].x)
			oval_radial = atan2f(temp_x, temp_y) + 0.5f * M_PI;
		else
			oval_radial = atan2f(temp_y, temp_x);
	}
	rotate_angle = 0;
	gap_between = sqrtf(float_square(center[flag].x-center[flag + 1].x) + float_square(center[flag].y-center[flag + 1].y)) / 2.0f;
}

void enemyTan_curve::setSpeed(float speed){
	rotate_angle_acc = speed;
}

float enemyTan_curve::getAngle(){
	return enemyTan_curve::angle;
}

cocos2d::Sprite *enemyTan_curve::getSprite(){
	return enemyTan_curve::tanSprite;
}

bool enemyTan_curve::getStat(){
	return isAlive;
}

void enemyTan_curve::updateStat(){
	if (-100.0f > tanSprite->getPositionX()
		|| tanSprite->getPositionX() > 868.0f
		|| -100.0f > tanSprite->getPositionY()
		|| tanSprite->getPositionY() > 1124.0f){
		isAlive = false;
	}
}

void enemyTan_curve::move(){

	float new_x, new_y;
	//원형 이동
	float beta_tan_value;

	if (rotate_angle == 90.0f * M_PI / 180.0f){
		new_x = ovalRate[flag];
		new_y = 0.0f;
	}
	else if (rotate_angle == 270.0f * M_PI / 180.0f){
		new_x = -ovalRate[flag];
		new_y = 0.0f;
	}
	else{
		new_x = gap_between * cosf(rotate_angle) * cosf(oval_radial) - ovalRate[flag] * sinf(rotate_angle) * sinf(oval_radial);
		new_y = gap_between * cosf(rotate_angle) * sinf(oval_radial) + ovalRate[flag] * sinf(rotate_angle) * cosf(oval_radial);
		new_x += getSmaller(center[flag].x, center[flag + 1].x) + (getBigger(center[flag].x, center[flag + 1].x) - getSmaller(center[flag].x, center[flag + 1].x)) / 2.0f;
		new_y += getSmaller(center[flag].y, center[flag + 1].y) + (getBigger(center[flag].y, center[flag + 1].y) - getSmaller(center[flag].y, center[flag + 1].y)) / 2.0f;
	}
	//설정된 방향에 따라 이동
	if (direct[flag]==CURVE_CCW)
		rotate_angle += rotate_angle_acc;
	else
		rotate_angle -= rotate_angle_acc;
	rotate_angle = adjust_angle_rad(rotate_angle);
	///반바퀴 도는 과정이 끝난 경우.
	//단 마지막이였던 경우, 이 연산은 생략한다.
	if ((rotate_angle >= M_PI || rotate_angle <= -M_PI || rotate_angle == 0)){
		if (flag >= center.size() - 2){
			isAlive = false;
			return;
		}
		//플래그 증가
		flag++;
		//다음 좌표와 그 다음 좌표 사이에 값을 정의
		float temp_x = getBigger(center[flag].x, center[flag + 1].x) - getSmaller(center[flag].x, center[flag + 1].x);
		float temp_y = getBigger(center[flag].y, center[flag + 1].y) - getSmaller(center[flag].y, center[flag + 1].y);
		if (center[flag].y < center[flag + 1].y){
			if (center[flag].x > center[flag + 1].x)
				oval_radial = atan2f(temp_x, temp_y) + 1.5f * M_PI;
			else
				oval_radial = atan2f(temp_y, temp_x) + M_PI;
		}
		else{
			if (center[flag].x < center[flag + 1].x)
				oval_radial = atan2f(temp_x, temp_y) + 0.5f * M_PI;
			else
				oval_radial = atan2f(temp_y, temp_x);
		}
		rotate_angle = 0;
		gap_between = sqrtf(float_square(center[flag].x - center[flag + 1].x) + float_square(center[flag].y - center[flag + 1].y)) / 2.0f;
	}
	if (tan_type == 6 || tan_type == 7){
		tanSprite->setRotation(tanSprite->getRotation() + 60.0f);
	}
	else{
		tanSprite->setRotation(-1 * abs(rotate_angle * oval_radial * 180 / M_PI) - 90.0f);
	}
	enemyTan_curve::tanSprite->setPosition(Point(new_x, new_y));
//	enemyTan_curve::updateStat();
}