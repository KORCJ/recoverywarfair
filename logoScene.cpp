/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "logoScene.h"

Scene* logoScene::createScene(){
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = logoScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool logoScene::init(){
	//////////////////////////////
	// 1. super init first
	if (!LayerColor::initWithColor(Color4B(0, 0, 0, 0)))
	{
		return false;
	}

	auto logo = Sprite::create("before_menu_logo.png");
	logo->setPosition(Point(384.0f, 512.0f));
	this->addChild(logo);
	this->scheduleOnce(schedule_selector(logoScene::call_menu), 2.0f);
	return true;
}

void logoScene::call_menu(float dt){
	Director::getInstance()->pushScene(MenuSelectScene::createScene());
}

