/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "mainRogic.h"
#include "tan_basic.h"
#include "tan_001.h"
#include "tan_wram.h"
#include "tan_curve.h"
#include "tan_drawing.h"
#include "toolBoxxxxx.h"
#include "tan_hurricane.h"
#include "player_tan_cotrol.h"
#include "sub_player_control.h"
#include "neverb_tan_control.h"
#include "SimpleAudioEngine.h"  
#include "MenuSelectScene.h"
USING_NS_CC;

std::vector<tan_basic*> tantan;

std::vector<jako_control*> jakos;

//100과 0을 동일시 시키는 함수
float make_max_one(float value){
	if (value > 100.0)
		return 0;
	else if (value < -100.0)
		return 0;
	else if (value == 0)
		return 0;
	else
		return value;
}

Scene* mainRogic::createScene(int player_type, int stage_type)
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = mainRogic::create();

	//setPlayer type
	layer->player_type = player_type;

	layer->replace_player(0);

	char boss_name[30] = { 0, };
	sprintf(boss_name, "boss/boss_stage_01_%d.plist", player_type);
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile(boss_name);

	char boss_intro_name[30] = { 0, };
	sprintf(boss_intro_name, "boss/boss_intro_%d.plist", stage_type);
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile(boss_intro_name);

	//스테이지 테스트///////////////////////
	char stage_filename[20] = { 0, };
	sprintf(stage_filename, "stage_%02d.txt", stage_type);
	layer->stage_type = stage_type;
	layer->stageFile = CCFileUtils::sharedFileUtils()->getFileData(CCFileUtils::sharedFileUtils()->fullPathForFilename(stage_filename).c_str(), "r", &layer->fileSize);
	if (stage_type != 2){
		sprintf(stage_filename, "boss_%02d.txt", stage_type);
		layer->bossFile = CCFileUtils::sharedFileUtils()->getFileData(CCFileUtils::sharedFileUtils()->fullPathForFilename(stage_filename).c_str(), "r", &layer->bossfileSize);

		sprintf(stage_filename, "boss_final_%02d.txt", stage_type);
		layer->bossfinalFile = CCFileUtils::sharedFileUtils()->getFileData(CCFileUtils::sharedFileUtils()->fullPathForFilename(stage_filename).c_str(), "r", &layer->bossfinalfileSize);
	}
	if (layer->pref_data.getforBGM()){
		char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
		sprintf(soundLocation, "sdForIOS/stage_%02d_field.m4a", stage_type);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		sprintf(soundLocation, "sdForAndroid/stage_%02d_field.ogg", stage_type);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		sprintf(soundLocation, "sdForPC/stage_%02d_field.wav", stage_type);
#endif
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(soundLocation, false);
	}
	////////////////////////
	///////////
	///////////
	/////////
//////////////
	///////////

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool mainRogic::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0)))
    {
        return false;
    }
	BossisRunning = false;
	//버튼 처리 on
	this->setKeypadEnabled(true);
	pref_data = LocalPrefernceData();

	Director::getInstance()->getTextureCache()->destroyInstance();
	Director::getInstance()->getTextureCache()->removeAllTextures();

	//여기부터 시작
	Size ScreenSize = Size(768, 1024);

	uiCache = SpriteFrameCache::getInstance();
	uiCache->removeSpriteFrames();
	uiCache->addSpriteFramesWithFile("etcui.plist");
	uiCache->addSpriteFramesWithFile("fever.plist");
	uiCache->addSpriteFramesWithFile("recovering.plist");
	uiCache->addSpriteFramesWithFile("combo.plist");
	uiCache->addSpriteFramesWithFile("jakoTan.plist");
	uiCache->addSpriteFramesWithFile("burst.plist");
	uiCache->addSpriteFramesWithFile("burst_tan.plist");
	CCTextureCache::sharedTextureCache()->addImage("particle/bbuyeh.png");
	CCTextureCache::sharedTextureCache()->addImage("particle/player_broken_particle.png");
	CCTextureCache::sharedTextureCache()->addImage("particle/yukgak.png");
	CCTextureCache::sharedTextureCache()->addImage("particle/attack.png");
	CCTextureCache::sharedTextureCache()->addImage("particle/startBooster.png");
	CCTextureCache::sharedTextureCache()->addImage("particle/Booster.png");
	uiSprite = Sprite::create();

	auto mainFrame = Sprite::create("mainui_line.png");
	mainFrame->setPosition(Point(ScreenSize.width * 0.48, ScreenSize.height * 0.5));
	uiSprite->addChild(mainFrame);

	jako_hps[1] = 23;
	jako_hps[2] = 38;
	jako_hps[3] = 3;
	jako_hps[4] = 23;
	jako_hps[5] = 38;
	
	jako_hps[6] = 40;
	jako_hps[7] = 50;
	jako_hps[8] = 10;

	jako_hps[16] = 15;
	jako_hps[17] = 10;
	
	jako_hps[30] = 600;
	jako_hps[32] = 350;
	jako_hps[42] = 2100;
	
	jako_hps[22] = 30;
	jako_hps[31] = 200;
	jako_hps[33] = 200;
	jako_hps[40] = 2100;

	//내부 스프라이트
	recoveringSprite = Sprite::createWithSpriteFrameName("1..png");
	recoveringSprite->setPosition(Point(ScreenSize.width * 0.948, ScreenSize.height * 0.452));
	recoveringSprite->setName("recoveringSprite");
	uiSprite->addChild(recoveringSprite, 1);

	feverSprite = Sprite::createWithSpriteFrameName("fever0.png");
	feverSprite->setPosition(Point(ScreenSize.width * 0.17, ScreenSize.height * 0.141));
	feverSprite->setName("fever");
	uiSprite->addChild(feverSprite, 1);
	
	auto status = Sprite::createWithSpriteFrameName("status.png");
	status->setPosition(Point(ScreenSize.width * 0.112, ScreenSize.height * 0.892));
	status->setName("status");
	uiSprite->addChild(status);

//	CC_CALLBACK_1(mainRogic::bombTriger, this)

	auto burst_btn = MenuItemSprite::create(Sprite::createWithSpriteFrameName("buster.png"), Sprite::createWithSpriteFrameName("buster.png"), CC_CALLBACK_1(mainRogic::bombTriger, this));
	burst_btn->setPosition(Point(ScreenSize.width * 0.17, ScreenSize.height * 0.03f));
	burst_btn->setName("burst_btn");
	burst_btn->setScale(0.4f);

	auto uiSpriteMenu = Menu::create(burst_btn, NULL);
	uiSpriteMenu->setPosition(Point::ZERO);
	uiSprite->addChild(uiSpriteMenu, 1, "uiSpriteMenu");
	
	auto pause = MenuItemSprite::create(Sprite::createWithSpriteFrameName("pause.png"), Sprite::createWithSpriteFrameName("pause.png"), CC_CALLBACK_1(mainRogic::pause_func, this));
	pause->setPosition(Point(ScreenSize.width * 0.879, ScreenSize.height * 0.974));
	uiSpriteMenu->addChild(pause, 1, "pause");
	nowPauseing = false;

	uiSprite->setPosition(Point::ZERO);

	this->addChild(uiSprite, 1, "uiSprite");
	///
	isGameEnd = false;
	//초기화 변수
	isGoin = true;
	left_clicked = false;
	save_time_s = 0;
	save_time_ss = 0;
	prev_mouse_pos.first = 389.0f;
	prev_mouse_pos.second = 160.0f;
	stage_cursor = -1;
	time_counter = 0;
	total_tanmak = 0;
	total_jako = 0;
	strean_scanf_totalCounter = 0;
	////초기화 끝

	//마우스 리스너 로드
//	auto m_listener = EventListenerMouse::create();
//	m_listener->onMouseDown = CC_CALLBACK_1(mainRogic::switch_mouse_stat, this);
//	m_listener->onMouseMove = CC_CALLBACK_1(mainRogic::mov_player, this);
//	_eventDispatcher->addEventListenerWithSceneGraphPriority(m_listener, this);
	auto t_listener = EventListenerTouchOneByOne::create();
	t_listener->setSwallowTouches(false);
	t_listener->onTouchBegan = CC_CALLBACK_2(mainRogic::onTouchBegined, this);
	t_listener->onTouchMoved = CC_CALLBACK_2(mainRogic::onTouchMoved, this);
	t_listener->onTouchEnded = CC_CALLBACK_2(mainRogic::onTouchEnd, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(t_listener, this);
	////마우스 리스너 로드 끝

	current_running_time = current_fever_value = current_combo_value = 0;

	current_score_value = viewing_score_value = 0;

	life = bomb = 2;
	//////////////////////////////객체, 위치,//////// 속도, 초기각도, 각도,// 각속도, 갯수, 스프라이트 종류

	upupdowndown = 0;

	auto scoreFNT = LabelBMFont::create("0", "score_font.fnt", 1.0f);
	scoreFNT->setScale(1.2f);
	scoreFNT->setAnchorPoint(Vec2(0.0, 0.5));
	scoreFNT->setPosition(Point(150.0f, 1005.0f));
	scoreFNT->setColor(Color3B::WHITE);
	this->addChild(scoreFNT, 1 , "scoreFNT");

	auto lifeFNT = LabelBMFont::create("2", "score_font.fnt", 1.3f);
	lifeFNT->setAnchorPoint(Vec2(0.0, 0.5));
	lifeFNT->setPosition(Point(130.0f, 930.0f));
	lifeFNT->setColor(Color3B::WHITE);
	this->addChild(lifeFNT, 1, "lifeFNT");

	auto bombFNT = LabelBMFont::create("2", "score_font.fnt", 1.3f);
	bombFNT->setAnchorPoint(Vec2(0.0, 0.5));
	bombFNT->setPosition(Point(130.0f, 900.0f));
	bombFNT->setColor(Color3B::WHITE);
	this->addChild(bombFNT, 1, "bombFNT");


	auto game_background = Sprite::create("ingame_bg.png");
	game_background->setPosition(Point(384.0f, 512.0f));
	this->addChild(game_background, -5, "ingame_background");
	game_background->setOpacity(125.0f);

	auto game_background_2nd = Sprite::create("ingame_bg_2nd.png");
	game_background_2nd->setPosition(Point(384.0f, 512.0f + 1024.0f));
	this->addChild(game_background_2nd, -5, "ingame_background_2nd");
	game_background_2nd->setOpacity(125.0f);

	auto game_background_fader = Sprite::create("ingame_bg_fader.png");
	game_background_fader->setPosition(Point(384.0f, 312.0f));
	this->addChild(game_background_fader, -4);

	auto game_background_fader_2nd = Sprite::create("ingame_bg_fader.png");
	game_background_fader_2nd->setPosition(Point(384.0f, 712.0f));
	game_background_fader_2nd->setRotation(180.0f);
	this->addChild(game_background_fader_2nd, -4);

	skiiiiiiiiping = false;

//	this->schedule(schedule_selector(mainRogic::test_tan_01), 1.0f / 100.0f);

	mainRogic::scheduleUpdate();
	return true;
}

void mainRogic::onKeyReleased(cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event){
	if (keycode == EventKeyboard::KeyCode::KEY_BACK || keycode == EventKeyboard::KeyCode::KEY_ESCAPE) {
		mainRogic::pause_func(this);
	}
}

void mainRogic::bombTriger(cocos2d::Object *pSender){
	if (bomb == 0 || skiiiiiiiiping || isGameEnd || !isPlayerLive)
		return;
	current_fever_value = 0;
	//폭탄 트리거
	{
		auto burst_sprite = Sprite::createWithSpriteFrameName("Comp 2_00000.png");

		burst_sprite->setPosition(Point(384.0f, 512.0f));
		burst_sprite->setScale(2.0f);
		burst_sprite->setOpacity(230.0f);
		this->addChild(burst_sprite, 10);

		auto burst_animation = Animation::create();
		burst_animation->setDelayPerUnit(0.025);
		char temp_spriteName[100] = { 0, };
		for (int i = 0; i <= 59; i++){
			sprintf(temp_spriteName, "Comp 2_%05d.png", i);
			burst_animation->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_spriteName));
		}
		burst_sprite->runAction(Sequence::create(Animate::create(burst_animation), CallFunc::create(this, callfunc_selector(mainRogic::bombTriger_2nd)), CallFunc::create(burst_sprite, callfunc_selector(Sprite::removeFromParent)), NULL));
		skiiiiiiiiping = true;
	}
}

void mainRogic::bombTriger_2nd(){
	//실제 폭탄
	isPlayerOP = true;
	{
		auto burst_sprite_tan = Sprite::createWithSpriteFrameName("Comp 1_00000.png");

		burst_sprite_tan->setPosition(playerPoint - Point(15.0f, 0.0f));
		burst_sprite_tan->setScale(2.0f);
		burst_sprite_tan->setAnchorPoint(Vec2(0.5f, 0.0f));
		this->addChild(burst_sprite_tan, 2, "burst_tan");

		auto burst_tan_animation = Animation::create();
		burst_tan_animation->setDelayPerUnit(0.06);
		char temp_spriteName[100] = { 0, };
		for (int i = 0; i <= 94; i += 2){
			sprintf(temp_spriteName, "Comp 1_%05d.png", i);
			burst_tan_animation->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_spriteName));
		}
		burst_sprite_tan->runAction(Sequence::create(Animate::create(burst_tan_animation), CallFunc::create(burst_sprite_tan, callfunc_selector(Sprite::removeFromParent)), CallFunc::create(CC_CALLBACK_0(mainRogic::end_OPmode, this, 0)), NULL));
	}
	/*
	{
	auto burst_sprite_tan = Sprite::createWithSpriteFrameName("Comp 1_00000.png");

	burst_sprite_tan->setPosition(Point(playerPoint.x + 150.0f, 512.0f));
	burst_sprite_tan->setScale(2.0f);
	this->addChild(burst_sprite_tan, 2);

	auto burst_tan_animation = Animation::create();
	burst_tan_animation->setDelayPerUnit(0.04);
	char temp_spriteName[100] = { 0, };
	for (int i = 0; i <= 94; i += 2){
	sprintf(temp_spriteName, "Comp 1_%05d.png", i);
	burst_tan_animation->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_spriteName));
	}
	burst_sprite_tan->runAction(Sequence::create(Animate::create(burst_tan_animation), CallFunc::create(burst_sprite_tan, callfunc_selector(Sprite::removeFromParent)), NULL));
	}
	*/
	for (tan_basic *temp : tantan){
		temp->set_stat(0);
	}

	skiiiiiiiiping = false;

	bomb--;
	char temp_char[25] = { 0, };
	sprintf(temp_char, "%d", bomb);
	((LabelBMFont*)this->getChildByName("bombFNT"))->setString(temp_char);
	if (pref_data.getforSFX()){
		char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
		sprintf(soundLocation, "sdForIOS/burst.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		sprintf(soundLocation, "sdForAndroid/burst.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		sprintf(soundLocation, "sdForPC/burst.wav");
#endif
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(soundLocation);
	}
	this->schedule(schedule_selector(mainRogic::addDamageFromBomb), 0.2, 10, 0.7);
}

void mainRogic::addDamageFromBomb(float delta){
	for (int i = 0; i < jakos.size(); i++){
		jakos.at(i)->bomb_damage(40);
	}
	for (int i = 0; i < tantan.size(); i++){
		tantan.at(i)->removecurrentTan();
	}
	if (BossIslive){
		boss->bomb_damage(50);
	}
}

void mainRogic::playerDeadTriger(){
	if (!isPlayerLive || isPlayerOP || isGameEnd)
		return;
	isPlayerLive = false;

	//launch particle

	char particleName[30] = { 0, };

	if (player_type == 1)
		sprintf(particleName, "particle/vsuri_broken.plist");
	else if (player_type == 2)
		sprintf(particleName, "particle/bal_broken.plist");
	else
		sprintf(particleName, "particle/never_broken.plist");
	CCParticleSystem* particleSystem = CCParticleSystemQuad::create(particleName);
	
	particleSystem->setPosition(playerPoint);
	particleSystem->setAutoRemoveOnFinish(true);
	this->addChild(particleSystem);

	particleSystem = NULL;

	if (BossIslive)
		deadFromBoss++;

	//retain env
	if (player_tan != NULL){
		this->getChildByName("player")->setVisible(false);
		player->setPosition(Point(389,160));
		playerPoint = player->getPosition();
		playerBound->setPosition(playerPoint);
		playerBoundingBox = playerBound->getBoundingBox();
		delete(player_tan);
		player_tan = NULL;
		life--;
	}
	if (player_type == 2){
		if (sub_player != NULL){
			delete(sub_player);
			sub_player = NULL;
		}
	}
	if (player_type == 3){
		if (neverb_sub_player != NULL){
			delete(neverb_sub_player);
			neverb_sub_player = NULL;
		}
	}

	if (life < 0){
		isGameEnd = true;
		show_result_window(0);
	}
	else{
		this->scheduleOnce(schedule_selector(mainRogic::replace_player), 3);
	}
}

void mainRogic::end_OPmode(float dt){
	isPlayerOP = false;
	((Sprite*)this->getChildByName("player"))->setOpacity(255.0f);
}

void mainRogic::bossTriger(){
	//보스등장 애니메이션 트리거
	auto intro_sprite = Sprite::createWithSpriteFrameName("boss_intro_01.png");

	intro_sprite->setPosition(Point(384.0f, 512.0f));
	intro_sprite->setScale(2.0f);
	intro_sprite->setOpacity(230.0f);
	this->addChild(intro_sprite, 10, "boss_intro");

	auto intro_animation = Animation::create();
	intro_animation->setDelayPerUnit(0.046);
	char temp_spriteName[100] = { 0, };
	for (int i = 1; i <= 49; i++){
		sprintf(temp_spriteName, "boss_intro_%02d.png", i);
		intro_animation->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_spriteName));
	}
	intro_sprite->setScale(4.0f);
	intro_sprite->runAction(Sequence::create(Animate::create(intro_animation), CallFunc::create(this, callfunc_selector(mainRogic::end_bossTriger)), NULL));
	skiiiiiiiiping = true;
}

void mainRogic::end_bossTriger(){
	skiiiiiiiiping = false;
	if (pref_data.getforBGM()){
		char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
		sprintf(soundLocation, "sdForIOS/stage_%02d_boss.m4a", stage_type);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		sprintf(soundLocation, "sdForAndroid/stage_%02d_boss.ogg", stage_type);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		sprintf(soundLocation, "sdForPC/stage_%02d_boss.wav", stage_type);
#endif
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(soundLocation, false);
	}
	this->getChildByName("boss_intro")->removeFromParentAndCleanup(true);
}

void mainRogic::replace_player(float dt){
	//폰트
	if (uiSprite->getChildByName("score_") == NULL){
		switch (player_type){
		case 1:{
				   auto score = Sprite::createWithSpriteFrameName("score_1.png");
				   score->setPosition(Point(768.0f * 0.093, 1024.0f * 0.978));
				   uiSprite->addChild(score, 10, "score_");
		}break;
		case 2:{
				   auto score = Sprite::createWithSpriteFrameName("score_2.png");
				   score->setPosition(Point(768.0f * 0.093, 1024.0f * 0.978)); 
				   uiSprite->addChild(score, 10, "score_");
		}break;
		case 3:{
				   auto score = Sprite::createWithSpriteFrameName("score_3.png");
				   score->setPosition(Point(768.0f * 0.093, 1024.0f * 0.978)); 
				   uiSprite->addChild(score, 10, "score_");
		}
		}
	}
	if (life >= 0){
		if (this->getChildByName("player") == NULL){
			char temp_str[25] = { 0, };
			sprintf(temp_str, "player_%d.png", player_type);
			player = Sprite::create(temp_str);
			player->setPosition(Point(389, 160));
			playerPoint = Point(389, 160);
			player->setScale(0.5);
			this->addChild(player, 0, "player");
			playerBound = Sprite::create("player_bound.png");
			playerBound->setScale(0.3);
			playerBound->setName("player_bound");
			this->addChild(playerBound);
			playerBound->setPosition(player->getPosition());
			//				delete(player_tan);
			bomb = 2;
			isPlayerOP = false;
		}
		else{
			isPlayerOP = true;
			((Sprite*)this->getChildByName("player"))->setOpacity(150.0f);
			this->scheduleOnce(schedule_selector(mainRogic::end_OPmode), 3);
		}
			playerBoundingBox = playerBound->getBoundingBox();
			this->getChildByName("player")->setVisible(true);
		//플레이어 불러오기
		if (player_type == 3)
			player_tan = new player_tan_control(this, 0, 0, 0, 0, player_type);
		else
			player_tan = new player_tan_control(this, 30, 0, 3, 8, player_type);
		////플레이어 로드 끝
		////////////
		//조준탄막//
		////////////
		if (player_type == 2){
//			delete(sub_player);
			sub_player = new sub_player_control(this);
			sub_player->add_more_subPlayer();
		}
		if (player_type == 3){
			neverb_sub_player = new neverb_tan_control(this, 30, 0, 0);
		}
		char temp_char[25] = { 0, };
		sprintf(temp_char, "%d", life);
		((LabelBMFont*)this->getChildByName("lifeFNT"))->setString(temp_char);

		bomb = 2;
		sprintf(temp_char, "%d", bomb);
		((LabelBMFont*)this->getChildByName("bombFNT"))->setString(temp_char);
		isPlayerLive = true;
	}
	else{
	//end of game
	}
}

void mainRogic::add_combo(){
	if (current_fever_value == 48){
		if (this->getChildByName("comboSprite") == NULL){
			//콤보 스프라이트
			auto comboSprite = Sprite::create();
			this->addChild(comboSprite, 1, "comboSprite");

			auto combo = Sprite::createWithSpriteFrameName("combo.png");
			combo->setPosition(Point(768.0f * 0.849, 1024.0f * 0.884));
			combo->setOpacity(0);
			combo->runAction(FadeIn::create(0.2));
			combo->runAction(MoveTo::create(0.2, Point(768.0f * 0.829, 1024.0f * 0.884)));
			combo->setName("combo");
			this->getChildByName("uiSprite")->addChild(combo);
		}

		current_combo_value += 2;
		if (current_combo_value > 20)
			current_combo_value = 20;
		combo_counter = 0;
		char temp_char[20] = { 0, };
		sprintf(temp_char, "x%d.png", current_combo_value);
		((Sprite*)this->getChildByName("comboSprite"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_char));
		((Sprite*)this->getChildByName("comboSprite"))->setPosition(Point(768.0f * 0.680, 1024.0f * 0.904));
		((Sprite*)this->getChildByName("comboSprite"))->runAction(MoveTo::create(0.1, Point(768.0f * 0.680, 1024.0f * 0.884)));
	}
}

void mainRogic::end_of_combo(){
	//콤포 끝내기
	combo_counter++;
	if (combo_counter == 120 && this->getChildByName("comboSprite") != NULL){
		combo_counter = 0;
		current_combo_value = 0;
		this->getChildByName("comboSprite")->removeFromParentAndCleanup(true);
		this->getChildByName("uiSprite")->getChildByName("combo")->removeFromParentAndCleanup(true);
	}
}

void mainRogic::refresh_score(){
	//점수 업데이트
	if (viewing_score_value < current_score_value){
		char temp_char[50] = { 0, };
		viewing_score_value += 10;
		if (viewing_score_value > current_score_value)
			viewing_score_value = current_score_value;
		sprintf(temp_char, "%llu", viewing_score_value);
		((LabelBMFont*)this->getChildByName("scoreFNT"))->setString(temp_char);
	}
}

void mainRogic::add_score(int jako_num){
	//점수 추가
	viewing_score_value = current_score_value;
	unsigned long long temp = 0;
	if (0 <= jako_num && jako_num <= 29){
//		current_score_value
		temp += 200;
		if (current_combo_value != 0)
			temp *= sqrtf(current_combo_value * current_combo_value * current_combo_value);
	}
	else if (30 <= jako_num && jako_num <= 39){
		temp += 1000;
		if (current_combo_value != 0)
			temp *= sqrtf(current_combo_value * current_combo_value * current_combo_value);
		add_combo();
	}
	else if(jako_num < 50){
		temp += 10000;
		if (current_combo_value != 0)
			temp *= sqrtf(current_combo_value * current_combo_value * current_combo_value);
		add_combo();
	}
	if (jako_num == 100){
		current_score_value += 100000000 - 1000000 * deadFromBoss;
		viewing_score_value = current_score_value - 4000;
	}
	else{
		current_score_value += temp;
	}
}

void mainRogic::set_playerStatFlag(bool stat){
	mainRogic::playerStatFlag = stat;
}

bool mainRogic::get_playerStat(){
	return playerStatFlag;
}

void mainRogic::test_tan_01(float dt){
//	time_counter++;
}

void mainRogic::update_save_time(float dt){
//	log("update");
	/*
	char ll[100]{0, };
	sprintf(ll, "save = %d", save_time_s);
	save_time_ss++;
	if (save_time_ss == 100){
		save_time_ss = 0;
		save_time_s++;                                                                                                                                                                                                                                                                                                                                                                                        
	}
	log(ll);
	*/
}

void mainRogic::show_result_window(int type){
	//결과창 출력
	auto result_window = Sprite::create();
	viewing_score_value = current_score_value - 1;
	
	char temp_score_dexer[50] = { 0, };
	sprintf(temp_score_dexer, "%llu", current_score_value);
	auto re_score_FNT = LabelBMFont::create(temp_score_dexer,"result_font.fnt");
	re_score_FNT->setAnchorPoint(Vec2(0.0f, 0.5f));
	re_score_FNT->setScale(1.2f);

	auto exit_btn = MenuItemImage::create("ongoing_exit.png", "ongoing_exit.png", CC_CALLBACK_1(mainRogic::result_exit , this));

	auto btn_menu = Menu::create();
	
	btn_menu->setPosition(Point::ZERO);

	btn_menu->addChild(exit_btn);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();

	if (type == 1){
		//클리어시
		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
		if (pref_data.getforBGM()){
			char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
			sprintf(soundLocation, "sdForIOS/result.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			sprintf(soundLocation, "sdForAndroid/result.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
			sprintf(soundLocation, "sdForPC/result.wav");
#endif
			CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(soundLocation, true);
		}

		result_window->setSpriteFrame(SpriteFrame::create("result_cleared.png", Rect(0, 0, 698, 251)));
		
		re_score_FNT->setPosition(Point(698 * 0.445, 251 * 0.460f));

		re_score_FNT->setScale(1.5f);

		exit_btn->setPosition(Point(698 * 0.5, -100.0f));
		
		result_window->addChild(re_score_FNT);

		{
			CCParticleSystem* particleSystem = CCParticleSystemQuad::create("particle/startBooster.plist");

			particleSystem->setPosition(player->getPosition());
			particleSystem->setAutoRemoveOnFinish(true);
			particleSystem->setLife(1.0f);
			this->addChild(particleSystem, 5);
			particleSystem = NULL;
		}
		isGameEnd = true;
		this->getChildByName("player_bound")->removeFromParentAndCleanup(true);
		this->schedule(schedule_selector(mainRogic::end_buster_effect));
	}
	else if (type == 2){
		//2스테이지 한정 클리어시
		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
		if (pref_data.getforBGM()){
			char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
			sprintf(soundLocation, "sdForIOS/result.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			sprintf(soundLocation, "sdForAndroid/result.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
			sprintf(soundLocation, "sdForPC/result.wav");
#endif
			CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(soundLocation, true);
		}

		result_window->setSpriteFrame(SpriteFrame::create("result_thanks.png", Rect(0, 0, 698, 251)));

		exit_btn->setPosition(Point(698 * 0.5, -100.0f));

		{
			CCParticleSystem* particleSystem = CCParticleSystemQuad::create("particle/startBooster.plist");

			particleSystem->setPosition(player->getPosition());
			particleSystem->setAutoRemoveOnFinish(true);
			particleSystem->setLife(1.0f);
			this->addChild(particleSystem, 5);
			particleSystem = NULL;
		}
		isGameEnd = true;
		this->getChildByName("player_bound")->removeFromParentAndCleanup(true);
		this->schedule(schedule_selector(mainRogic::end_buster_effect));
	}
	else{
		//게임 실패시
		char ll[20] = { 0, };
		if (BossisRunning){
			sprintf(ll, "%d%%", 90 + BossDamage);
		}
		else{
			sprintf(ll, "%d%%", (int) ((strean_scanf_totalCounter / (float) fileSize) * 90));
		}
		auto re_running_FNT = LabelBMFont::create(ll, "result_font.fnt");
		re_running_FNT->setAnchorPoint(Vec2(0.0f, 0.5f));
		re_running_FNT->setScale(1.2f);

		re_score_FNT->setPosition(Point(698 * 0.449, 251 * 0.524));
		re_running_FNT->setPosition(Point(698 * 0.449, 251 * 0.383));

		result_window->setSpriteFrame(SpriteFrame::create("result_failed.png", Rect(0, 0, 698, 251)));
		result_window->addChild(re_score_FNT);
		result_window->addChild(re_running_FNT);

		exit_btn->setPosition(Point(698 * 0.250, -100.0f));

		//이어하기 버튼
		auto continue_btn = MenuItemImage::create("ongoing_continue.png", "ongoing_continue.png", CC_CALLBACK_1(mainRogic::result_continue, this));
		continue_btn->setPosition(Point(698 * 0.700, -100.0f));

		btn_menu->addChild(continue_btn);
		skiiiiiiiiping = true;
	}
	result_window->addChild(btn_menu);
	result_window->setPosition(Point(384.0f, 512.0f));
	this->addChild(result_window, 3, "result_window");
}

void mainRogic::result_continue(cocos2d::Object *pSender){
	//게임 실패시 이어할 경우
	CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
	this->getChildByName("result_window")->runAction(Sequence::create(MoveTo::create(0.3f, Point(384.0f, 1300.0f)), CallFunc::create(this->getChildByName("result_window"), callfunc_selector(Sprite::removeFromParent)), NULL));
	life = 2;
	current_combo_value = 0;
	current_fever_value = 0;
	isGameEnd = false;
	skiiiiiiiiping = false;
	replace_player(0);
}

void mainRogic::result_exit(cocos2d::Object *pSender){
	//게임 종료 후 퇴장
	this->getChildByName("result_window")->runAction(Sequence::create(MoveTo::create(0.3f, Point(384.0f, -512.0f)), CallFunc::create(this->getChildByName("result_window"), callfunc_selector(Sprite::removeFromParent)), NULL));
	this->scheduleOnce(schedule_selector(mainRogic::call_main_scene), 0.3f);
}

void mainRogic::call_main_scene(float dt){
	//모든 부분 삭제
	for (int i = 0; i < jakos.size(); i++){
		delete(*(jakos.begin() + i));
		jakos.erase(jakos.begin() + i--);
	}
	for (int i = 0; i < tantan.size(); i++){
		delete(*(tantan.begin() + i));
		tantan.erase(tantan.begin() + i--);
	}
	
	this->unscheduleAllSelectors();
	this->removeAllChildrenWithCleanup(true);
	Director::getInstance()->getTextureCache()->destroyInstance();
	Director::getInstance()->getTextureCache()->removeAllTextures();
	Director::getInstance()->replaceScene(MenuSelectScene::createScene());
}

void mainRogic::update_current_running_time(int current_value){
	//현제 진행 바 업데이트
	if (BossisRunning){
		char temp_str[20] = { 0, };
		if (BossDamage < 10){
			sprintf(temp_str, "%d..png", 10 + BossDamage);
			((Sprite*) this->getChildByName("uiSprite")->getChildByName("recoveringSprite"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_str));
		}
	}
	else if(!isGameEnd){
		int left_value = fileSize / 10;
		if (current_value / left_value != current_running_time){
			current_running_time = current_value / left_value;
			char temp_str[20] = { 0, };
			sprintf(temp_str, "%d..png", current_running_time);
			((Sprite*)this->getChildByName("uiSprite")->getChildByName("recoveringSprite"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_str));
		}
	}
}

void mainRogic::switch_mouse_stat(Event *event){
	//PC테스트용 마우스 함수
	//	log("moved");
	EventMouse* evt = (EventMouse*) event;
	left_clicked = !left_clicked;
	prev_mouse_pos.first = evt->getCursorX();
	prev_mouse_pos.second = evt->getCursorY()*-1;
}

bool mainRogic::onTouchBegined(cocos2d::Touch *touch, cocos2d::Event *event){
	return true;
}

void mainRogic::onTouchEnd(cocos2d::Touch *touch, cocos2d::Event *event){}

void mainRogic::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event){
	//터치시 게임내의 오브젝트 위치 이동 함수
	if (!isPlayerLive || skiiiiiiiiping || isGameEnd)
		return;

	Point new_pos = touch->getDelta() + player->getPosition();

	if (new_pos.x < 0){
		player->setPosition(Point(0, new_pos.y));
	}
	else if (new_pos.x > 768){
		player->setPosition(Point(768, new_pos.y));
	}
	if (new_pos.y < 0){
		player->setPosition(Point(new_pos.x, 0));
	}
	else if (new_pos.y > 1024){
		player->setPosition(Point(new_pos.x, 1024));
	}
	if (0 < new_pos.x && new_pos.x < 768 && 0 < new_pos.y && new_pos.y < 1024){
		player->setPosition(new_pos);
	}
	prev_mouse_pos.first = new_pos.x;
	prev_mouse_pos.second = new_pos.y;

	playerPoint = player->getPosition();
	playerBound->setPosition(playerPoint);
	playerBoundingBox = playerBound->getBoundingBox();

	if (this->getChildByName("burst_tan") != NULL)
		this->getChildByName("burst_tan")->setPosition(playerPoint - Point(15.0f ,0.0f));
}

void mainRogic::mov_player(Event *event){
	//플레이어 이동
	EventMouse* evt = (EventMouse*) event;
	if (left_clicked){
		int new_X = player->getPositionX() + make_max_one((evt->getCursorX() - prev_mouse_pos.first))
			, new_Y = player->getPositionY() + make_max_one((prev_mouse_pos.second - (evt->getCursorY()*-1)));
		if (0 < new_X && new_X < 768 && 0 < new_Y&&new_Y < 1024){
			player->setPositionX(new_X);
			player->setPositionY(new_Y);
		}
		//log
/*		char ll[100]{0, };
		sprintf(ll, "delta = %f : %f\nball = %f : %f", (
			prev_mouse_pos.first - evt->getCursorX())
			, prev_mouse_pos.second - (evt->getCursorY()*-1)
			, player->getPositionX()
			, player->getPositionY());
		log(ll);
*/		
		prev_mouse_pos.first = evt->getCursorX();
		prev_mouse_pos.second = evt->getCursorY()*-1;
		
		playerPoint = player->getPosition();
		playerBound->setPosition(playerPoint);
		playerBoundingBox = playerBound->getBoundingBox();
	}
}
void mainRogic::pause_func(cocos2d::Object *pSender){
	//일시정지 버튼
	if (!nowPauseing){
		if (skiiiiiiiiping || isGameEnd)
			return;
		CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
		CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseAllEffects();
		if(this->getChildByName("burst_tan")!=NULL)
			this->getChildByName("burst_tan")->pauseSchedulerAndActions();
		if (pref_data.getforSFX()){
			char soundLocation[100] = { 0, };
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFOMR == CC_PLATFORM_MAC)
			sprintf(soundLocation, "sdForIOS/click.m4a");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			sprintf(soundLocation, "sdForAndroid/click.ogg");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
			sprintf(soundLocation, "sdForPC/click.wav");
#endif
			CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(soundLocation);
		}
		auto pause_bg = Sprite::create("pause_bg.png");
		pause_bg->setPosition(Point(384.0f, 512.0f));
		this->addChild(pause_bg, 5, "pause_bg");

		auto pause_menu = Menu::create();
		pause_menu->setPosition(Point::ZERO);
		pause_bg->addChild(pause_menu);

		auto pause_continue = MenuItemImage::create("pause_continue.png", "pause_continue.png", CC_CALLBACK_1(mainRogic::pause_to_resume_func, this));
		pause_continue->setPosition(Point(596 * 0.201f, 188 * 0.17f));
		pause_menu->addChild(pause_continue);

		auto pause_exit = MenuItemImage::create("pause_exit.png", "pause_exit.png", CC_CALLBACK_0(mainRogic::call_main_scene, this, 0));
		pause_exit->setPosition(Point(596 * 0.773f, 188 * 0.17f));
		pause_menu->addChild(pause_exit);

		nowPauseing = true;
		skiiiiiiiiping = true;
	}
	else{
		mainRogic::pause_to_resume_func(this);
	}
}

void mainRogic::pause_to_resume_func(cocos2d::Object *pSender){
	//일시정지에서 이어 할 경우
	this->getChildByName("pause_bg")->removeAllChildrenWithCleanup(true);
	this->getChildByName("pause_bg")->removeFromParentAndCleanup(true);
	nowPauseing = false;
	skiiiiiiiiping = false;
	CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeAllEffects();
	if (this->getChildByName("burst_tan") != NULL)
		this->getChildByName("burst_tan")->resumeSchedulerAndActions();
}

void mainRogic::doEnd(){
	//게임 끝날경우(PC디버그용)
	this->unscheduleAllSelectors();

	player->removeAllChildrenWithCleanup(true);
//	Scene *pScene = newGame::createScene(save_time_s,save_time_ss);
//	Director::getInstance(0)->replaceScene(TransitionProgressInOut::create(1, pScene));
}

void mainRogic::update_background(){
	//배경화면 업데이트
	this->getChildByName("ingame_background")->setPositionY(this->getChildByName("ingame_background")->getPositionY() - 20);
	this->getChildByName("ingame_background_2nd")->setPositionY(this->getChildByName("ingame_background_2nd")->getPositionY() - 20);
	if (this->getChildByName("ingame_background_2nd")->getPositionY() <= -512.0f){
		this->getChildByName("ingame_background_2nd")->setPositionY(512.0f + 1024.0f);
	}
	else if (this->getChildByName("ingame_background")->getPositionY() <= -512.0f){
		this->getChildByName("ingame_background")->setPositionY(512.0f + 1024.0f);
	}
}

void mainRogic::tanmak_read_func(int parentNum){
	//스테이지 파일에서 탄막 읽어오기
	int outCursor;
	bool flag = true;
	outCursor = strean_scanf_totalCounter;
	strean_scanf(stageFile, "%d", &stage_cursor);
	while (1){
		strean_scanf(stageFile, "%d", &whatIsNext);
		//자코
		if (whatIsNext == 1){
			strean_scanf(stageFile, "%d", &whatMotion);
			switch (whatMotion){
			case 1:{
					   //탈출!
					   strean_scanf_totalCounter = outCursor;
					   flag = false;
					   break;
			}break;
			}
		}
		else if (whatIsNext == 5 || whatIsNext == 6){
			//탈출!
			strean_scanf_totalCounter = outCursor;
			flag = false;
			break;
		}
		//탄막
		else if (whatIsNext == 2){
			strean_scanf(stageFile, "%d", &whatTanmak);
			switch (whatTanmak){
				//n-way탄막
			case 1:{
					   *intValues = { 0, };
					   *floatValues = { 0.0f, };
					   strean_scanf(stageFile, "%d/%f/%d/%f/%f/%f/%d/%d/%d/%d/%d", &intValues[0], &floatValues[0], &intValues[1], &floatValues[1], &floatValues[2], &floatValues[3], &intValues[2], &intValues[3], &intValues[4], &intValues[5], &intValues[6]);
//					   tantan.push_back(new tan_001(this, floatValues[0], floatValues[1], floatValues[2], intValues[0], floatValues[3], floatValues[4], floatValues[5], intValues[1], intValues[2], intValues[3]));
					   /*
					   temptemptemp.typeflag = 2;
					   temptemptemp.startTime = stage_cursor; { int typeflag, startTime, intValue[8]; float floatValue[8]; };
					   */
					   tan_stayline_form temptemptemp = { 2, 1, stage_cursor, parentNum };
					   for (int i = 0; i <= 3; i++){
						   temptemptemp.floatValue[i] = floatValues[i];
					   }
					   for (int i = 0; i <= 5; i++){
						   temptemptemp.intValue[i] = intValues[i];
					   }
					   tan_stayline.push_back(temptemptemp);
					   //추가정보 입력
					   if (intValues[6] != 0){
						   while (intValues[6]-- > 0){
							   tan_additional_form temp_addi = { tan_stayline.size() - 1 , 1};
							   strean_scanf(stageFile, "%d/%f/%d/%f", &temp_addi.frame, &temp_addi.floatValue[0], &temp_addi.intValue[0], &temp_addi.floatValue[1]);
							   temp_addi.frame += stage_cursor;
							   tan_additional_info.push_back(temp_addi);
						   }
					   }
			
			}break;
				//웜 탄막
			case 2:{
					   *intValues = { 0, };
					   *floatValues = { 0.0f, };
					   strean_scanf(stageFile, "%f/%d/%d/%f/%d/%d/%d/%d", &floatValues[0], &intValues[0], &floatValues[1], &intValues[1], &intValues[2], &intValues[3], &intValues[4]);
//					   tantan.push_back(new tan_wram(this, floatValues[0], intValues[0], floatValues[1], intValues[1], intValues[2], intValues[3]));
					   tan_stayline_form temptemptemp = { 2, 2, stage_cursor, parentNum };
					   for (int i = 0; i < 2; i++){
						   temptemptemp.floatValue[i] = floatValues[i];
					   }
					   for (int i = 0; i < 4; i++){
						   temptemptemp.intValue[i] = intValues[i];
					   }
					   tan_stayline.push_back(temptemptemp);
					   while (intValues[0]-- > 0){
						   strean_scanf(stageFile, "%f,%f", &floatValues[0], &floatValues[1]);
//						   dynamic_cast<tan_wram *> (tantan.at(tantan.size() - 1))->add_point(floatValues[0], floatValues[1]);
						   tan_stayline_form temptemptemp_sub = { 0, 0, 0, 0 };
						   temptemptemp.floatValue[0] = floatValues[0];
						   temptemptemp.floatValue[1] = floatValues[1];
						   tan_stayline.push_back(temptemptemp);
					   }
					   //추가정보 입력
					   if (intValues[4] != 0){
						   while (intValues[4]-- > 0){
							   tan_additional_form temp_addi = { tan_stayline.size() - 1 , 2};
							   strean_scanf(stageFile, "%d/%f/%d/%f", &temp_addi.frame, &temp_addi.floatValue[0], &temp_addi.intValue[0], &temp_addi.floatValue[1]);
							   temp_addi.frame += stage_cursor;
							   tan_additional_info.push_back(temp_addi);
						   }
					   }
			}break;
				//커브 탄막
			case 3:{
					   *intValues = { 0, };
					   *floatValues = { 0.0f, };
					   strean_scanf(stageFile, "%d/%f/%d/%d/%d/%d", &intValues[0], &floatValues[0], &intValues[1], &intValues[2], &intValues[3], &intValues[4]);
					   tan_stayline_form temptemptemp = { 2, 3, stage_cursor, parentNum };
					   temptemptemp.floatValue[0] = floatValues[0];
					   for (int i = 0; i < 4; i++){
						   temptemptemp.intValue[i] = intValues[i];
					   }
					   tan_stayline.push_back(temptemptemp);
//					   tantan.push_back(new tan_curve(this, intValues[0], floatValues[0], intValues[1], intValues[2], intValues[3]));
					   while (intValues[0]-- > 0){
						   strean_scanf(stageFile, "%f,%f/%f/%d", &floatValues[0], &floatValues[1], &floatValues[2], &intValues[1]);
						   tan_stayline_form temptemptemp_sub = { 0, 0, 0, 0 };
						   for (int i = 0; i < 3; i++){
							   temptemptemp_sub.floatValue[i] = floatValues[i];
						   }
						   temptemptemp_sub.intValue[0] = intValues[1];
						   tan_stayline.push_back(temptemptemp_sub);
//						   dynamic_cast<tan_curve *> (tantan.at(tantan.size() - 1))->add_point(floatValues[0], floatValues[1], floatValues[2], (intValues[1] == 0) ? CURVE_CW : CURVE_CCW);
					   }
					   //추가정보 입력
					   if (intValues[4] != 0){
						   while (intValues[4]-- > 0){
							   tan_additional_form temp_addi = { tan_stayline.size() - 1 , 3};
							   strean_scanf(stageFile, "%d/%f/%d", &temp_addi.frame, &temp_addi.floatValue[0], &temp_addi.intValue[0]);
							   temp_addi.frame += stage_cursor;
							   tan_additional_info.push_back(temp_addi);
						   }
					   }
			}break;
			case 4:{
					   *intValues = { 0, };
					   *floatValues = { 0.0f, };
					   strean_scanf(stageFile, "%f,%f/%f,%f/%f,%f/%f/%d/%d/%d/%d/%d/%f/%d/%d", &floatValues[0], &floatValues[1], &floatValues[2], &floatValues[3], &floatValues[4], &floatValues[5], &floatValues[6], &intValues[0], &intValues[1], &intValues[2], &intValues[3], &intValues[4], &floatValues[7], &intValues[5], &intValues[6]);
					   tan_stayline_form temptemptemp = { 2, 4, stage_cursor, parentNum };
					   for (int i = 0; i < 8; i++){
						   temptemptemp.floatValue[i] = floatValues[i];
					   }
					   for (int i = 0; i < 6; i++){
						   temptemptemp.intValue[i] = intValues[i];
					   }
					   tan_stayline.push_back(temptemptemp);
//					   tantan.push_back(new tan_drawing(this, Point(floatValues[0], floatValues[1]), Point(floatValues[2], floatValues[3]), Point(floatValues[4], floatValues[5]), floatValues[6], intValues[0], intValues[1], intValues[2], intValues[3], intValues[4], floatValues[7], intValues[5]));
					   //추가정보 입력
					   if (intValues[6] != 0){
						   while (intValues[6]-- > 0){
							   tan_additional_form temp_addi = { tan_stayline.size() - 1, 1 };
							   strean_scanf(stageFile, "%d/%f/%d/%f", &temp_addi.frame, &temp_addi.floatValue[0], &temp_addi.intValue[0], &temp_addi.floatValue[1]);
							   temp_addi.frame += stage_cursor;
							   tan_additional_info.push_back(temp_addi);
						   }
					   }
			}break;
			case 5:{
					   *intValues = { 0, };
					   *floatValues = { 0.0f, };
					   strean_scanf(stageFile, "%f/%f/%f/%d/%d/%d/%d/%d", &floatValues[0], &floatValues[1], &floatValues[2], &intValues[0], &intValues[1], &intValues[2], &intValues[3], &intValues[4]);
					   tan_stayline_form temptemptemp = { 2, 5, stage_cursor, parentNum };
					   for (int i = 0; i <= 2; i++){
						   temptemptemp.floatValue[i] = floatValues[i];
					   }
					   for (int i = 0; i < 4; i++){
						   temptemptemp.intValue[i] = intValues[i];
					   }
					   tan_stayline.push_back(temptemptemp);
//					   tantan.push_back(new tan_hurricane(this, Point(floatValues[0], floatValues[1]), floatValues[2], floatValues[3], floatValues[4], intValues[0], intValues[1], intValues[2], intValues[3]));
					   //추가정보 입력
					   if (intValues[4] != 0){
						   while (intValues[4]-- > 0){
							   tan_additional_form temp_addi = { tan_stayline.size() - 1, 1 };
							   strean_scanf(stageFile, "%d/%f/%d/%f", &temp_addi.frame, &temp_addi.floatValue[0], &temp_addi.intValue[0], &temp_addi.floatValue[1]);
							   temp_addi.frame += stage_cursor;
							   tan_additional_info.push_back(temp_addi);
						   }
					   }
			}break;
			}
			//탄막에 부모자코 정보입력
//			tantan.at(tantan.size() - 1)->set_parent_tanmak(whoIsParent);
//			tantan.at(tantan.size() - 1)->set_tag(++total_tanmak);

			//부모 자코가 처음부터 죽어있다면 바로 삭제
//			if (whoIsParent != 0 && i == jakos.size())
//				tantan.erase(tantan.begin() + tantan.size() - 1);
		}
		//아이템
//		else if{
//			sub_player->add_more_subPlayer();
//			flag = false;
//		}
		/*		tantan.push_back(new tan_curve(this, 8.0f, 5, 0.03f, 2, 8, 5));
		dynamic_cast<tan_curve *> (tantan.at(0))->add_point(100.0f, 500.0f, 10.0f, CURVE_CW);
		dynamic_cast<tan_curve *> (tantan.at(0))->add_point(300.0f, 500.0f, 50.0f, CURVE_CCW);
		dynamic_cast<tan_curve *> (tantan.at(0))->add_point(600.0f, 500.0f, 80.0f, CURVE_CW);
		dynamic_cast<tan_curve *> (tantan.at(0))->add_point(700.0f, 500.0f, 100.0f, CURVE_CCW);
		dynamic_cast<tan_curve *> (tantan.at(0))->add_point(760.0f, 500.0f, NULL, NULL);
		*/
		if (flag){
			outCursor = strean_scanf_totalCounter;
			strean_scanf(stageFile, "%d", &stage_cursor);
			if (stage_cursor == 0)
				break;
		}
		else
			break;
	}
}

void mainRogic::end_buster_effect(float dt){
	//버스트 이펙트 끝내기
	player->setPosition(player->getPosition() + Point(0, 7.0f));
	if (this->getChildByName("endingBoost") == NULL){
		CCParticleSystem* particleSystem = CCParticleSystemQuad::create("particle/Booster.plist");

		particleSystem->setPosition(player->getPosition());
		particleSystem->setAutoRemoveOnFinish(true);
		this->addChild(particleSystem, -1, "endingBoost");
		particleSystem = NULL;
	}
	else{
		this->getChildByName("endingBoost")->setPosition(player->getPosition());
	}
}

void mainRogic::do_stage_function(){
	//스테이지 파일 읽기 메인
	if (stage_cursor == -1)
		strean_scanf(stageFile, "%d", &stage_cursor);
	//로그
//	char ll[100] = { 0, };
//	sprintf(ll, "%d", time_counter);
//	log(ll);
	//로그 끝
	while (time_counter == stage_cursor && time_counter != -1){
		strean_scanf(stageFile, "%d", &whatIsNext);
		//자코
		if (whatIsNext == 1){
			strean_scanf(stageFile, "%d", &whatMotion);
			switch (whatMotion){
			case 1:{
					   char ll[20] = { 0, };
					   sprintf(ll,"%d", stage_cursor);
//					   log(ll);
					   *intValues = { 0, };
					   *floatValues = { 0.0f, };
					   strean_scanf(stageFile, "%f,%f/%f,%f/%f/%d/%d", &floatValues[0], &floatValues[1], &floatValues[2], &floatValues[3], &floatValues[4], &intValues[0], &intValues[1]);
					   jakos.push_back(new jako_control(this, Point(floatValues[0], floatValues[1]), Point(floatValues[2], floatValues[3]), floatValues[4], player_type ,intValues[0]));
					   tanmak_read_func(++total_jako);
			}break;
			}
			jakos.at(jakos.size() - 1)->set_tag(total_jako);
		}
		else if (whatIsNext == 5 && !BossisRunning){
			BossisRunning = true;
			BossDamage = 1;
			stageFile = bossFile;
		}
		//탄막
		strean_scanf(stageFile, "%d", &stage_cursor);
	}
}

int mainRogic::search_jako(int tag_num){
	//자코 목록에서 자코 위치 찾기
	for (int i = 0; i < jakos.size(); i++){
		if (jakos.at(i)->get_tag() == tag_num){
			return i;
		}
	}
	return -1;
}

int mainRogic::search_tanmak(int tag_num){
	//탄막 목록에서 탄막 찾기
	for (int i = 0; i < tantan.size(); i++){
		if (tantan.at(i)->get_tag() == tag_num){
			return i;
		}
	}
	return -1;
}

void mainRogic::tanmak_pull_info(){
	//탄막 대기목록에서 게임으로 송출
	for (int i = 0; i<tan_stayline.size(); i++){
		if (tan_stayline.at(i).startTime == time_counter){
			tan_stayline_form temp_info = tan_stayline.at(i);
			int target_point, temp_size = tantan.size();
			if (temp_info.sub_info > 90000){ target_point = temp_info.sub_info; }
			else{ target_point = search_jako(temp_info.sub_info); }
			if (target_point != -1){
				switch (temp_info.tantype){
				case 1:{
						   if (target_point >= 90000){
							   if (BossIslive)
								   tantan.push_back(new tan_001(this, boss->getSprite()->getPositionX(), boss->getSprite()->getPositionY(), temp_info.intValue[0], temp_info.floatValue[0], temp_info.intValue[1], temp_info.floatValue[1], temp_info.floatValue[2], temp_info.floatValue[3], temp_info.intValue[2], temp_info.intValue[3], temp_info.intValue[4]));
						   }
						   else
							   tantan.push_back(new tan_001(this, jakos.at(target_point)->getSprite()->getPositionX(), jakos.at(target_point)->getSprite()->getPositionY(), temp_info.intValue[0], temp_info.floatValue[0], temp_info.intValue[1], temp_info.floatValue[1], temp_info.floatValue[2], temp_info.floatValue[3], temp_info.intValue[2], temp_info.intValue[3], temp_info.intValue[4]));
						   if (tantan.size() > temp_size)
							   dynamic_cast<tan_001*>(tantan.at(tantan.size() - 1))->setBounds(temp_info.intValue[5]);
				}break;
				case 2:{
						   if (target_point >= 90000){
							   if (BossIslive){
								   tantan.push_back(new tan_wram(this, temp_info.floatValue[0], temp_info.intValue[0], temp_info.floatValue[1], temp_info.intValue[1], temp_info.intValue[2], temp_info.intValue[3]));
								   i++;
								   ((tan_wram*) tantan.at(tantan.size() - 1))->add_point(boss->getSprite()->getPositionX(), boss->getSprite()->getPositionY());
							   }
						   }
						   else{
							   tantan.push_back(new tan_wram(this, temp_info.floatValue[0], temp_info.intValue[0], temp_info.floatValue[1], temp_info.intValue[1], temp_info.intValue[2], temp_info.intValue[3]));
							   i++;
							   ((tan_wram*) tantan.at(tantan.size() - 1))->add_point(jakos.at(target_point)->getSprite()->getPositionX(), jakos.at(target_point)->getSprite()->getPositionY());
						   }
						   if (tantan.size() > temp_size){
							   while (temp_info.intValue[0]-- > 0){
								   ((tan_wram*) tantan.at(tantan.size() - 1))->add_point(tan_stayline.at(i).floatValue[0], tan_stayline.at(i).floatValue[1]);
								   tan_stayline.erase(tan_stayline.begin() + i);
							   }
							   i--;
						   }
				}break;
				case 3:{
						   if (target_point >= 90000){
							   if (BossIslive){
								   tantan.push_back(new tan_curve(this, temp_info.intValue[0], temp_info.floatValue[0], temp_info.intValue[1], temp_info.intValue[2], temp_info.intValue[3]));
								   i++;
								   ((tan_curve*) tantan.at(tantan.size() - 1))->add_point(boss->getSprite()->getPositionX(), boss->getSprite()->getPositionY(), tan_stayline.at(i).floatValue[2], tan_stayline.at(i).intValue[0]);
							   }
						   }
						   else{
							   tantan.push_back(new tan_curve(this, temp_info.intValue[0], temp_info.floatValue[0], temp_info.intValue[1], temp_info.intValue[2], temp_info.intValue[3]));
							   i++;
							   ((tan_curve*) tantan.at(tantan.size() - 1))->add_point(jakos.at(target_point)->getSprite()->getPositionX(), jakos.at(target_point)->getSprite()->getPositionY(), tan_stayline.at(i).floatValue[2], tan_stayline.at(i).intValue[0]);
						   }
						   if (tantan.size() > temp_size){
							   while (temp_info.intValue[0]-- > 0){
								   ((tan_curve*) tantan.at(tantan.size() - 1))->add_point(tan_stayline.at(i).floatValue[0], tan_stayline.at(i).floatValue[1], tan_stayline.at(i).floatValue[2], tan_stayline.at(i).intValue[0]);
								   tan_stayline.erase(tan_stayline.begin() + i);
							   }
							   i--;
						   }
				}break;
				case 4:{
						   tantan.push_back(new tan_drawing(this, Point(temp_info.floatValue[0], temp_info.floatValue[1]), Point(temp_info.floatValue[2], temp_info.floatValue[3]), Point(temp_info.floatValue[4], temp_info.floatValue[5]), temp_info.floatValue[6], temp_info.intValue[0], temp_info.intValue[1], temp_info.intValue[2], temp_info.intValue[3], temp_info.intValue[4], temp_info.floatValue[7], temp_info.intValue[5]));
				}break;
				case 5:{
						   if (target_point >= 90000){
							   if (BossIslive)
								   tantan.push_back(new tan_hurricane(this, boss->getSprite()->getPosition(), temp_info.floatValue[0], temp_info.floatValue[1], temp_info.floatValue[2], temp_info.intValue[0], temp_info.intValue[1], temp_info.intValue[2], temp_info.intValue[3]));
						   }
						   else
								tantan.push_back(new tan_hurricane(this, jakos.at(target_point)->getSprite()->getPosition(), temp_info.floatValue[0], temp_info.floatValue[1], temp_info.floatValue[2], temp_info.intValue[0], temp_info.intValue[1], temp_info.intValue[2], temp_info.intValue[3]));
				}break;
				}
				if (tantan.size() > temp_size){
					tantan.at(tantan.size() - 1)->set_tag(++total_tanmak);
					tantan.at(tantan.size() - 1)->set_parent_tanmak(temp_info.sub_info);
					if (target_point > 90000)
						boss->add_child_tanmak(total_tanmak);
					else
						jakos.at(target_point)->add_child_tanmak(total_tanmak);

					for (int j = 0; j < tan_additional_info.size(); j++){
						if (tan_additional_info.at(j).stay_target == i && tan_additional_info.at(j).taget == 0){
							tan_additional_info.at(j).taget = total_tanmak;
						}
						else if (tan_additional_info.at(j).stay_target > i){
							tan_additional_info.at(j).stay_target -= 1;
						}
					}
				}
			}
//			delete(&tan_stayline.at(i));
			tan_stayline.erase(tan_stayline.begin() + i);
			i--;
		}
	}
}

void mainRogic::acc_fever(){
	//피버 게이지 추가
	current_fever_value++;
	upupdowndown = 0;
	if (current_fever_value > 48){
		current_fever_value = 48;
		return;
	}
	char temp_str[25] = { 0, }, particleName[30] = { 0, };
	if (current_fever_value == 48){
		if (player_type == 1){
			sprintf(temp_str, "fever_%d.png", 3);
		}
		else if (player_type == 2){
			sprintf(temp_str, "fever_%d.png", 1);
		}
		else{
			sprintf(temp_str, "fever_%d.png", 2);
		}
		((Sprite*)this->getChildByName("uiSprite")->getChildByName("fever"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_str));
		
		if (this->getChildByName("fever_particle") == NULL){
			char particleName[30] = { 0, };
			if (player_type == 1){
				sprintf(particleName, "particle/vsuri_fever.plist");
			}
			else if (player_type == 2){
				sprintf(particleName, "particle/bal_fever.plist");
			}
			else{
				sprintf(particleName, "particle/never_fever.plist");
			}
			CCParticleSystem* particleSystem = CCParticleSystemQuad::create(particleName);

			particleSystem->setPosition(((Sprite*)this->getChildByName("uiSprite")->getChildByName("fever"))->getPosition());

			particleSystem->setGravity(Vec2((rand() % 1) * ((rand() % 2 == 0) ? -1 : 1), (rand() % 1) * ((rand() % 2 == 0) ? -1 : 1)));

			particleSystem->setScale(0.7f);

			particleSystem->setAutoRemoveOnFinish(true);

			this->addChild(particleSystem, -2, "fever_particle");

			particleSystem = NULL;
		}
	}
	else{
		sprintf(temp_str, "fever%d.png", current_fever_value / 3);
		((Sprite*)this->getChildByName("uiSprite")->getChildByName("fever"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_str));
	}
}

void mainRogic::dcc_fever(){
	//피버 게이지 감소
	current_fever_value--;
	upupdowndown = 0;
	if (current_fever_value < 0){
		current_fever_value = 0;
		return;
	}
	char temp_str[25] = { 0, };
	sprintf(temp_str, "fever%d.png", current_fever_value/3);
	((Sprite*)this->getChildByName("uiSprite")->getChildByName("fever"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(temp_str));
}

void mainRogic::clear_jakos_prev_shoted_number(int value){
	//자코가 죽을때 마지막으로 맞은 플레이어탄의 번호를 지우기
	for (int j = jakos.size(); j > 0; j--){
		if(jakos.at(j - 1)->get_prev_shot_tan_number() == value)
			jakos.at(j - 1)->clear_shoted_number();
	}
}

void mainRogic::update(float dt){
	//스코어 연출 시간 단축
	if (current_score_value - viewing_score_value > 1200){
		viewing_score_value = current_score_value - 1200;
	}
	refresh_score();
	if (skiiiiiiiiping)
		return;
	time_counter++;
	char ll[50] = { 0, };
	sprintf(ll, "%d", time_counter);
	log(ll);
//	if (!playerStatFlag){
//		left_clicked = false;
//		return;
//	}
	//탄막 생성
	//자코 탄막
	bool isAttack = false;
	for (int i = 0; i < tantan.size(); i++){
		if (tantan.at(i)->get_state() >= 2){
			if (tantan.at(i)->get_prevMarkedTime() == tantan.at(i)->get_density()){
				if (tantan.at(i)->get_parent_tag() == 90001){
					tantan.at(i)->set_center(boss->getSprite()->getPosition() + Point(-100.0f, -100.0f));
				}
				else if (tantan.at(i)->get_parent_tag() == 90002){
					tantan.at(i)->set_center(boss->getSprite()->getPosition() + Point(100.0f, -100.0f));
				}
				else
					tantan.at(i)->set_center(jakos.at(search_jako(tantan.at(i)->get_parent_tag()))->getSprite()->getPosition());
				tantan.at(i)->make();
			}
			else
				tantan.at(i)->add_prevMarkedTime();
		}
	}
	//플레이어 탄막
	if (isPlayerLive){
		if (player_tan->get_make_density() == player_tan->get_make_prevMarkedTime())
			player_tan->make();
		else{
			player_tan->add_make_prevMarkedTime();
		}
		//탄막 생성 끝
		if (player_type == 2){
			if (sub_player->get_make_density() == sub_player->get_make_prevMarkedTime())
				sub_player->make();
			else{
				sub_player->add_make_prevMarkedTime();
			}
		}
		if (player_type == 3){
			if (neverb_sub_player->get_make_density() == neverb_sub_player->get_make_prevMarkedTime())
				neverb_sub_player->make();
			else{
				neverb_sub_player->add_make_prevMarkedTime();
			}
		}
	}
	if (BossIslive){
		boss->move();
	}

	do_stage_function();
	set_additional_info();
	tanmak_pull_info();
	update_current_running_time((strean_scanf_totalCounter <= fileSize) ? strean_scanf_totalCounter:fileSize);
	end_of_combo();
	update_background();
	if (BossIslive){
		//보스 전용 피격파트
		for (int i = 0; isPlayerLive && BossIslive  && i < player_tan->get_playerTanVector().size(); i++){
			if (boss->getSprite()->getBoundingBox().containsPoint(player_tan->get_playerTanVector().at(i)->getPosition()) && boss->get_prev_shot_tan_number() != i && boss->get_state() != 0){
				if (player_tan->get_playerTanVector().at(i)->isVisible() == true){
					boss->add_damage(i);
					isAttack = true;
					player_tan->get_playerTanVector().at(i)->setVisible(false);
					combo_counter = 0;
					upupdowndown = 0;
				}
			}
		}

		if (player_type == 2 && isPlayerLive){
			for (int k = 0; k < 6; k++){
				if (k % 3 > sub_player->get_line())
					continue;
				for (int i = 0; i < sub_player->get_subTanVector(k).size(); i++){
					if (boss->get_state() != 0 && boss->getSprite()->getBoundingBox().containsPoint(sub_player->get_subTanVector(k).at(i)->getPosition()) && boss->get_prev_shot_sub_tan_number() != (k) * sub_player->get_value() + i){
						if (sub_player->get_subTanVector(k).at(i)->isVisible() == true){
							boss->add_sub_damage((k) * sub_player->get_value() + i);
							sub_player->get_subTanVector(k).at(i)->setVisible(false);
							isAttack = true;
							combo_counter = 0;
							upupdowndown = 0;
						}
					}
				}
			}
		}

		if (player_type == 3 && isPlayerLive){
			for (int i = 0; i < neverb_sub_player->get_playerTanVector().size(); i++){
				if (boss->get_state() != 0 && boss->getSprite()->getBoundingBox().containsPoint(neverb_sub_player->get_playerTanVector().at(i)->getPosition() - Point(0,100.0f)) && boss->get_prev_shot_sub_tan_number() != i){
					if (neverb_sub_player->get_playerTanVector().at(i)->isVisible() == true){
						boss->add_sub_damage(i);
						neverb_sub_player->get_playerTanVector().at(i)->setVisible(false);
						isAttack = true;
						combo_counter = 0;
						upupdowndown = 0;
					}
				}
			}
		}
	}
	//자코 전용 피격 시작
	for (int i = 0; isPlayerLive && i < player_tan->get_playerTanVector().size(); i++){
		for (int j = 0; j < jakos.size(); j++){
			if (player_tan->get_playerTanVector().at(i)->getBoundingBox().intersectsRect(jakos.at(j)->getSprite()->getBoundingBox()) && jakos.at(j)->get_prev_shot_tan_number() != i && jakos.at(j)->get_state() != 0){
				if (player_tan->get_playerTanVector().at(i)->isVisible() == true){
					jakos.at(j)->add_damage(i);
					isAttack = true;
					player_tan->get_playerTanVector().at(i)->setVisible(false);
					combo_counter = 0;
					upupdowndown = 0;
				}
			}
		}
	}
	
	if (player_type == 2 && isPlayerLive){
		for (int k = 0; k < 6; k++){
			if (k % 3 > sub_player->get_line())
				continue;
			for (int i = 0; i < sub_player->get_subTanVector(k).size(); i++){
				for (int j = 0; j < jakos.size(); j++){
					if (jakos.at(j)->get_state() != 0 && sub_player->get_subTanVector(k).at(i)->getBoundingBox().intersectsRect(jakos.at(j)->getSprite()->getBoundingBox()) && jakos.at(j)->get_prev_shot_sub_tan_number() != (k) * sub_player->get_value() + i){
						if (sub_player->get_subTanVector(k).at(i)->isVisible() == true){
							jakos.at(j)->add_sub_damage((k) * sub_player->get_value() + i);
							sub_player->get_subTanVector(k).at(i)->setVisible(false);
							isAttack = true;
							combo_counter = 0;
							upupdowndown = 0;
						}
					}
				}
			}
		}
	}

	if (player_type == 3 && isPlayerLive){
		for (int i = 0; i < neverb_sub_player->get_playerTanVector().size(); i++){
			for (int j = 0; j < jakos.size(); j++){
				if (jakos.at(j)->get_state() != 0 && neverb_sub_player->get_playerTanVector().at(i)->getBoundingBox().intersectsRect(jakos.at(j)->getSprite()->getBoundingBox()) && jakos.at(j)->get_prev_shot_sub_tan_number() !=  i){
					if (neverb_sub_player->get_playerTanVector().at(i)->isVisible() == true){
						jakos.at(j)->add_sub_damage(i);
						neverb_sub_player->get_playerTanVector().at(i)->setVisible(false);
						isAttack = true;
						combo_counter = 0;
						upupdowndown = 0;
					}
				}
			}
		}
	}
	//자코 전용 피격 끝
	if (isAttack){
		
	}
	else
		upupdowndown--;
	if (upupdowndown <= -50){
		dcc_fever();
	}

	//탄막 이동
	//플레이어 탄막
	if (isPlayerLive){
		if (player_tan->get_density() == player_tan->get_prevMarkedTime())
			player_tan->move();
		else{
			player_tan->add_prevMarkedTime();
		}
		//
		if (player_type == 2){
			if (sub_player->get_density() == sub_player->get_prevMarkedTime())
				sub_player->move();
			else{
				sub_player->add_prevMarkedTime();
			}
		}
		if (player_type == 3){
			if (neverb_sub_player->get_density() == neverb_sub_player->get_prevMarkedTime())
				neverb_sub_player->move();
			else{
				neverb_sub_player->add_prevMarkedTime();
			}
		}
	}

	//자코 탄막
	for (int i = 0; i < tantan.size(); i++){
		tantan.at(i)->move();
		if ((tantan.at(i))->get_state() == 0){
			delete(*(tantan.begin() + i));
			tantan.erase(tantan.begin() + i);
			i--;
		}
	}
	//자코 이동
	for (int i = 0; i < jakos.size(); i++){
		if (jakos.at(i)->get_state()>1)
			jakos.at(i)->move();
		if (jakos.at(i)->get_state() == 0){
			delete(*(jakos.begin() + i));
			jakos.erase(jakos.begin() + i);
			i--;
		}
	}
	//보스 타임 시작
	if (stageFile == bossFile && tantan.size() == 0 && !isGameEnd && BossisRunning && !BossIslive&& jakos.size() == 0){
		/*
		isGameEnd = true;
		this->runAction(Sequence::create(CCDelayTime::create(2.0f), CallFunc::create(CC_CALLBACK_0(mainRogic::show_result_window, this, 1)), NULL));
		*/
		
		BossDamage = 1;
		BossIslive = true;
		boss = new boss_control(this, 4.0f, player_type);
		time_counter = 0;
		strean_scanf_totalCounter = 0;
		tan_stayline.clear();
		tantan.clear();
		tanmak_read_func(90001);
		strean_scanf(stageFile, "%d", &stage_cursor);
		strean_scanf(stageFile, "%d", &stage_cursor);
		tanmak_read_func(90002);
		bossTriger();
	}
	//보스 탄막 반복
	if (tan_stayline.size() == 0 && BossisRunning && BossIslive){
		if (boss->get_state() == 2){
			stageFile = bossfinalFile;
		}
		strean_scanf_totalCounter = 0;
		time_counter = 0;
		tanmak_read_func(90001);
		strean_scanf(stageFile, "%d", &stage_cursor);
		strean_scanf(stageFile, "%d", &stage_cursor);
		tanmak_read_func(90002);
	}
	if (jakos.size() > 0 && BossisRunning){
		for (int i = 0; i < jakos.size(); i++){
			Point pos = jakos.at(i)->getSprite()->getPosition();
			if (pos.x < 0 || pos.x > 768 || pos.y <0 || pos.y > 1024){
				delete(*(jakos.begin() + i));
				jakos.erase(jakos.begin() + i);
				i--;
			}
		}
	}
	if (fileSize <= strean_scanf_totalCounter && jakos.size() == 0 && tantan.size() == 0 && stage_type == 2 && !isGameEnd){
		isGameEnd = true;
		show_result_window(2);
	}
}

void mainRogic::jako_stop_tanmak(std::vector<int> *tempTanmak){
	//자코가 죽을때 자신이 발사하던 탄막을 모두 해제
	for (int i = 0, j = 0; i < tempTanmak->size(); i++){
		j = search_tanmak(tempTanmak->at(i));
		if (j != -1){
			tantan.at(j)->set_stat(1);
			for (int i = 0; i < tan_additional_info.size(); i++){
				if (tan_additional_info.at(i).taget == tantan.at(j)->get_tag()){
					tan_additional_info.erase(tan_additional_info.begin() + i);
					i--;
				}
			}
		}
	}	
}

int mainRogic::get_time_counter(){
	return time_counter;
}

Sprite* mainRogic::get_player(){
	return player;
}

std::vector<jako_control*>* mainRogic::get_jakos(){
	return &jakos;
}

void mainRogic::set_additional_info(){
	//탄막추가 옵션 입력
	for (int i = 0; i < tan_additional_info.size(); i++){
		if (tan_additional_info.at(i).taget > 0 && tan_additional_info.at(i).frame == time_counter){
			tan_additional_form temp_addi = tan_additional_info.at(i);
			if (tantan.at(search_tanmak(temp_addi.taget))->get_state() > 1){
				switch (temp_addi.type){
				case 1:{
						   dynamic_cast<tan_001*>(tantan.at(search_tanmak(temp_addi.taget)))->set_additional_info(temp_addi.floatValue[0], temp_addi.intValue[0], temp_addi.floatValue[1]);
				}break;
				case 2:{

				}break;
				case 3:{

				}break;
				case 4:{

				}break;
				case 5:{

				}break;
				}
			}
			tan_additional_info.erase(tan_additional_info.begin() + i);
			i--;
		}
	}
}

void mainRogic::strean_scanf(unsigned char *_str, const char *format, ...){
	//스테이지 파일 scanf함수
	va_list list;
	va_start(list, format);
	if (stageFile == bossFile && bossfileSize < strean_scanf_totalCounter){
		*va_arg(list, int *) = 0;
		return;
	}
	else if (stageFile == bossfinalFile && bossfinalfileSize < strean_scanf_totalCounter){
		*va_arg(list, int *) = 0;
		return;
	}
//	else if (fileSize < strean_scanf_totalCounter){
//		*va_arg(list, int *) = 0;
//		return;
//	}
	insideCounter = 0, counter = 0, return_value_counter = 0;
	while (1){
		if (format[counter] == '%'){
			counter++;
			switch (format[counter])
			{
			case 'd':{
						 temp_int = 0;
						 isUnder = false;
						 while (_str[strean_scanf_totalCounter + insideCounter] >= '0' &&
							 _str[strean_scanf_totalCounter + insideCounter] <= '9'
							 || _str[strean_scanf_totalCounter + insideCounter] == '-'){
							 if (_str[strean_scanf_totalCounter + insideCounter] == '-'){
								 insideCounter++;
								 isUnder = true;
								 continue;
							 }
							 if (temp_int>0)
								 temp_int *= 10;
							 temp_int += (_str[strean_scanf_totalCounter + insideCounter] - '0');
							 insideCounter++;
						 }
						 temp_int *= (isUnder) ? -1 : 1;
						 *va_arg(list, int *) = temp_int;
			}break;
			case 'f':{
						temp_float = 0.0f;
						underCounter = 1.0f;
						isover = false, isUnder = false;
						while (_str[strean_scanf_totalCounter + insideCounter] >= '0' &&
							 _str[strean_scanf_totalCounter + insideCounter] <= '9' ||
							 _str[strean_scanf_totalCounter + insideCounter] == '.' ||
							 _str[strean_scanf_totalCounter + insideCounter] == '-'){
							 if (_str[strean_scanf_totalCounter + insideCounter] == '-'){
								 insideCounter++;
								 isUnder = true;
								 continue;
							 }
							 if (_str[strean_scanf_totalCounter + insideCounter] == '.'){
								 isover = true;
								 insideCounter++;
								 continue;
							 }
							 if (isover)
								 underCounter /= 10;
							 if (temp_float>0)
								 temp_float *= 10;
							 temp_float += (_str[strean_scanf_totalCounter + insideCounter++] - '0');
						 }
						 temp_float *= (isUnder) ? -1 : 1;
						 *va_arg(list, float *) = temp_float*underCounter;
			}break;
			default:
				break;
			}
		}
		else{}
		if (format[counter + 1] == 0)
			break;
		else{
			counter += 2;
			insideCounter++;
		}
	}
	va_end(list);
	strean_scanf_totalCounter += insideCounter + 2;
}
