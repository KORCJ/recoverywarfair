/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef __mainRogic_H__
#define __mainRogic_H__

#include <fstream>
#include <stdarg.h>
#include "jako_control.h"
#include "cocos2d.h"
#include "LocalPreferenceManager.h"
#include "boss_control.h"


class player_tan_control;

class sub_player_control;

class neverb_tan_control;

class mainRogic : public cocos2d::LayerColor
{
private:
	void end_bossTriger();

	void bossTriger();

	int stage_type;

	void update(float dt);

	bool playerStatFlag;

	int stage_cursor, total_jako, total_tanmak, strean_scanf_totalCounter;

	int current_running_time, current_fever_value, current_combo_value;

	unsigned long long current_score_value, viewing_score_value;

	struct Pair_int{ int first, second; };

	struct Pair_float{ float first, second; };

	struct tan_stayline_form{ int typeflag, tantype, startTime, sub_info, intValue[9]; float floatValue[9]; };

	struct tan_additional_form{ unsigned int stay_target; int type, frame, taget, intValue[4]; float floatValue[4]; };

	std::vector<tan_stayline_form> tan_stayline;

	std::vector<tan_additional_form> tan_additional_info;

	Pair_float prev_mouse_pos;

	cocos2d::Sprite *player;

	cocos2d::Sprite *playerBound;

	bool left_clicked = false;

	int save_time_s;

	short save_time_ss;

	bool isGoin;

	ssize_t fileSize, bossfileSize, bossfinalfileSize;

	unsigned char *stageFile, *bossFile, *bossfinalFile;

	int whatIsNext, whatTanmak, whoIsParent, whatMotion, temp_int;

	int insideCounter = 0, counter = 0, return_value_counter = 0;

	float temp_float = 0, underCounter = 1.0f;

	bool isover = false, isUnder = false;

	int intValues[8];

	float floatValues[8];

	int time_counter;

	cocos2d::SpriteFrameCache *uiCache;

	cocos2d::Sprite *uiSprite, *feverSprite, *recoveringSprite;

	virtual bool onTouchBegined(cocos2d::Touch *touch, cocos2d::Event *event);
	virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);
	virtual void onTouchEnd(cocos2d::Touch *touch, cocos2d::Event *event);

	void tanmak_read_func(int parentNum);

	void tanmak_pull_info();

	void update_current_running_time(int current_value);

	int upupdowndown;

	void dcc_fever();

	int search_jako(int tag_num);

	int search_tanmak(int tag_num);

	int combo_counter = 0;

	void end_of_combo();

	void update_background();

	void bombTriger(cocos2d::Object *pSender);

	void bombTriger_2nd();

	void addDamageFromBomb(float delta);

	int life, bomb;

	void replace_player(float dt);

	void end_OPmode(float dt);

	void end_buster_effect(float dt);

	bool skiiiiiiiiping;

	void result_continue(cocos2d::Object *pSender);

	void result_exit(cocos2d::Object *pSender);

	void call_main_scene(float dt);

	void pause_func(cocos2d::Object *pSender);

	void pause_to_resume_func(cocos2d::Object *pSender);

	bool nowPauseing;

	LocalPrefernceData pref_data;

	void set_additional_info();
public:
	void show_result_window(int type);

	int BossDamage;

	bool BossIslive = false;

	bool BossisRunning;

	int deadFromBoss = 0;

	boss_control *boss;

	bool isGameEnd;

	int player_type;

	player_tan_control *player_tan;

	sub_player_control *sub_player;

	neverb_tan_control *neverb_sub_player;
	// button func
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event);

	int jako_hps[50];

	bool isPlayerLive, isPlayerOP;

	void acc_fever();

	void refresh_score();

	void add_score(int jako_num);

	void add_combo();

	int get_time_counter();

	void set_playerStatFlag(bool stat);

	bool get_playerStat();

	void playerDeadTriger();

	cocos2d::Point playerPoint;

	cocos2d::Rect playerBoundingBox;
	
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene(int player_type, int stage_type);

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // implement the "static create()" method manually
	CREATE_FUNC(mainRogic);

	void mov_player(cocos2d::Event *event);

	void update_save_time(float dt);

	void test_tan_01(float dt);

	void switch_mouse_stat(cocos2d::Event *event);

	void doEnd();

	void do_stage_function();

	void clear_jakos_prev_shoted_number(int value);

	void jako_stop_tanmak(std::vector<int> *tempTanmak);

	void strean_scanf(unsigned char *_str, const char *format, ...);

	cocos2d::Sprite* get_player();

	std::vector<jako_control*>* get_jakos();
};

#endif // __HELLOWORLD_SCENE_H__
