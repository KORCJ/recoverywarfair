/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef _boss_control_h_
#define _boss_control_h_
#include <cocos2d.h>

class mainRogic;

class boss_control{
private:
	int state, gaved_damage, jako_type, prev_shoted_tan, prev_shoted_sub_tan, tag;
	int grade_var, fat_var, hp, flag, dt_density, density, frame_var, redraw_img_density, dt_redraw_img_density;
	float speed, gap_between, oval_radial, rotate_angle_acc, rotate_angle, ovalRate;
	cocos2d::Sprite *boss_sprite;
	cocos2d::Point	center;
	std::vector<int> child_tanmak;
	mainRogic *forRun;
	char png_name[25];
	float radian;
	bool wasInside;
	cocos2d::Rect BoundRect;
public:
	cocos2d::Sprite *getSprite();

	void move();

	boss_control(mainRogic *forRun, float speed, short jako_type);

	~boss_control();

	int get_state();

	void set_state(int state);

	void add_damage(int shot_tan_number);

	void add_sub_damage(int shot_tan_number);

	void bomb_damage(int delta);

	void clear_shoted_number();

	int get_prev_shot_tan_number();

	int get_prev_shot_sub_tan_number();

	void add_child_tanmak(int tag);

	void delete_child_tanmak(int *tag);

	void launch_particle(int type);

	void get_BounundRect();
};

#endif