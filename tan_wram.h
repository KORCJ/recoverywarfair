/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "cocos2d.h"
#include "enemyTan_wram.h"
#include "mainRogic.h"
#include "tan_basic.h"

class tan_wram : public tan_basic{
private:
	std::vector<cocos2d::Point> center;
	mainRogic *Run;
	float rotate_angle_acc, speed, rotate_angle;
	std::vector<enemyTan_wram*> tanmak;
	int tanType, total_point, state, value, density, tag, parentTag;
public:
	~tan_wram();

	int prevMakedTime;

	void add_prevMarkedTime();

	int get_prevMarkedTime();

	tan_wram(mainRogic *forRun, float n_speed, int total_point, float rotate_angle_acc, int tanType, int value, int density);

	void make();

	void move();

	int get_state();

	int get_density();

	void add_point(float x, float y);

	void set_stat(int stat);

	void set_parent_tanmak(int tag);

	void set_tag(int tag);

	int get_tag();

	void set_center(cocos2d::Point pos);

	int get_parent_tag();

	void set_additional_info(float speed);

	void removecurrentTan();
};