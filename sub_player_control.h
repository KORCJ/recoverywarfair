/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef _sub_player_control_
#define _sub_player_control_

#include "cocos2d.h"
#include "jako_control.h"

class mainRogic;

class sub_player_control{
private:
	cocos2d::Sprite *sub_player[6];
	cocos2d::Texture2D *tanmakTexture;
	mainRogic *forRun;
	char tan_png[15];
	float speed , prevMakedTime, density, make_prevMakedTime, make_density, total_size;
	std::vector<cocos2d::Sprite*> sub_player_tan[6];
	std::vector<float> sub_player_tan_angle[6];
	void set_angle_func(int i, int k);
	int line;
	int value;
public:
	~sub_player_control();

	void add_more_subPlayer();

	sub_player_control(mainRogic *forRun);

	/*index is 0~3 number*/
	std::vector<cocos2d::Sprite*> get_subTanVector(int index);

	void add_prevMarkedTime();

	int get_prevMarkedTime();

	int get_density();

	void add_make_prevMarkedTime();

	int get_make_prevMarkedTime();

	int get_make_density();

	void move();

	void make();

	int get_line();

	int get_value();
};
#endif