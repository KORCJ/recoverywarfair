/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef __HELLOWORLD_tan_BASIC__
#define __HELLOWORLD_tan_BASIC__

#include"cocos2d.h"
class tan_basic{
public:
	virtual ~tan_basic();

	virtual void make();

	virtual void move();

	virtual int get_state();

	virtual int get_density();

	virtual void set_stat(int stat);

	virtual void add_prevMarkedTime();

	virtual int get_prevMarkedTime();

	virtual void set_parent_tanmak(int tag);

	virtual void set_tag(int tag);

	virtual int get_tag();

	virtual void set_center(cocos2d::Point pos);

	virtual int get_parent_tag();

	virtual void removecurrentTan();
};
#endif