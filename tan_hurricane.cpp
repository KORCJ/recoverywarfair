/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "tan_hurricane.h"

USING_NS_CC;

tan_hurricane::tan_hurricane(mainRogic *forRun, cocos2d::Point center, float speed, float rotate_angle_acc, float max_radius, int tanType, int rounded_value, int value, int density){
	tan_hurricane::Run = forRun;
	tan_hurricane::tanType = tanType;
	tan_hurricane::rotate_angle_acc = rotate_angle_acc;
	tan_hurricane::radius = 10.0f;
	tan_hurricane::max_radius = max_radius;
	tan_hurricane::state = 3;
	tan_hurricane::value = value;
	tan_hurricane::density = density;
	tan_hurricane::center = center;
	tan_hurricane::player = Run->playerPoint;
	tan_hurricane::prevMakedTime = 0;
	tan_hurricane::rounded_value = rounded_value;
	tan_hurricane::speed = speed;
	tan_hurricane::rotate_density = M_PI*2 / (float)rounded_value;
	tan_hurricane::angle = 0.0f;
	tan_hurricane::radius_density = max_radius / (float) value;
	tan_hurricane::total_counter = 0;
	if (center.y > forRun->playerPoint.y){
		if (center.x < forRun->playerPoint.x)
			tan_hurricane::target_angle = atan2f(abs(center.x - forRun->playerPoint.x), abs(center.y - forRun->playerPoint.y)) + 1.5f * M_PI;
		else{
			tan_hurricane::target_angle = atan2f(abs(center.y - forRun->playerPoint.y), abs(center.x - forRun->playerPoint.x));
			tan_hurricane::target_angle += 1.0f * M_PI;
		}
	}
	else{
		if (center.x < forRun->playerPoint.x)
			tan_hurricane::target_angle = atan2f(abs(center.x - forRun->playerPoint.x), abs(center.y - forRun->playerPoint.y));
		else{
			tan_hurricane::target_angle = atan2f(abs(center.y - forRun->playerPoint.y), abs(center.x - forRun->playerPoint.x)) + 0.5f * M_PI;
		}
	}
}

tan_hurricane::~tan_hurricane(){
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
		delete tanmak.at(i);
		tanmak.erase(tanmak.begin() + i--);
	}
}

void tan_hurricane::set_additional_info(float speed, int angle, float angle_acc){
	if (speed != -1){
		//		this->speed = speed;
		for (enemyTan *temp : tanmak){
			temp->setSpeed(speed);
		}
	}
	if (angle != -1.0f){
		for (enemyTan *temp : tanmak){
			temp->setAngle(temp->getAngle() + angle / 360.0f);
		}
	}
	if (angle_acc != -1.0f){
		for (enemyTan *temp : tanmak){
			temp->setAngle_acc(angle_acc);
		}
	}
}

void tan_hurricane::make(){
	if (center.x > Run->playerPoint.x){
		center.x -= 0.3f;
		temp_target_angle = atan2f(abs(center.y - Run->playerPoint.y), abs(center.x - Run->playerPoint.x)) + 1.0f * M_PI;
		target_angle -= 0.01f;
		if (target_angle < temp_target_angle)
			target_angle += 0.01f;
	}
	else{
		center.x += 0.3f;
		temp_target_angle = atan2f(abs(center.x - Run->playerPoint.x), abs(center.y - Run->playerPoint.y)) + 1.5f * M_PI;
		target_angle += 0.01f;
		if (target_angle > temp_target_angle)
			target_angle -= 0.01f;
	}
	tan_hurricane::rotate_angle = 0.0f;
	for (int i = 0; i < rounded_value; i++){
		temp_x = radius * cosf(rotate_angle) * cosf(angle) - radius * sinf(rotate_angle) * sinf(angle);
		temp_y = radius * cosf(rotate_angle) * sinf(angle) + radius * sinf(rotate_angle) * cosf(angle);
		temp_x += center.x;
		temp_y += center.y;
		tanmak.push_back(new enemyTan(tanType, temp_x, temp_y, speed, target_angle, 0.0f, 0.0f));
		tanmak.at(tanmak.size() - 1)->setAngle((target_angle * 180 / M_PI) / 360.0f);
		rotate_angle += rotate_density;
		Run->addChild(tanmak.at(tanmak.size() - 1)->getSprite());
	}
	angle += rotate_angle_acc;
	radius += radius_density;
	total_counter++;
	prevMakedTime = 0;
	state = 2;
	if (total_counter == value)
		radius_density *= -1;

	if (total_counter > value * 2)
		state = 1;
}

void tan_hurricane::move(){
	if (state == 3)
		return;
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->move();
		if (!tanmak.at(i)->isAlive){
			tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
			delete(tanmak.at(i));
			tanmak.erase(tanmak.begin() + i);
			i--;
		}
		else if ((Run->isPlayerLive && !Run->isPlayerOP) && tanmak.at(i)->getSprite()->getBoundingBox().intersectsRect(Run->playerBoundingBox)){
			float d_x = abs(tanmak.at(i)->getSprite()->getPositionX() - Run->playerPoint.x);
			float d_y = abs(tanmak.at(i)->getSprite()->getPositionY() - Run->playerPoint.y);
			float temp = sqrtf(d_x*d_x + d_y*d_y);
			if (temp < tanmak.at(i)->getSprite()->getBoundingBox().getMaxX() - tanmak.at(i)->getSprite()->getBoundingBox().getMidX() - 8){
				Run->playerDeadTriger();
				tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
				delete(tanmak.at(i));
				tanmak.erase(tanmak.begin() + i);
				i--;
			}
		}
	}

	if (tanmak.size() == 0){
		state = 0;
	}
}

void tan_hurricane::set_stat(int stat){
	tan_hurricane::state = stat;
}

void tan_hurricane::set_parent_tanmak(int tag){
	parentTag = tag;
}

void tan_hurricane::set_tag(int tag){
	tan_hurricane::tag = tag;
}

int tan_hurricane::get_tag(){
	return tag;
}

int tan_hurricane::get_density(){
	return tan_hurricane::density;
}

int tan_hurricane::get_state(){
	return tan_hurricane::state;
}

void tan_hurricane::add_prevMarkedTime(){
	prevMakedTime++;
}

int tan_hurricane::get_prevMarkedTime(){
	return prevMakedTime;
}

void tan_hurricane::set_center(Point pos){
	tan_hurricane::center = pos;
}

int tan_hurricane::get_parent_tag(){
	return parentTag;
}

void tan_hurricane::removecurrentTan(){
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
		delete(tanmak.at(i));
		tanmak.erase(tanmak.begin() + i);
		i--;
	}
}