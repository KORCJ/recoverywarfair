/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef __HELLOWORLD_enemyTan__
#define __HELLOWORLD_enemyTan__

#include"cocos2d.h"
	class enemyTan : cocos2d::Sprite{
private:
	float speed, angle, speed_acc, angle_acc;

	cocos2d::Sprite *tanSprite;

	int tan_type;
public:
	bool isAlive;

	void setSpeed(float speed);
	
	void setAngle(float angle);
	
	void setAngle_acc(float angle_acc);

	float getSpeed();
	
	float getAngle();
	
	void move();
	
	void updateStat();

	cocos2d::Sprite *getSprite();
		
	enemyTan(int tan_type, float x, float y, float n_speed, float n_angle, float n_speed_acc, float n_angle_acc);
};
#endif