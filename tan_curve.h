/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "cocos2d.h"
#include "iostream"
#include "enemyTan_curve.h"
#include "tan_basic.h"
#include "mainRogic.h"

class tan_curve : public tan_basic{
private:
	mainRogic *Run;
	int tanType, state, density, tag, parentTag;
	float rotate_angle_acc, rotate_angle;
	int total_point, value, maked_tanmak;
	std::vector<float> ovalRate;
	std::vector<char> direction;
	std::vector<enemyTan_curve*> tanmak;
	std::vector<cocos2d::Point> center;
public:
	~tan_curve();

	int prevMakedTime;

	void add_prevMarkedTime();

	int get_prevMarkedTime();

	tan_curve(mainRogic *forRun, int total_point, float rotate_angle_acc, int tanType, int value, int density);

	void make();

	void move();

	void add_point(float x, float y, float ovalRate, char direction);

	int get_density();

	int get_state();

	void set_stat(int stat);

	void set_parent_tanmak(int tag);

	void set_tag(int tag);

	int get_tag();

	void set_center(cocos2d::Point pos);

	int get_parent_tag();

	void set_additional_info(float speed);

	void removecurrentTan();
};