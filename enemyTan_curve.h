/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#ifndef __enemyTan_curve__
#define __enemyTan_curve__
#include"cocos2d.h"

class enemyTan_curve{
private:
	float angle, rotate_angle_acc, lenth, rotate_angle;

	float oval_radial, gap_between;

	cocos2d::Sprite *tanSprite;

	std::vector<cocos2d::Point> center;

	std::vector<char> direct;

	std::vector<float> ovalRate;

	int total_point,flag,tan_type;

	bool isAlive;
public:

	float getAngle();

	void setSpeed(float speed);

	void move();

	void updateStat();

	bool getStat();

	cocos2d::Sprite *getSprite();

	enemyTan_curve(int tan_type, int total_point, std::vector<cocos2d::Point> center, std::vector<float> ovalRate, std::vector<char> direct, float rotate_angle_acc);
};
#endif