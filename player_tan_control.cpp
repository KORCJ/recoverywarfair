/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "player_tan_cotrol.h"

USING_NS_CC;

player_tan_control::player_tan_control(mainRogic *run, int speed, int density, int make_density, int total_size, int tanType){
	player_tan_control::forRun = run;
	player_tan_control::speed = speed;
	player_tan_control::density = density;
	player_tan_control::total_size = total_size;
	player_tan_control::make_density = make_density;

	switch (tanType){
	case 1:sprintf(png_name, "vsuri_3.png"); break;
	case 2:sprintf(png_name, "byak_2.png"); break;
	case 3:sprintf(png_name, "never_1.png"); break;
	}
	player_tan_control::texture = Director::getInstance()->getTextureCache()->addImage(png_name);
}

void player_tan_control::add_prevMarkedTime(){
	prevMakedTime++;
}

int player_tan_control::get_prevMarkedTime(){
	return prevMakedTime;
}

int player_tan_control::get_density(){
	return density;
}

void player_tan_control::add_make_prevMarkedTime(){
	make_prevMakedTime++;
}

int player_tan_control::get_make_prevMarkedTime(){
	return make_prevMakedTime;
}

player_tan_control::~player_tan_control(){
	for (int i = 0; i < playerTan.size(); i++){
		playerTan.at(i)->removeFromParentAndCleanup(true);
		playerTan.erase(playerTan.begin() + i--);
	}
}

int player_tan_control::get_make_density(){
	return make_density;
}

void player_tan_control::move(){
	prevMakedTime = 0;

	for (int i = 0; i < playerTan.size(); i++){
		if (playerTan.at(i) != NULL){
			if (playerTan.at(i)->getPositionY()>1224.0f - (texture->getPixelsHigh() / 2.0f)){
				playerTan.at(i)->setVisible(false);
			}
			else{
				playerTan.at(i)->setPositionY(playerTan.at(i)->getPositionY() + speed);
			}
		}
	}
}

std::vector<cocos2d::Sprite*> player_tan_control::get_playerTanVector(){
	return playerTan;
}

void player_tan_control::make(){
	if (forRun->isGameEnd)
		return;
	make_prevMakedTime = 0;

	if (playerTan.size() < player_tan_control::total_size){
		playerTan.push_back(Sprite::createWithTexture(player_tan_control::texture));
		playerTan.at(playerTan.size() - 1)->setPosition(forRun->playerPoint);
		playerTan.at(playerTan.size() - 1)->setScale(0.5f);
		forRun->addChild(playerTan.at(playerTan.size() - 1));
	}
	else{
		for (int i = 0; i < total_size; i++){
			if (!playerTan.at(i)->isVisible()){
				forRun->clear_jakos_prev_shoted_number(i+1);
				playerTan.at(i)->setPosition(forRun->playerPoint);
				playerTan.at(i)->setVisible(true);
				break;
			}
		}
	}
}