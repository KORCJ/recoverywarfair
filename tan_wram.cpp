/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "tan_wram.h"
USING_NS_CC;

tan_wram::tan_wram(mainRogic *forRun, float n_speed, int total_point, float rotate_angle_acc, int tanType, int value, int density){
	tan_wram::Run = forRun;
	tan_wram::rotate_angle_acc = rotate_angle_acc;
	tan_wram::tanType = tanType;
	tan_wram::speed = n_speed;
	tan_wram::rotate_angle = rotate_angle / 360.0f;
	tan_wram::total_point = total_point;
	tan_wram::state = 3;
	tan_wram::value = value;
	tan_wram::density = density;
	tan_wram::prevMakedTime = 0;
}

void tan_wram::set_additional_info(float speed){
	if (speed != -1){
		//		this->speed = speed;
		for (enemyTan_wram *temp : tanmak){
			temp->setSpeed(speed);
		}
	}
}

tan_wram::~tan_wram(){
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
		delete tanmak.at(i);
		tanmak.erase(tanmak.begin() + i--);
	}
}

void tan_wram::make(){

	prevMakedTime = 0;

	//탄막 텍스쳐 로드
	tanmak.push_back(new enemyTan_wram(tanType , total_point, center, speed, rotate_angle_acc));
	//mainRogic에 추가
	Run->addChild(tanmak.at(tanmak.size() - 1)->getSprite());
	if (tanmak.size() == 1)
		tan_wram::state = 2;
	if (tanmak.size() == tan_wram::value)
		tan_wram::state = 1;
}

int tan_wram::get_density(){
	return tan_wram::density;
}

int tan_wram::get_state(){
	return tan_wram::state;
}

void tan_wram::add_prevMarkedTime(){
	prevMakedTime++;
}

int tan_wram::get_prevMarkedTime(){
	return prevMakedTime;
}

void tan_wram::move(){
	if (state == 3)
		return;
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->move();
		if (!tanmak.at(i)->isAlive){
			tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
			delete(tanmak.at(i));
			tanmak.erase(tanmak.begin() + i);
			i--;
		}
		else if ((Run->isPlayerLive && !Run->isPlayerOP) && tanmak.at(i)->getSprite()->getBoundingBox().intersectsRect(Run->playerBoundingBox)){
			float d_x = abs(tanmak.at(i)->getSprite()->getPositionX() - Run->playerPoint.x);
			float d_y = abs(tanmak.at(i)->getSprite()->getPositionY() - Run->playerPoint.y);
			float temp = sqrtf(d_x*d_x + d_y*d_y);
			if (temp < tanmak.at(i)->getSprite()->getBoundingBox().getMaxX() - tanmak.at(i)->getSprite()->getBoundingBox().getMidX() - 8){
				Run->playerDeadTriger();
				tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
				delete(tanmak.at(i));
				tanmak.erase(tanmak.begin() + i);
				i--;
			}
		}
	}
	if (tanmak.size() == 0){
		state = 0;
	}
}

void tan_wram::add_point(float x, float y){
	center.push_back(Point(x, y));
}

void tan_wram::set_stat(int stat){
	tan_wram::state = stat;
}

void tan_wram::set_parent_tanmak(int tag){
	parentTag = tag;
}

void tan_wram::set_tag(int tag){
	tan_wram::tag = tag;
}

int tan_wram::get_tag(){
	return tag;
}

void tan_wram::set_center(cocos2d::Point pos){
	tan_wram::center.at(0) = pos;
}

int tan_wram::get_parent_tag(){
	return parentTag;
}

void tan_wram::removecurrentTan(){
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
		delete(tanmak.at(i));
		tanmak.erase(tanmak.begin() + i);
		i--;
	}
}