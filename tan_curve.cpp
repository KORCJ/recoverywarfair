/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "tan_curve.h"
USING_NS_CC;

tan_curve::tan_curve(mainRogic *forRun, int total_point, float rotate_angle_acc, int tanType, int value, int density){
	tan_curve::Run = forRun;
	tan_curve::rotate_angle_acc = rotate_angle_acc;
	tan_curve::tanType = tanType;
	tan_curve::rotate_angle = rotate_angle / 360.0f;
	tan_curve::state = 3;
	tan_curve::total_point = total_point;
	tan_curve::value = value;
	tan_curve::density = density;
	maked_tanmak = 0;
	tan_curve::prevMakedTime = 0;
}

tan_curve::~tan_curve(){
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
		delete tanmak.at(i);
		tanmak.erase(tanmak.begin() + i--);
	}
}

void tan_curve::set_additional_info(float speed){
	if (speed != -1){
		//		this->speed = speed;
		for (enemyTan_curve *temp : tanmak){
			temp->setSpeed(speed);
		}
	}
}

void tan_curve::make(){
	prevMakedTime = 0;
	maked_tanmak++;
	//탄막 텍스쳐 로드
	tanmak.push_back(new enemyTan_curve(tanType, total_point, center, ovalRate, direction, rotate_angle_acc));
	//mainRogic에 추가
	Run->addChild(tanmak.at(tanmak.size() - 1)->getSprite());
	if (tanmak.size() == 1)
		tan_curve::state = 2;
	if (maked_tanmak == tan_curve::value)
		tan_curve::state = 1;
}

void tan_curve::move(){
	if (state == 3)
		return;
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->move();
		if (!tanmak.at(i)->getStat()){
			tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
			delete(tanmak.at(i));
			tanmak.erase(tanmak.begin() + i);
			i--;
		}
		else if ((Run->isPlayerLive && !Run->isPlayerOP) && tanmak.at(i)->getSprite()->getBoundingBox().intersectsRect(Run->playerBoundingBox)){
			float d_x = abs(tanmak.at(i)->getSprite()->getPositionX() - Run->playerPoint.x);
			float d_y = abs(tanmak.at(i)->getSprite()->getPositionY() - Run->playerPoint.y);
			float temp = sqrtf(d_x*d_x + d_y*d_y);
			if (temp < tanmak.at(i)->getSprite()->getBoundingBox().getMaxX() - tanmak.at(i)->getSprite()->getBoundingBox().getMidX() - 8){
				Run->playerDeadTriger();
				tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
				delete(tanmak.at(i));
				tanmak.erase(tanmak.begin() + i);
				i--;
			}
		}
	}

	if (tanmak.size() == 0){
		state = 0;
	}
}

void tan_curve::add_point(float x, float y, float ovalRate, char direction){
	tan_curve::center.push_back(Point(x, y));
	tan_curve::ovalRate.push_back(ovalRate);
	tan_curve::direction.push_back(direction);
}

void tan_curve::set_stat(int stat){
	tan_curve::state = stat;
}

void tan_curve::set_parent_tanmak(int tag){
	parentTag = tag;
}

void tan_curve::set_tag(int tag){
	tan_curve::tag = tag;
}

int tan_curve::get_tag(){
	return tag;
}

int tan_curve::get_density(){
	return tan_curve::density;
}

int tan_curve::get_state(){
	return tan_curve::state;
}

void tan_curve::add_prevMarkedTime(){
	prevMakedTime++;
}

int tan_curve::get_prevMarkedTime(){
	return prevMakedTime;
}

void tan_curve::set_center(cocos2d::Point pos){
	tan_curve::center.at(0) = pos;
}

int tan_curve::get_parent_tag(){
	return parentTag;
}

void tan_curve::removecurrentTan(){
	for (int i = 0; i < tanmak.size(); i++){
		tanmak.at(i)->getSprite()->removeFromParentAndCleanup(true);
		delete(tanmak.at(i));
		tanmak.erase(tanmak.begin() + i);
		i--;
	}
}