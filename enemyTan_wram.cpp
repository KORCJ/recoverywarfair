/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

#include "enemyTan_wram.h"
#include "toolBoxxxxx.h"
USING_NS_CC;

bool enemyTan_wram::checkCorrectNextCircle(){
	s_angle_acc = (rotate_angle_acc > 0.0f) ? -0.0001f : +0.0001f;
	prev_temp_y = 9999.0f;
	while (true){
		rotate_angle += s_angle_acc;
		s_tan_value = tanf(adjust_angle(added_angle(rotate_angle)) * M_PI * 2);;
		if(checkReachNextCircle(s_tan_value,1))
			return true;
	}
}

bool enemyTan_wram::checkReachNextCircle(float &tan_value, int call_type){

	if (adjust_angle(added_angle(rotate_angle)) == 0.25f){
		doubleLenth_y = lenth * 2.0f;
		doubleLenth_x = 0.0f;
		inline_flag = 1;
	}
	else if (adjust_angle(added_angle(rotate_angle)) == 0.75f){
		doubleLenth_y = -lenth * 2.0f;
		doubleLenth_x = 0.0f;
		inline_flag = 2;
	}
	else {
		doubleLenth_x = lenth * 2 / sqrtf(1.0f + tan_value*tan_value);
		if (rotate_angle < 0.5f)
			doubleLenth_x = -doubleLenth_x;
		doubleLenth_y = tan_value * doubleLenth_x;
		inline_flag = 0;
	}

	doubleLenth_x += center[flag / 2].x;
	doubleLenth_y += center[flag / 2].y;
	if (inline_flag == 1 || inline_flag == 2){
		temp_y = doubleLenth_y;
	}
	else{
		temp_y = tanf(adjust_angle(added_angle(added_angle(rotate_angle))) * M_PI * 2)*(center[flag / 2 + 1].x - doubleLenth_x) + doubleLenth_y;
	}
	if (prev_temp_y == 9999.0f && temp_y < center[flag / 2 + 1].y && upside_flag==0 && call_type==0)
		upside_flag = 1;

	if (0 <= prev_temp_y && prev_temp_y <= 1024 && 0 <= temp_y && temp_y <= 1024){
		if ((prev_temp_y <= center[flag / 2 + 1].y && center[flag / 2 + 1].y <= temp_y) ||
			(temp_y <= center[flag / 2 + 1].y && center[flag / 2 + 1].y <= prev_temp_y)){
			if (call_type == 0){

				if (checkForupSide == 0 && upside_flag==1 && center[flag / 2].y < center[flag / 2 + 1].y){
					prev_temp_y = temp_y;
					checkForupSide = 1;
					return false;
				}
				checkForupSide = 0;
				upside_flag = 0;
				checkCorrectNextCircle();
				prev_temp_y = 9999.0f;
				temp_y = 0.0f;
				flag++;
				angle = added_angle(rotate_angle);
				///////////////
				if (state == 0 && center[flag / 2].x <= center[flag / 2 + 1].x){
					angle = adjust_angle(added_angle(rotate_angle));
					rotate_angle = adjust_angle(rotate_angle + 0.5f);
					rotate_angle_acc = -rotate_angle_acc;
					state = 1;
				}
				else if (state == 1 && center[flag / 2].x > center[flag / 2 + 1].x){
					angle = adjust_angle(added_angle(rotate_angle - 0.5f));
					rotate_angle = adjust_angle(rotate_angle + 0.5f);
					rotate_angle_acc = -rotate_angle_acc;
					state = 0;
				}
				///////////////
				return true;
			}
			else{
				return true;
			}
		}
		else{
			prev_temp_y = temp_y;
			return false;
		}
	}
	else{
		prev_temp_y = temp_y;
		return false;
	}
}

void enemyTan_wram::setSpeed(float speed){
	enemyTan_wram::speed = speed;
}

float enemyTan_wram::getSpeed(){
	return enemyTan_wram::speed;
}

float enemyTan_wram::getAngle(){
	return enemyTan_wram::angle;
}

void enemyTan_wram::move(){
	if (flag%2==1){
		//직선 이동

		prev_pos.x = enemyTan_wram::tanSprite->getPositionX();
		prev_pos.y = enemyTan_wram::tanSprite->getPositionY();

		new_x = cos(adjust_angle(added_angle(enemyTan_wram::angle)) * M_PI * 2)*speed + enemyTan_wram::tanSprite->getPositionX();
		new_y = sin(adjust_angle(added_angle(enemyTan_wram::angle)) * M_PI * 2)*speed + enemyTan_wram::tanSprite->getPositionY();

		
		enemyTan_wram::tanSprite->setPosition(Point(new_x, new_y));

		if ((prev_pos.x <= center[flag / 2 + 1].x && center[flag / 2 + 1].x <= new_x) ||
			(new_x <= center[flag / 2 + 1].x && center[flag / 2 + 1].x <= prev_pos.x)){
			flag++;
//			new_x = cos(adjust_angle(added_angle(enemyTan_wram::angle)) * M_PI * 2)*speed + enemyTan_wram::tanSprite->getPositionX();
//			new_y = sin(adjust_angle(added_angle(enemyTan_wram::angle)) * M_PI * 2)*speed + enemyTan_wram::tanSprite->getPositionY();
//			rotate_angle = adjust_angle(added_angle(atan2(new_y - center[flag / 2].y,new_x - center[flag / 2].x)));
			rotate_angle = adjust_angle(atan2(new_y - center[flag / 2].y , new_x - center[flag / 2].x) / M_PI * 2);
			if (flag / 2 + 1 == total_point)
				isAlive = false;
		}
	}
	else{
		//원형 이동
		if (added_angle(rotate_angle) == 0.25f){
			new_x = 0.0f;
			new_y = lenth;

			checkReachNextCircle(tan_value,0);
		}
		else if (added_angle(rotate_angle) == 0.75f){
			new_x = 0.0f;
			new_y = -lenth;

			checkReachNextCircle(tan_value,0);
		}
		else{
			tan_value = tanf(adjust_angle(added_angle(rotate_angle)) * M_PI * 2);
			new_x = lenth / sqrtf(1.0f + tan_value*tan_value);
			if (rotate_angle < 0.5f)
				new_x = -new_x;
			new_y = tan_value * new_x;

			if(checkReachNextCircle(tan_value,0))
				log(123);
		}

		new_x += center[flag / 2].x;
		new_y += center[flag / 2].y;
		rotate_angle_acc = (float) rotate_angle_acc * 10000;
		rotate_angle_acc /= 10000;
		rotate_angle = adjust_angle(rotate_angle + rotate_angle_acc);
		if (rotate_angle > 1.0f)
			rotate_angle -= 1.0f;
		

		enemyTan_wram::tanSprite->setPosition(Point(new_x, new_y));
	}
	if (tan_type == 6 || tan_type == 7){
		tanSprite->setRotation(tanSprite->getRotation() + 60.0f);
	}
	else{
		tanSprite->setRotation(-1 * abs(rotate_angle * 180 / M_PI) - 90.0f);
	}
	enemyTan_wram::updateStat();
}

void enemyTan_wram::updateStat(){
	if (-100.0f > tanSprite->getPositionX()
		|| tanSprite->getPositionX() > 868.0f
		|| -100.0f > tanSprite->getPositionY()
		|| tanSprite->getPositionY() > 1124.0f){
		isAlive = false;
	}
}

cocos2d::Sprite *enemyTan_wram::getSprite(){
	return enemyTan_wram::tanSprite;
}

bool enemyTan_wram::getStat(){
	return isAlive;
}

enemyTan_wram::enemyTan_wram(int tan_type, int total_point, std::vector<cocos2d::Point> center, float n_speed, float rotate_angle_acc){
	//	log("init");
	char temp_char[25] = { 0, };
	sprintf(temp_char, "jako_tan_0%d.png", tan_type);
	tanSprite = Sprite::createWithSpriteFrameName(temp_char);

	enemyTan_wram::tanSprite->setScale(0.7f);

	enemyTan_wram::speed = n_speed;

	enemyTan_wram::rotate_angle = 0.0f;

	enemyTan_wram::rotate_angle_acc = rotate_angle_acc;

	enemyTan_wram::lenth = 15.0f;

	enemyTan_wram::center = center;

	enemyTan_wram::total_point = total_point;

	enemyTan_wram::tan_type = tan_type;

	upside_flag = 0;

	prev_temp_y = 9999.0f;

	checkForupSide = 0;

	flag = 0;

	state = 0;

	angle = 0.0f;

//	if (center[0].x < center[1].x)
//		state = 0;
//	else
//		state = 1;
//
	isAlive = true;
}