/*
This Source may has runtime error
This Source can use only for testing
2014 all rights reserved for team samdaeoh
*/

float added_angle(float angle){
	return angle + 0.25f;
}

float adjust_angle(double angle){
	if (angle >= 1.0f)
		return angle - 1.0f;
	else if (angle < 0.0f)
		return angle + 1.0f;
	else
		return angle;
}

float adjust_angle_rad(float &angle){
	if (angle > 6.2832f)
		return angle - 6.2832f;
	else if (angle < -6.2832f)
		return angle + 6.2832f;
	return angle;
}

float getBigger(float x1, float x2){
	if (x1 >= x2)
		return x1;
	return x2;
}
float getSmaller(float x1, float x2){
	if (x1 >= x2)
		return x2;
	return x1;
}